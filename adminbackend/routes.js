var express = require('express')
	, multer  = require('multer')
	, passport = require('passport')

var router = express.Router()

var app_params = require('../public/public_params.json')

// Language middleware
router.use(function timeLog (req, res, next) {
	console.log('Admin - Time: ', Date.now())
	// load language content for a logged in user
	if(req.user && typeof(app_params['lang'][req.user.preferences.ui_language]) !== "undefined" ){
		langcontent = app_params['lang'][req.user.preferences.ui_language];
	} else {
		langcontent = app_params['lang']['en'];
	}

	next()
})

// ADMIN HOME
router.get('/', function (req, res) {
	res.redirect(302, '/admin/trips')
})

// SELLER ADMIN
router.get('/sellers', function (req, res) {
	admin.adminLogin('/admin/sellers', req, res, function(err){

		console.log("admin/sellers login (user: "+req.user.username+")");

		seller.getLocaleSellerNames(app_params.supported_locales, '', function(err, localeSellers){
			if (err) res.render('admin/sellers', { title: 'Sellers: Error', err:err });

			var timed_key = token.makeTimedToken('item', req.user.id);

			res.render('admin/sellers', { title: 'Sellers', user : req.user, sellers : localeSellers, key : timed_key });
		});
	})
})

// TRIPS ADMIN
router.get('/trips', function (req, res) {
	admin.adminLogin('/admin/trips', req, res, function(err){
		console.log("admin/trips login in map (user: "+req.user.username+")");
		var skip = 0;
		if(req.query.s){
			skip = parseInt(req.query.s);
		}
		var skipCount = skip * req.user.preferences.ui_resultsPerPage;
		trip.getPendingTrips(30, skipCount, function(err, pending_trips){
			trip.getTripGlobalMetadata(function(err, tripGlobalMetadata){
				if (err){
					res.render('admin/trips', { title: 'admin/trips: Error', lang_content:langcontent, err:err });
				}

				var timed_key = token.makeTimedToken('trip', req.user.id);

				res.render('admin/trips', { title: 'admin/trips', adminuser : req.user, is_admin : true, trips : pending_trips, tripGlobalMetadata : tripGlobalMetadata, s:skip, sc:skipCount, key : timed_key });
			});
		})
	})
})

// STATS COCKPIT
router.get('/stats', function (req, res) {
	admin.adminLogin('/admin/stats', req, res, function(err){
		console.log("admin/stats login in map (user: "+req.user.username+")");
		var skip = parseInt(req.query.s);
		trip.getPendingTrips(40, skip, function(err, pending_trips){
			if (err){
				res.render('admin/stats', { title: 'admin/stats: Error', lang_content:langcontent, err:err });
			}

			var timed_key = token.makeTimedToken('trip', req.user.id);

			res.render('admin/stats', { title: 'admin/stats', adminuser : req.user, is_admin : true, trips : pending_trips, key : timed_key });
		});
	})
})

// POS ADMIN
router.get('/stores', function (req, res) {
	admin.adminLogin('/admin/stores', req, res, function(err){

		console.log("admin/stores login (user: "+req.user.username+")");

		pos.getCityPos(app_params.supported_locales, 'de-berlin', '', function(err, cityPOS){
			if (err) res.render('admin/stores', { title: 'Stores: Error', err:err });

			var timed_key = token.makeTimedToken('item', req.user.id);

			res.render('admin/stores', { title: 'Stores', user : req.user, stores : cityPOS, key : timed_key });
		});
	})
})

// ITEM ADMIN
router.get('/items', function (req, res) {
	admin.adminLogin('/admin/items', req, res, function(err){
		console.log("admin/items login (user: "+req.user.username+")");
		
		// trip.getPendingItems(app_params.supported_locales, 250, function(err, pendingItems){
		trip.getPriceLines(app_params.supported_locales, 250, function(err, pendingItems){
			console.dir(pendingItems[0]);
			if (err) res.render('admin/items', { title: 'Items: Error', err:err });
			
			var timed_key = token.makeTimedToken('item', req.user.id);

			res.render('admin/items', { title: 'Items', user : req.user, 'pendingItems' : pendingItems, key : timed_key });
		});
	})
})

// USER ADMIN
router.get('/users', function (req, res) {
	admin.adminLogin('/admin/users', req, res, function(err){

		console.log("admin/users login in map (user: "+req.user.username+")");
		if(req.query.locale){
			user.getLocaleUsers(app_params.supported_locales, function(err, localeUsers){
				if (err) {
					res.render('/admin/users', { title: 'Users: Error', err:err });
				} else {
					res.render('admin/users', { title: 'Users', user : req.user, is_admin : true, users : localeUsers});
				}
			});
		} else {
			user.getUsers(function(err, users){
				if (err) {
					res.render('/admin/users', { title: 'Users: Error', err:err });
				} else {
					res.render('admin/users', { title: 'Users', user : req.user, is_admin : true, users : users});
				}
			});
		}
	})
})

module.exports = router
