var express = require('express')
    , router = express.Router()
	, app_params = require('./public/public_params.json')
	, conf = require('./_conf_protected')
	, http = require('http')
	, favicon = require('serve-favicon')
	, mongodb = require('mongodb')
	, mongoose = require('mongoose')
	, bodyParser = require('body-parser')
	, multer = require('multer')
	, cookieParser = require('cookie-parser')
	, session = require('express-session')
	, crypto = require('crypto')
    , passport = require('passport')
	, SimpleTimestamps = require( 'mongoose-simpletimestamps' ).SimpleTimestamps
	, MongoStore = require('connect-mongodb-session')(session)
	, dust = require('dustjs-helpers')
	, cons = require('consolidate')
	, fla = require('./models/admin.js')
	, ums = require('./models/user.js')
	, tm = require('./models/trip.js')
	, __t = require('./models/token.js')
	, __s = require('./models/seller.js')
	, __z = require('./models/zip.js')
	, __p = require('./models/pos.js')
	, __m = require('./models/message.js')
	, __r = require('./models/report.js');


var app = express();
app.engine('dust', cons.dust);

// CONNECT TO DATA SOURCES AND START
var dboptions = {
	// user: conf.mongoUser,
	// pass: conf.mongoPass,
	server: { 
		// ssl: conf.mongoSSL,
		// sslCert: fs.readFileSync(conf.mongoSSLcrt),
		// sslKey: fs.readFileSync(conf.mongoSSLkey)
	}
};
var dbstring = "mongodb://" + conf.mongoHost + ":" + conf.mongoPort + "/payprd";
mongoose.connect(dbstring, dboptions);

var mdb = mongoose.connection;
mdb.on('error', console.error.bind(console, 'connection error:'));
mdb.once('open', function callback() {
	console.log('Connected to DB through Mongoose.');
	if(conf.appSecret){
		console.log('Got appSecret (from conf)');
	}else{
		console.log('No appSecret! Make a dummy file "_conf_protected" with content like:');
		console.log('"var Protected = {');
		console.log('appSecret : \'Th3_s3Cr3t_ha5H\'');
		console.log('};');
		console.log('module.exports = Protected;');
	}
});

app.set('domain', 'localhost');
app.set('port', process.env.PORT || 8080);
app.use(favicon("/home/payprd/public/favicon.ico"));

app.set('views', __dirname + '/views');
app.set('view engine', 'dust');
app.set('template_engine', 'dust');

app.use(cookieParser());
app.use(bodyParser.urlencoded(
	{extended: true}
));
app.use(bodyParser.json());
app.use(multer({
    dest: './uploads/'
}).single('trip_pic'));

app.use(session({
	resave:true,
	saveUninitialized: true,
	store: new MongoStore({
		ssl: conf.mongoSSL,
		// sslCert: fs.readFileSync(conf.mongoSSLcrt),
		// sslKey: fs.readFileSync(conf.mongoSSLkey),
		uri: dbstring,
		collection: 'webapp-sessions'
	}, function () {
		console.log("Sessionstore db connection open");
	}),
	secret: 'fishboomerangNYANbananawallaby'+conf.appSecret
}));

app.use(passport.initialize());
app.use(passport.session());

var routes = require('./routes.js')
app.use('/', routes)

var apiendpoints = require('./api/endpoints.js')
app.use('/api/', apiendpoints)

var adminbackend = require('./adminbackend/routes.js')
app.use('/admin/', adminbackend)

http.createServer(app).listen(app.get('port'), function(){
	console.log("Express server listening on port " + app.get('port'));
	
	var route, routes = [];
	app._router.stack.forEach(function(middleware){
		route_methods='';
		route_path='';
		if(middleware.route){
			routes.push(middleware.route);
		} else if(middleware.name === 'router'){ 
			middleware.handle.stack.forEach(function(handler){
				route = handler.route;
				route && routes.push(route);
			});
		}
	});
	console.log(routes);
});
