var express = require('express')
	, multer  = require('multer')
	, fs = require('fs')
	, passport = require('passport')

var router = express.Router()

var app_params = require('./public/public_params.json')

// Language middleware
router.use(function timeLog (req, res, next) {
	// console.log('Time: ', Date.now())
	// load language content for a logged in user
	if(req.user && typeof(app_params['lang'][req.user.preferences.ui_language]) !== "undefined" ){
		langcontent = app_params['lang'][req.user.preferences.ui_language];
	} else {
		langcontent = app_params['lang']['en'];
	}

	next()
})

// HOME
router.get('/', function (req, res) {
	res.render('index', { title: 'payprd', lang_content:langcontent, user : req.user });
})

// LOGIN
router.get('/login', function (req, res) {
	res.render('login', { title: 'Log in', lang_content:langcontent, user : req.user });
})
router.post('/login', function (req, res) {
	user.locallogin(req, res, function(err, success){
		if(success === true){
			res.redirect('/my');
		} else {
			res.render('login', { title: 'Log in', lang_content:langcontent, user : req.user, ui_error: err });
		}
	});
})

// LOGOUT
router.get('/logout', function (req, res) {
	// user.logout
	req.logout();
	res.redirect('/');
})

// JOIN
router.get('/join', function (req, res) {
	res.render('join', { title: 'Sign Up', lang_content:langcontent, user : req.user });
})
router.post('/join', function (req, res) {
	user.create(req.body.username, req.body.password, req.body.verify, req.body.email, req.body.city, res, function(err, success){
		if(success === true){
			console.log("Join success: "+err);
			res.render('join', { title: 'Sign Up', lang_content:langcontent, user : req.user, created : true, ui_error: err});
		} else {
			console.log("Join failed: "+err);
			res.render('join', { title: 'Sign Up', lang_content:langcontent, user : req.user, ui_error: err});
		}
	});
})

// ACTIVATE
router.get('/activate', function (req, res){
	user.activate(req.query.u, req.query.c, function(err, success){
		if(success === true){
			res.render('login', { title: 'Log in', lang_content:langcontent, user : req.user, ui_error: err});
		} else {
			ui_error_message = "Error activating user: "+err;
			res.render('login', { title: 'Log in', lang_content:langcontent, user : req.user, ui_error: ui_error_message});
		}
	});
})

// PASSWD
router.get('/passwd', function (req, res) {
	if(req.user){
		res.render('passwd', { title: 'Change Password', lang_content:langcontent, user : req.user });
	} else {
		res.redirect('/login');
	}
})
router.post('/passwd', function (req, res) {
	user.changePass(req.user.id, req.body.oldpassword, req.body.newpassword, req.body.confpassword, function(err, success){
		if(success === true){
			res.redirect('/my');
		} else {
			// res.redirect('/passwd');
			ui_error_message = langcontent['shared']['error']+" "+err;
			res.render('passwd', { title: 'Change Password', lang_content:langcontent, user : req.user, ui_error: ui_error_message});
		}
	});
})

// CONTACT
router.get('/contact', function (req, res) {
	if(req.user){
		res.render('contact', { title: 'Contact payprd', lang_content:langcontent, user : req.user });
	} else {
		res.render('contact', { title: 'Contact payprd', lang_content:langcontent });
	}
})
router.post('/contact', function (req, res) {
	message.save(req.user.id, req.body.contact_name, req.body.contact_email, req.body.contact_topic, req.body.contact_message, function(err, messageId){
		if(err){
			ui_error_message = langcontent['shared']['error']+" "+err;
			res.render('contact', { title: 'Contact payprd', lang_content:langcontent, user : req.user, ui_error: ui_error_message});
		} else {
			message.send(messageId, function(err){
				if(err){
					console.log("Send messageId "+ messageId +" failed");
				} else {
					console.log("Sent messageId "+ messageId);
					res.render('contact', { title: 'Contact payprd', lang_content:langcontent, user : req.user, ui_error: app_params['lang'][req.user.preferences.ui_language]['shared']['ui_message_sent']});
				}
			});
		}
	});
})

// ADD
router.get('/add', function (req, res) {
	if(req.user){
		var timed_key = token.makeTimedToken('trip', req.user._id);
		res.render('add', { title: 'Add', lang_content:langcontent, user : req.user, token : timed_key });
	} else {
		res.redirect('/login');
	}
})
router.post('/add', function (req, res) {
	res.setHeader('Content-Type', 'application/json');
	if(typeof(req.body.apiKey) !== 'undefined'){
		console.log("Upload via API, userId: "+req.body.userId);
		// Stateless API usage
		// Authorisation only, no Authentication (this is done by token request)
		// Activated for one user, who needs a token from /rest/token
		var testUserId='534bb8eae46016a516170f20';
		if(token.tokenAuth(req, req.path, testUserId, res)){
			if(typeof(req.body.triptext) === 'undefined'){
				console.log("populateFromImage...");
				trip.populateFromImage(req.files.trip_pic, testUserId, res, 'batch', function(err, trip){
					if(err || trip == null){
						console.log("Error adding trip: "+err);
						res.send(500, 'Error: '+err);
					} else {
						console.log("Added trip.");
					}
				});
			}else{
				console.log("populateFromText...");
				trip.populateFromText(req.body.triptext, req.files.trip_pic, testUserId, res, '', function(err, trip){
					if(err || trip == null){
						console.log("Error adding trip: "+err);
						res.send(500, 'Error: '+err);
					} else {
						console.log("Added trip.");
					}
				});
			}
		}
	} else if(typeof(req.user)){
		console.log("Upload in web UI");
		console.log("Calling populateFromImage (/add POST)");
		trip.populateFromImage(req.file, req.user._id, res, 'web-ui', function(err, trip){
			if(err || trip == null){
				console.log("Error adding trip: "+err);
				ui_error_message = "Sorry, couldn't add that image..."+err;
				res.render('add', { title: 'Add', lang_content:langcontent, user : req.user, ui_error: ui_error_message });
				// res.render('add', { title: 'Add', user : req.user, ui_error: ui_error_message });
			} else {
				console.log("Added trip.");
			}
		});
	} else {
		// a 'lost' user?
		res.redirect('/my');
	}
})

// MY (trips/scans...)
router.get('/my', function (req, res) {
	if(req.user){
		var timed_key = token.makeTimedToken('trip', req.user.id);
		var page = parseInt(req.query.s);
		var skipCount = page * req.user.preferences.ui_resultsPerPage;
		if(typeof(req.query.tag) != 'undefined'){
			trip.getUserTripsByTag(req.user, req.query.tag, req.user.preferences.ui_resultsPerPage, skipCount, function(err, user_trips){
				if (err) res.render('my', { title: 'My Trips', lang_content:langcontent });
				trip.getUserTripTags(req.user.id, '', function(err, uniqueTags){
					res.render('my', { title: 'My Trips', lang_content:langcontent, user : req.user, user_id: req.user._id, trips : user_trips, tag : req.query.tag, key: timed_key, tagList : uniqueTags});
				});
			});
		} else {
			trip.getUserTrips(req.user, req.user.preferences.ui_resultsPerPage, skipCount, function(err, user_trips){
				if (err) {
					res.render('my', { title: 'My Trips', lang_content:langcontent });
				}
				if(user_trips.length > 0){
					// console.log("user_trips.length: " + user_trips.length);
					trip.getUserTripTotal(req.user, {}, function(err, result){
						trip.getUserTripTotalHistory(req.user, req.user.preferences.ui_resultsPerPage, function(err, historyresult){
							trip.getUserTripTags(req.user.id, '', function(err, uniqueTags){
								res.render('my', { title: 'My Trips', lang_content:langcontent, user : req.user, user_id: req.user._id, trips : user_trips, triptotalever : String(result[0]['totalSpent']),tripcountever : user_trips.length, trip_total_history : historyresult, tag : req.query.tag, key: timed_key, tagList : uniqueTags});
							});
						});
					});

				} else {
					res.render('my', { title: 'My Trips', lang_content:langcontent, user : req.user, user_id: req.user._id, trips : user_trips });
				}
			});
		}
	} else {
		res.redirect('/login');
	}
})

// SEEK
router.get('/seek', function (req, res) {
	res.render('seek', { title: 'Search Prices', lang_content:langcontent, user : req.user });
})

// PREMIUM
router.get('/premium', function (req, res) {
    res.render('premium', { title: 'Premium Benefits - Why go premium at payprd?', lang_content:langcontent, user : req.user });
})

// ABOUT
router.get('/about', function (req, res) {
    res.render('about', { title: 'About', lang_content:langcontent, user : req.user });
})

// IMPRESSUM
router.get('/impressum', function (req, res) {
    res.render('impressum', { title: 'Impressum', lang_content:langcontent, user : req.user });
})

// MOBILE APP LOGIN
router.get('/app_login', function (req, res) {
	res.render('app_signin', { title: 'Log in', lang_content:langcontent, user : req.user });
});
router.post('/app_login', function (req, res) {
	user.locallogin(req, res, function(err, success){
		if(success === true){
			res.redirect('/my');
		} else {
			res.render('app_signin', { title: 'Log in', lang_content:langcontent, user : req.user, ui_error: err });
		}
	});
});

// EXTERNAL LOGINS
// GOOGLE
router.get('/auth/google', passport.authenticate('google', {scope: "https://www.googleapis.com/auth/userinfo.email"}));
router.get('/auth/google/callback', passport.authenticate('google', {
	failureRedirect: '/join' }),
	function(req, res) {
		res.redirect('/my');
	}
);
// Google secondary domain ownership verification
router.get('/googled6ec0e409f15bbc4.html',function(req,res){res.status(200).send('google-site-verification: googled6ec0e409f15bbc4.html')})

// LINKEDIN
router.get('/auth/linkedin', passport.authenticate('linkedin', {state: 'test1231231422435435'}));
router.get('/auth/linkedin/callback', passport.authenticate('linkedin', {
	failureRedirect: '/join' }),
	function(req, res) {
		res.redirect('/my');
	}
);
// MICROSOFT
router.get('/auth/windowslive', passport.authenticate('windowslive', { scope: ['wl.signin', 'wl.basic', 'wl.emails'] }));
router.get('/auth/windowslive/callback', passport.authenticate('windowslive', { 
	failureRedirect: '/join' }),
	function(req, res) {
		res.redirect('/my');
	}
);

// UPLOADED IMAGES
// Routes to images, behind auth
router.get('/uploads/:uid/:tid', function (req, res) {
	if(req.user ){
		// Admins can view whatever image they want - force-replace user_id
		if(req.user.roles && req.user.roles[0]==='admin'){
			var u = req.params.uid;
		} else {
			var u = req.user.id;
		}
		// type could be anything so check all in order
		var path='/home/payprd/uploads/'+u+'/'+req.params.tid+'.';
		if (fs.existsSync(path+'jpg')) {
			res.sendfile(path+'jpg');
		} else if (fs.existsSync(path+'jpeg')){
			res.sendfile(path+'jpeg');
		} else if (fs.existsSync(path+'png')){
			res.sendfile(path+'png');
		} else if (fs.existsSync(path+'bmp')){
			res.sendfile(path+'bmp');
		} else {
			res.status(403).send('No image here')
		}
	} else {
		// Not logged in
		res.status(403).send('Not sure about this. 2wQagz348z6lmkeRf7ho7j5');
	}
});
router.get('/uploads/:uid/_thumbs/:tid', function (req, res) {
	if(req.user){
		// Admins can view whatever image they want - force-replace user_id
		if(req.user.roles && req.user.roles[0]==='admin'){
			var u = req.params.uid;
		} else {
			var u = req.user.id;
		}
		// thumbs are all png
		var path='/home/payprd/uploads/'+u+'/_thumbs/'+req.params.tid+'.png';
		if (fs.existsSync(path)) {
			res.sendFile(path);
		} else {
			res.status(404).send('No image here')
		}
	} else {
		// Not logged in
		res.status(403).send('Not sure about this. z6lmkho7j52874eRf76gz348')
	}
});
router.get('/uploads/:uid/_cleaned/:tid', function (req, res) {
	if(req.user){
		// Admins can view whatever image they want - force-replace user_id
		if(req.user.roles && req.user.roles[0]==='admin'){
			var u = req.params.uid;
		} else {
			var u = req.user.id;
		}
		// thumbs are all tiff
		var path='/home/payprd/uploads/'+u+'/_cleaned/'+req.params.tid+'.tiff';
		if (fs.existsSync(path)) {
			res.sendFile(path);
		} else {
			res.status(404).send('No image here')
		}
	} else {
		// Not logged in
		res.status(403).send('Not sure about this. z6lmkho7j52874eRf76gz348')
	}
});

module.exports = router
