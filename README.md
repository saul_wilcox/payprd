# README #

### What is this repository for? ###

Payprd is a nodejs + mongo webapp which takes images of receipts and parses useful data from them.
This is done with ocr (tesseract), a flexible parser which can take complex parameters to find data (multiple regexes and match lists) and a knowledgebase of cities, zips, streetnames etc.

### How do I get set up? ###

You'll need:

Users:
- a user called 'payprd' to do most app-related actions. The app expects /home/payprd to exist on the server.

- nginx serving anything under /home/payprd/public

- nodejs (plugins listed in /home/payprd/package.json)

- mongod + mongodb with a db called 'payprd' who uses the admin user (the ssl build is used on live, see note below)

- tesseract (I added the standard DE training files, should still work without)

- imagemagick and graphicsmagick (it does some things much faster using mostly the same commands)


Not everything is easily configurable, but it's getting better!
Passwords & keys are in /home/payprd/_conf_protected
A sample _conf_protected_empty file will be added to help you fill in test cases like external APIs
Note that you'll probably want to use 'mongoSSL : false' since the ssl package isn't available for download and takes hours to build from source

Tips:

- pm2 is a great way to start+manage multiple instances of node for speed / redundancy

- after running tests, pipe the results through ansi2html.sh or similar and dump in a public folder

- tests are WIP, but try something like: 

fu@bar:~# qunit -c /home/payprd/lib/ppp.js -t /home/payprd/test/_ppp.js | tee /home/payprd/public/static/test/results.html

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

It's been pretty much a one-man show from the start:

Saul Wilcox

saulwilcox1@gmail.com

[Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)