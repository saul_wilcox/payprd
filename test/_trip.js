var exec = require("child_process").exec
	execSync = require('sync-exec')
	, fs = require('fs')
	, test_params = require("/home/payprd/test/_params.json");

// var _testServerIp = 'localhost';
var _testServerIp = test_params['targetIp'];

var imagefile_1 = "/home/payprd/test/fixtures/images/lidl_10969_5-8-13.JPG"; // 5b7fe64fdefbec44fea9f0581a801129808d2584
var imagefile_2 = "/home/payprd/test/fixtures/images/DSC_0055_conrad.JPG"; // 5cbdfdc836e59a484d1b09756ec10a862c2ca5fa
var imagefile_3 = "/home/payprd/test/fixtures/images/edeka_10967_10-8-13.jpg"; // 951b6e21acdcf3ab481213000106c049cc3917a0
var imagefile_4 = "/home/payprd/test/fixtures/images/lidl_10969_charl_.JPG"; // 6e65f2c26c3357a75a47f21b472847bc7d1e3f0d
var imagefile_5 = "/home/payprd/test/fixtures/images/kaisers_10969_2-10-13.JPG"; // c8f356cd024dea2e26f6c1dbc97f64402874beae
var imagefile_6 = "/home/payprd/test/fixtures/images/DSC_0243.JPG"; // afe3e55ffc1e7b62d1991ba634b28ba2ed7609a3
var imagefile_7 = "/home/payprd/test/fixtures/images/DSC_0180.JPG"; // 646229dcb02f91ab317684f41a33cca857e3de34
/*
var imagefile_8 = "/home/payprd/test/fixtures/images/cafe-godot_kasta_2013-01-24.jpg"; // 3b3904de89af44807f11223e9f8fa516f1ab4556
var imagefile_9 = "/home/payprd/test/fixtures/images/08a3126497e87624574a3b23db9b9142.jpg"; // 89c420e9fb93ec8d24b2e2f5c137e414ca28330c
var imagefile_10 = "/home/payprd/test/fixtures/images/DSC_0180.JPG"; // 646229dcb02f91ab317684f41a33cca857e3de34
*/

console.log('Logging out (make sure cURL cookie jar is empty)');
var logout = execSync('curl -s http://'+_testServerIp+'/logout');

console.log('Getting access object...');
var res = execSync('curl -s -F "username=local" -F "password=local" http://'+_testServerIp+'/rest/token');
var accessObject = JSON.parse(res.stdout);
console.log('Got token from access object: ' + accessObject.token);

console.log('Teardown: Deleting old tests...');

var commandText = 'curl -s -k -X DELETE https://'+_testServerIp+'/rest/user/'+accessObject.userId+'/trip/5b7fe64fdefbec44fea9f0581a801129808d2584?token='+accessObject.token;
var curlResponse = execSync(commandText);
var commandText = 'curl -s -k -X DELETE https://'+_testServerIp+'/rest/user/'+accessObject.userId+'/trip/5cbdfdc836e59a484d1b09756ec10a862c2ca5fa?token='+accessObject.token;
var curlResponse = execSync(commandText);
var commandText = 'curl -s -k -X DELETE https://'+_testServerIp+'/rest/user/'+accessObject.userId+'/trip/951b6e21acdcf3ab481213000106c049cc3917a0?token='+accessObject.token;
var curlResponse = execSync(commandText);
var commandText = 'curl -s -k -X DELETE https://'+_testServerIp+'/rest/user/'+accessObject.userId+'/trip/6e65f2c26c3357a75a47f21b472847bc7d1e3f0d?token='+accessObject.token;
var curlResponse = execSync(commandText);
var commandText = 'curl -s -k -X DELETE https://'+_testServerIp+'/rest/user/'+accessObject.userId+'/trip/c8f356cd024dea2e26f6c1dbc97f64402874beae?token='+accessObject.token;
var curlResponse = execSync(commandText);
var commandText = 'curl -s -k -X DELETE https://'+_testServerIp+'/rest/user/'+accessObject.userId+'/trip/afe3e55ffc1e7b62d1991ba634b28ba2ed7609a3?token='+accessObject.token;
var curlResponse = execSync(commandText);
var commandText = 'curl -s -k -X DELETE https://'+_testServerIp+'/rest/user/'+accessObject.userId+'/trip/646229dcb02f91ab317684f41a33cca857e3de34?token='+accessObject.token;
var curlResponse = execSync(commandText);

/*
var commandText = 'curl -s -k -X DELETE https://'+_testServerIp+'/rest/user/'+accessObject.userId+'/trip/89c420e9fb93ec8d24b2e2f5c137e414ca28330c?token='+accessObject.token;
var curlResponse = execSync(commandText);
var commandText = 'curl -s -k -X DELETE https://'+_testServerIp+'/rest/user/'+accessObject.userId+'/trip/3b3904de89af44807f11223e9f8fa516f1ab4556?token='+accessObject.token;
var curlResponse = execSync(commandText);
*/

// for checking the trips later
var parsedResult; // global, re-used tempoary store for the result

//  for parsing a json from curl console output
parseCurlResult = function(content){
// preserve newlines, etc - use valid JSON
	var result = content.replace(/\\n/g, "\\n")
				   .replace(/\\r\\n/g, "\\n")  
				   .replace(/\\'/g, "\\'")
				   .replace(/\\"/g, '\\"')
				   .replace(/\\&/g, "\\&")
				   .replace(/\\r/g, "\\r")
				   .replace(/\\t/g, "\\t")
				   .replace(/\\b/g, "\\b")
				   .replace(/\\f/g, "\\f");
	// remove unprintable & invalid characters in JSON
	result = result.replace(/[\u0000-\u0019]+/g,""); 

	return JSON.parse(''+result);
}

uploadTrip = function(accessObject, filepath){
	var commandText = 'curl -s -F "userId='+accessObject.userId+'" -F "apiKey='+accessObject.apiKey+'" -F trip_pic=@'+filepath+' http://'+test_params['targetIp']+'/add';

	var res = execSync(commandText);
	var parsedResult = parseCurlResult(res.stdout);

	return parsedResult.imagechecksum;
}

checkTrip = function(accessObject, checksum){
	var pollTripCommand = 'bash /home/payprd/test/curlpoll.sh '+_testServerIp+' 534bb8eae46016a516170f20 '+checksum+' da6af7c3d95061eced065ce6dd2df7d855c517eb';
	var r = execSync(pollTripCommand);

	var checkTripCommand = 'wget -qO- https://'+_testServerIp+'/rest/user/'+accessObject.userId+'/trip/'+imageChecksum+'?token='+accessObject.token;
	var checkTripResult = execSync(checkTripCommand);

	return parseCurlResult(checkTripResult.stdout);
}

test('Sanity checks', function(){
	ok(true, 'Test process should start testing trip.js');
    ok(trip, 'trip is in namespace');
});

test('Upload + check using async cURL', function(){
	imageChecksum = uploadTrip(accessObject, imagefile_1);
	ok(imageChecksum, 'Image was recieved and checksummed');
	var parsedResult = checkTrip(accessObject,imageChecksum);
	equal(parsedResult[0].foundGenericData.dates[0].content,'05.08.13','Got a response from API with correct generic data');
    expect(2);
});

test('Upload + check using async cURL', function(){
	imageChecksum = uploadTrip(accessObject, imagefile_2);
	ok(imageChecksum, 'Image was recieved and checksummed');
	var parsedResult = checkTrip(accessObject,imageChecksum);
	equal(parsedResult[0].foundGenericData.dates[0].content,'06.04.2013','Got a response from API with correct generic data');
    expect(2);
});

test('Upload + check using async cURL', function(){
	imageChecksum = uploadTrip(accessObject, imagefile_3);
	ok(imageChecksum, 'Image was recieved and checksummed');
	var parsedResult = checkTrip(accessObject,imageChecksum);
	equal(parsedResult[0].foundGenericData.dates[0].content,'10.08.13','Got a response from API with correct generic data');
    expect(2);
});

test('Upload + check using async cURL', function(){
	imageChecksum = uploadTrip(accessObject, imagefile_4);
	ok(imageChecksum, 'Image was recieved and checksummed');
	var parsedResult = checkTrip(accessObject,imageChecksum);
	equal(parsedResult[0].foundGenericData.dates[0].content,'12.09.13','Got a response from API with correct generic data');
    expect(2);
});

test('Upload + check using async cURL', function(){
	imageChecksum = uploadTrip(accessObject, imagefile_5);
	ok(imageChecksum, 'Image was recieved and checksummed');
	var parsedResult = checkTrip(accessObject,imageChecksum);
	equal(parsedResult[0].foundGenericData.dates[0].content,'02.10.2013','Got a response from API with correct generic data');
    expect(2);
});

test('Upload + check using async cURL', function(){
	imageChecksum = uploadTrip(accessObject, imagefile_6);
	ok(imageChecksum, 'Image was recieved and checksummed');
	var parsedResult = checkTrip(accessObject,imageChecksum);
	equal(parsedResult[0].foundGenericData.dates[0].content,'04.02.2014','Got a response from API with correct generic data');
    expect(2);
});

test('Upload + check using async cURL', function(){
	imageChecksum = uploadTrip(accessObject, imagefile_7);
	ok(imageChecksum, 'Image was recieved and checksummed');
	var parsedResult = checkTrip(accessObject,imageChecksum);
	equal(parsedResult[0].foundGenericData.dates[0].content,'12.09.13','Got a response from API with correct generic data');
    expect(2);
});

