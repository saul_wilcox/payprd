
// var _testServerIp = 'localhost';
var _testServerIp = '192.168.32.128';

httpSync = require('http-sync');

test('Sanity checks', function(){

	ok(true, 'Test process should start testing user.js');
    ok(user, 'user is in namespace');

    // ok(user, 'payprd app is running and responding');

    var request = httpSync.request({
	    method: 'GET',
	    headers: {},
	    body: '',

	    protocol: 'http',
	    host: _testServerIp,
	    port: 80,
	    path: '/logout'
	});

	var timedout = false;
	request.setTimeout(5000, function() {
	    console.log("Request Timed out!");
	    timedout = true;
	});
	var response = request.end();

	if (!timedout) {
	    equal(response.statusCode,200,'GET /logout and receive status 200');
	}
});

test('user: token request', function(){

	var request = httpSync.request({
	    method: 'GET',
	    headers: {},
	    body: '',

	    protocol: 'http',
	    host: _testServerIp,
	    port: 80,
	    path: '/rest/token'
	});

	var timedout = false;
	request.setTimeout(5000, function() {
	    console.log("Request Timed out!");
	    timedout = true;
	});
	var response = request.end();

	if (!timedout) {
	    equal(response.statusCode,200,'/rest/token gives 200 before login');
		equal(response.body.toString(),'<!DOCTYPE html><html><head></head><body><form method="POST" action="/rest/token">Username:<br /><input type="text" name="username" /><br />Password:<br /><input type="password" name="password" /><br /><input type="submit" value="Get Token" /><br /></form></body></html>','/rest/token responds with a simple html form');
	}

});
test('user: token request (for stateless login, using username + password)', function(){

	var request = httpSync.request({
	    method: 'POST',
	    headers: {},
	    body: 'username=local&password=local',

	    protocol: 'http',
	    host: _testServerIp,
	    port: 80,
	    path: '/rest/token'
	});

	var timedout = false;
	request.setTimeout(5000, function() {
	    console.log("Request Timed out!");
	    timedout = true;
	});
	var response = request.end();

	if (!timedout) {
		var response_object = JSON.parse(response.body.toString());
		equal(response.statusCode,200,'/rest/token gives 200 after login');
		equal(typeof(response_object),'object','response parsable to object');
		equal(response_object.userId,'534bb8eae46016a516170f20','/rest/token gives correct userId after login');
		equal(response_object.apiKey,'803756c5c6d5196134ad0da17174508333d9054c','/rest/token gives correct apiKey after login');
	}
});
