console.log("Start...");



test("Sanity test", function(){
	ok(true, "Test process should start testing image from image.js");
	// exports = image = {/*...functions...*/}
    ok(image, "image is in namespace");
});

test("Function rollcall", function(){
	ok(true, "Rotate,makeThumbnail,makeChecksum,clean,scan,addStamp");
	
	// exports = image = {/*...functions...*/}
	equal(Object.prototype.toString.call(image.rotate), '[object Function]', "image.rotate is a '[object Function]'");
	equal(Object.prototype.toString.call(image.makeThumbnail), '[object Function]', "image.makeThumbnail is a '[object Function]'");
	equal(Object.prototype.toString.call(image.makeChecksum), '[object Function]', "image.makeChecksum is a '[object Function]'");
	equal(Object.prototype.toString.call(image.clean), '[object Function]', "image.clean is a '[object Function]'");
	equal(Object.prototype.toString.call(image.scan), '[object Function]', "image.scan is a '[object Function]'");
	equal(Object.prototype.toString.call(image.addStamp), '[object Function]', "image.addStamp is a '[object Function]'");
});

test("image.rotate", function(){
	ok(true, "Rotate");
});

test("image.clean", function(){
	ok(true, "Clean");
	equal(image.clean('', '', function(){}),null,"Passing empty strings to image.clean returns null");
	equal(image.clean(0, 0, function(){}),null,"Passing zeroes to image.clean returns null");
	equal(image.clean('../test/temp/cleaned.jpg', '../test/fixtures/images/edeka_10967_10-8-13.jpg', function(){}),null,"Pass local files to image.clean returns true");
});


/*
test("a basic test example", function (assert) {
    ok(true, "this test is fine");
    var value = "hello";
    equal("hello", value, "We expect value to be hello");
});

// QUnit.module("Module A");

test("first test within module", 1, function (assert) {
    ok(true, "a dummy");
});

test("second test within module", 2, function (assert) {
    ok(true, "dummy 1 of 2");
    ok(true, "dummy 2 of 2");
});


QUnit.module("Module B", {
    setup: function () {
        // do some initial stuff before every test for this module
    },
    teardown: function () {
        // do some stuff after every test for this module
    }
});

test("some other test", function (assert) {
    expect(2);
    equal(true, false, "failing test");
    equal(true, true, "passing test");
});

QUnit.module("Module C", {
    setup: function() {
        // setup a shared environment for each test
        this.options = { test: 123 };
    }
});
test("this test is using shared environment", 1, function (assert) {
    deepEqual({ test: 123 }, this.options, "passing test");
});

test("this is an async test example", function (assert) {
    expect(2);
    stop();
    setTimeout(function () {
        ok(true, "finished async test");
        strictEqual(true, true, "Strict equal assertion uses ===");
        start();
    }, 100);
});

*/


