ok=0;
while [[ $ok != foundGenericData ]]; do
	printf '.'
	ok=$(curl -k --silent https://$1/rest/user/$2/trip/$3?token=$4 | grep -om 1 foundGenericData)
    sleep 1.5
done