var exec = require("child_process").exec
	execSync = require('sync-exec')
	, fs = require('fs')
	, test_params = require("/home/payprd/test/_params.json");

var _testServerIp = test_params['targetIp'];

console.log('Logging out (make sure cURL cookie jar is empty)');
var logout = execSync('curl -s http://'+_testServerIp+'/logout');

console.log('Getting access object...');
var res = execSync('curl -s -F "username=local" -F "password=local" http://'+_testServerIp+'/rest/token');
console.dir(res);
var accessObject = JSON.parse(res.stdout);
console.dir(accessObject);
console.dir('Got token from access object: ' + accessObject.token);