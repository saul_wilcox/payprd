var  exec = require("child_process").exec,
	 execSync = require('exec-sync'),
	 fs = require('fs')

// var _testServerIp = 'localhost';
var _testServerIp = '192.168.32.128';

var imagefile_1 = "/home/payprd/test/fixtures/images/lidl_10969_5-8-13.JPG"; // 5b7fe64fdefbec44fea9f0581a801129808d2584
var imagefile_2 = "/home/payprd/test/fixtures/images/DSC_0055_conrad.JPG"; // 5cbdfdc836e59a484d1b09756ec10a862c2ca5fa
var imagefile_3 = "/home/payprd/test/fixtures/images/mediamarkt_berlin.JPG"; // 9eb6db6059c94c7ee81197cb4f4c9d80af36b2e3
var imagefile_4 = "/home/payprd/test/fixtures/images/edeka_10967_10-8-13.jpg"; // 951b6e21acdcf3ab481213000106c049cc3917a0
var imagefile_5 = "/home/payprd/test/fixtures/images/cafe-godot_kasta_2013-01-24.jpg"; // 3b3904de89af44807f11223e9f8fa516f1ab4556
var imagefile_6 = "/home/payprd/test/fixtures/images/lidl_10969_charl_.JPG"; // 6e65f2c26c3357a75a47f21b472847bc7d1e3f0d
var imagefile_7 = "/home/payprd/test/fixtures/images/kaisers_10969_2-10-13.JPG"; // c8f356cd024dea2e26f6c1dbc97f64402874beae
var imagefile_8 = "/home/payprd/test/fixtures/images/DSC_0243.JPG"; // afe3e55ffc1e7b62d1991ba634b28ba2ed7609a3
/*
var imagefile_9 = "/home/payprd/test/fixtures/images/08a3126497e87624574a3b23db9b9142.jpg"; // 89c420e9fb93ec8d24b2e2f5c137e414ca28330c
var imagefile_10 = "/home/payprd/test/fixtures/images/DSC_0180.JPG"; // 646229dcb02f91ab317684f41a33cca857e3de34
*/
console.log('Logging out (make sure cURL cookie jar is empty)');
var logout = execSync('curl -s http://'+_testServerIp+'/logout');

console.log('Getting access object...');
var token = execSync('curl -s -F "username=local" -F "password=local" http://192.168.32.128/rest/token');
var accessObject = JSON.parse(token);

console.log('Deleting old tests...');
// cURL DELETE method: -k -X DELETE
var commandText = 'curl -s -k -X DELETE https://'+_testServerIp+'/rest/user/'+accessObject.userId+'/trip/5b7fe64fdefbec44fea9f0581a801129808d2584?token='+accessObject.token;
var curlResponse = execSync(commandText);
var commandText = 'curl -s -k -X DELETE https://'+_testServerIp+'/rest/user/'+accessObject.userId+'/trip/5cbdfdc836e59a484d1b09756ec10a862c2ca5fa?token='+accessObject.token;
var curlResponse = execSync(commandText);
var commandText = 'curl -s -k -X DELETE https://'+_testServerIp+'/rest/user/'+accessObject.userId+'/trip/9eb6db6059c94c7ee81197cb4f4c9d80af36b2e3?token='+accessObject.token;
var curlResponse = execSync(commandText);
var commandText = 'curl -s -k -X DELETE https://'+_testServerIp+'/rest/user/'+accessObject.userId+'/trip/951b6e21acdcf3ab481213000106c049cc3917a0?token='+accessObject.token;
var curlResponse = execSync(commandText);
var commandText = 'curl -s -k -X DELETE https://'+_testServerIp+'/rest/user/'+accessObject.userId+'/trip/3b3904de89af44807f11223e9f8fa516f1ab4556?token='+accessObject.token;
var curlResponse = execSync(commandText);
var commandText = 'curl -s -k -X DELETE https://'+_testServerIp+'/rest/user/'+accessObject.userId+'/trip/3b3904de89af44807f11223e9f8fa516f1ab4556?token='+accessObject.token;
var curlResponse = execSync(commandText);
var commandText = 'curl -s -k -X DELETE https://'+_testServerIp+'/rest/user/'+accessObject.userId+'/trip/c8f356cd024dea2e26f6c1dbc97f64402874beae?token='+accessObject.token;
var curlResponse = execSync(commandText);
var commandText = 'curl -s -k -X DELETE https://'+_testServerIp+'/rest/user/'+accessObject.userId+'/trip/afe3e55ffc1e7b62d1991ba634b28ba2ed7609a3?token='+accessObject.token;
var curlResponse = execSync(commandText);
var commandText = 'curl -s -k -X DELETE https://'+_testServerIp+'/rest/user/'+accessObject.userId+'/trip/89c420e9fb93ec8d24b2e2f5c137e414ca28330c?token='+accessObject.token;
var curlResponse = execSync(commandText);
var commandText = 'curl -s -k -X DELETE https://'+_testServerIp+'/rest/user/'+accessObject.userId+'/trip/646229dcb02f91ab317684f41a33cca857e3de34?token='+accessObject.token;
var curlResponse = execSync(commandText);

/*
var commandText = 'curl -s -k -X DELETE https://'+_testServerIp+'/rest/user/'+accessObject.userId+'/trip/'+imagechecksum+'?token='+accessObject.token;
var curlResponse = execSync(commandText);
*/
console.dir('Got access object: ' + accessObject);

test('Sanity checks', function(){

	ok(true, 'Test process should start testing trip.js');
    ok(trip, 'trip is in namespace');

});

test('Request using cURL and exec sync', function(){
	console.log('Test upload (image path: ' + imagefile_1 +')');

	var commandText = 'curl -s -F "apiKey='+accessObject.apiKey+'" -F trip_pic=@'+imagefile_1+' http://'+_testServerIp+'/add';

	console.log(commandText);

	var result = execSync(commandText);
});

test('Request using cURL and exec sync', function(){
	console.log('Test upload (image path: ' + imagefile_2 +')');

	var commandText = 'curl -s -F "apiKey='+accessObject.apiKey+'" -F trip_pic=@'+imagefile_2+' http://'+_testServerIp+'/add';

	console.log(commandText);

	var result = execSync(commandText);
});

test('Request using cURL and exec sync', function(){
	console.log('Test upload (image path: ' + imagefile_3 +')');

	var commandText = 'curl -s -F "apiKey='+accessObject.apiKey+'" -F trip_pic=@'+imagefile_3+' http://'+_testServerIp+'/add';

	console.log(commandText);

	var result = execSync(commandText);
});

test('Request using cURL and exec sync', function(){
	console.log('Test upload (image path: ' + imagefile_4 +')');

	var commandText = 'curl -s -F "apiKey='+accessObject.apiKey+'" -F trip_pic=@'+imagefile_4+' http://'+_testServerIp+'/add';

	console.log(commandText);

	var result = execSync(commandText);
});

test('Request using cURL and exec sync', function(){
	console.log('Test upload (image path: ' + imagefile_5 +')');

	var commandText = 'curl -s -F "apiKey='+accessObject.apiKey+'" -F trip_pic=@'+imagefile_5+' http://'+_testServerIp+'/add';

	console.log(commandText);

	var result = execSync(commandText);
});

test('Request using cURL and exec sync', function(){
	console.log('Test upload (image path: ' + imagefile_6 +')');

	var commandText = 'curl -s -F "apiKey='+accessObject.apiKey+'" -F trip_pic=@'+imagefile_6+' http://'+_testServerIp+'/add';

	console.log(commandText);

	var result = execSync(commandText);
});

test('Request using cURL and exec sync', function(){
	console.log('Test upload (image path: ' + imagefile_7 +')');

	var commandText = 'curl -s -F "apiKey='+accessObject.apiKey+'" -F trip_pic=@'+imagefile_7+' http://'+_testServerIp+'/add';

	console.log(commandText);

	var result = execSync(commandText);
});

test('Request using cURL and exec sync', function(){
	console.log('Test upload (image path: ' + imagefile_8 +')');

	var commandText = 'curl -s -F "apiKey='+accessObject.apiKey+'" -F trip_pic=@'+imagefile_8+' http://'+_testServerIp+'/add';

	console.log(commandText);

	var result = execSync(commandText);
});

test('Request using cURL and exec sync', function(){
	console.log('Test upload (image path: ' + imagefile_9 +')');

	var commandText = 'curl -s -F "apiKey='+accessObject.apiKey+'" -F trip_pic=@'+imagefile_9+' http://'+_testServerIp+'/add';

	console.log(commandText);

	var result = execSync(commandText);
});

test('Request using cURL and exec sync', function(){
	console.log('Test upload (image path: ' + imagefile_10 +')');

	var commandText = 'curl -s -F "apiKey='+accessObject.apiKey+'" -F trip_pic=@'+imagefile_10+' http://'+_testServerIp+'/add';

	console.log(commandText);

	var result = execSync(commandText);
});

