var exec = require("child_process").exec
	execSync = require('sync-exec')
	, fs = require('fs')
	, test_params = require("/home/payprd/test/_params.json");

var _testServerIp = test_params['targetIp']; // '192.168.32.128'

// var imagefile_1 = "/home/payprd/test/fixtures/images/lidl_10969_5-8-13.JPG"; // 5b7fe64fdefbec44fea9f0581a801129808d2584
var imagefile_1 = "/home/payprd/test/fixtures/images/DSC_0516.jpg"; // 

console.log('Logging out (make sure cURL cookie jar is empty)');
var logout = execSync('curl -s http://'+_testServerIp+'/logout');

console.log('Getting access object...');
var res = execSync('curl -s -F "username=local" -F "password=local" http://'+_testServerIp+'/rest/token');
var accessObject = JSON.parse(res.stdout);
console.log('Got token from access object: ' + accessObject.token);

console.log('Teardown: Deleting old tests...');

var commandText = 'curl -s -k -X DELETE https://'+_testServerIp+'/rest/user/'+accessObject.userId+'/trip/5b7fe64fdefbec44fea9f0581a801129808d2584?token='+accessObject.token;
var curlResponse = execSync(commandText);

// for checking the trips later
var parsedResult; // global, re-used tempoary store for the result

//  for parsing a json from curl console output
parseCurlResult = function(content){
// preserve newlines, etc - use valid JSON
	var result = content.replace(/\\n/g, "\\n")
				   .replace(/\\r\\n/g, "\\n")  
				   .replace(/\\'/g, "\\'")
				   .replace(/\\"/g, '\\"')
				   .replace(/\\&/g, "\\&")
				   .replace(/\\r/g, "\\r")
				   .replace(/\\t/g, "\\t")
				   .replace(/\\b/g, "\\b")
				   .replace(/\\f/g, "\\f");
	// remove unprintable & invalid characters in JSON
	result = result.replace(/[\u0000-\u0019]+/g,""); 

	return JSON.parse(''+result);
}

uploadTrip = function(accessObject, filepath){
	var commandText = 'curl -s -F "userId='+accessObject.userId+'" -F "apiKey='+accessObject.apiKey+'" -F trip_pic=@'+filepath+' http://'+test_params['targetIp']+'/add';

	var res = execSync(commandText);
	var parsedResult = parseCurlResult(res.stdout);

	return parsedResult.imagechecksum;
}

checkTrip = function(accessObject, checksum){
	var pollTripCommand = 'bash /home/payprd/test/curlpoll.sh 192.168.0.14 534bb8eae46016a516170f20 '+checksum+' da6af7c3d95061eced065ce6dd2df7d855c517eb';
	var r = execSync(pollTripCommand);

	var checkTripCommand = 'wget -qO- https://'+_testServerIp+'/rest/user/'+accessObject.userId+'/trip/'+imageChecksum+'?token='+accessObject.token;
	var checkTripResult = execSync(checkTripCommand);

	return parseCurlResult(checkTripResult.stdout);
}

test('Sanity checks', function(){
	ok(true, 'Test process should start testing trip.js');
    ok(trip, 'trip is in namespace');
});

test('Upload + check using async cURL', function(){
	imageChecksum = uploadTrip(accessObject, imagefile_1);
	ok(imageChecksum, 'Image was recieved and checksummed');
	var parsedResult = checkTrip(accessObject,imageChecksum);
	console.dir(parsedResult[0].foundGenericData);
    expect(1);
});