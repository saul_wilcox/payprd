// Note: this output is intended for console
// but is also converted to HTML sometimes, using ansi2html.sh

console.log("\r\n\r\n");

console.log('Start: ' + new Date() + "\r\n");

console.log('Set up test data...'+"\r\n");

	var sample_text_0 = [ 
        {
            'lineIndex' : 0,
            'text' : '',
        }
	];
	
	var sample_text_1 = [ 
        {
            'lineIndex' : 0,
            'text' : 'Tomaten passiert 0,89 A',
        }
	];

    var sample_text_2 = [ 
        {
            'lineIndex' : 0,
            'text' : 'Nippon Knusperhappen 0,99 A',
        },
    ];

    var sample_text_3 = [ 
        {
            'lineIndex' : 0,
            'text' : 'Gouda 3,16 A',
        },
        {
            'lineIndex' : 1,
            'text' : 'Nutella 300g 0,89 A',
        }
    ];

    var sample_text_4 = [ 
        {
            'lineIndex' : 0,
            'text' : 'Cola 1,25l 1,39 A',
        },
        {
            'lineIndex' : 1,
            'text' : 'Nutella 300g 0,89 A',
        }
    ];

    var sample_text_f = [ 
        {
            'lineIndex' : 0,
            'text' : 'Charlottenstrasse 193',
        }, 
        {
            'lineIndex' : 1,
            'text' : ' 10969 Ber1in',
        },
        {
            'lineIndex' : 2,
            'text' : 'Orange 1l 0,99',
        },
        {
            'lineIndex' : 3,
            'text' : 'Cookies 250g 1,19',
        }
    ];

// Char1ottenstra0e
	var sample_text_5 = [ 
        {
            'lineIndex' : 0,
            'text' : '1 o 1., „., v',
            'flags' : []
        }, 
        {
            'lineIndex' : 1,
            'text' : 'Charlottenstrasse 193',
        }, 
        {
            'lineIndex' : 2,
            'text' : ', 10969 Ber11n',
        }, 
        {
            'lineIndex' : 3,
            'text' : 'No-Sa 8-21 Uhr S0 gesch1ossen',
        }, 
        {
            'lineIndex' : 4,
            'text' : 'Q , EUR',
        }, 
        {
            'lineIndex' : 5,
            'text' : 'B1attsa1at*R0hk.4N1x * 0,89 A',
        }, 
        {
            'lineIndex' : 6,
            'text' : '',
        }, 
        {
            'lineIndex' : 7,
            'text' : 'Kaminwurz. gereuch. 1,99 A',
        }, 
        {
            'lineIndex' : 8,
            'text' : 'Rohrzucker Fa1rg1obe 1,49 N',
        }, 
        {
            'lineIndex' : 9,
            'text' : 'B10 Cafe Mundo Fair 4,99 A',
        }, 
        {
            'lineIndex' : 10,
            'text' : '.Spaghetti XXL 0,49 A',
        }, 
        {
            'lineIndex' : 11,
            'text' : 'Nippon Knusperhappen 0,99 A',
        }, 
        {
            'lineIndex' : 12,
            'text' : 'I ab 4 1 .  .*',
        }, 
        {
            'lineIndex' : 13,
            'text' : 'zu zen1en   11,73',
        }, 
        {
            'lineIndex' : 14,
            'text' : ',Bar E 50,00--',
        }, 
        {
            'lineIndex' : 15,
            'text' : 'R0ckge1d I -38,27',
        }, 
        {
            'lineIndex' : 16,
            'text' : 'NNSTN NNST + Netto 2 Brutto',
        }, 
        {
            'lineIndex' : 17,
            'text' : 'A 7 N 0,77 .10,96 - 11,73',
        }, 
        {
            'lineIndex' : 18,
            'text' : 'Summe 0,77 10,96 11,73',
        }, 
        {
            'lineIndex' : 19,
            'text' : '4993 215545/01 . 05.08.13 09100',
        }, 
        {
            'lineIndex' : 20,
            'text' : 'UST-ID-NRz DE813388874',
        }, 
        {
            'lineIndex' : 21,
            'text' : 'e * e e',
        }, 
        {
            'lineIndex' : 22,
            'text' : 'VIELEN DANK EUR IHREN EINKAUF!',
        }, 
        {
            'lineIndex' : 23,
            'text' : 'LIDL LOHNT SICH.',
        }, 
        {
            'lineIndex' : 24,
            'text' : 'zr* * R',
        }, 
        {
            'lineIndex' : 25,
            'text' : '+ + + wwN.L1d1-Rezept1deen.de + + +',
        }, 
        {
            'lineIndex' : 26,
            'text' : 'Die neue Webseite ist da!',
        }, 
        {
            'lineIndex' : 27,
            'text' : 'Umtimtert fUr fast a11e',
        }, 
        {
            'lineIndex' : 28,
            'text' : 'tnternetfehigen Gerete.',
        }, 
        {
            'lineIndex' : 29,
            'text' : 'Jetzt mit to11en neuen Funktioneng',
        }, 
        {
            'lineIndex' : 30,
            'text' : '+ + +-NNw.L1U1-Rezept1deen.de +,+ +',
        }
    ];

    var sample_text_6 = [ 
        {
            'lineIndex' : 0,
            'text' : '1 o 1., „., v',
        }, 
        {
            'lineIndex' : 1,
            'text' : 'am Char1ottenstra0e 34w3B',
        }, 
        {
            'lineIndex' : 2,
            'text' : ', 10969 Ber11n',
        },
        {
            'lineIndex' : 3,
            'text' : '1 Re1seset Ber11n 9,90 1',
        }, 
        {
            'lineIndex' : 4,
            'text' : '1 Schmand 246 I 0,68 2',
        },
        {
            'lineIndex' : 5,
            'text' : 'Bye',
        },
    ];

	var sample_text_7 = [ 
        {
            'lineIndex' : 0,
            'text' : 'Kaisers 10117 Berlin',
        }, 
        {
            'lineIndex' : 1,
            'text' : ' Friedrichstrasse 5 ',
        }, 
        {
            'lineIndex' : 2,
            'text' : '...--,-,...----.---.-.-.---.----n-ng--n-n-n-.n-.u-n-n-nw*',
        }, 
        {
            'lineIndex' : 3,
            'text' : '1 Cot6sDuRhone0r1g 5,47 2',
        }, 
        {
            'lineIndex' : 4,
            'text' : 'V1e1en Dank f0r Ihren Einkauf  „,',
        }
    ];

    var sample_text_8 = [ 
        {
            'lineIndex' : 0,
            'text' : 'n-ng--n-n-n-.n-.u-n-n-nw*',
        },
        {
            'lineIndex' : 1,
            'text' : 'Conrad 12163 Berlin 5',
        }, 
        {
            'lineIndex' : 2,
            'text' : ' Schloßstraße ',
        }, 
        {
            'lineIndex' : 3,
            'text' : '...--,-,...----.---.-.-.---.----',
        }, 
        {
            'lineIndex' : 4,
            'text' : '1 SPEAKER 29,99 M',
        }, 
        {
            'lineIndex' : 5,
            'text' : 'KASSE 2 23/9/14 13:44',
        }
    ];

    var sample_text_9 = [ 
        {
            'lineIndex' : 0,
            'text' : 'n-ng--n-n-n-.n-.u-n-n-nw*',
        },
        {
            'lineIndex' : 1,
            'text' : 'Conrad 12163 Berlin 5',
        }, 
        {
            'lineIndex' : 2,
            'text' : ' Schloßstraße ',
        }, 
        {
            'lineIndex' : 3,
            'text' : '...--,-,...----.---.-.-.---.----',
        }, 
        {
            'lineIndex' : 4,
            'text' : '1 SPEAKER 29,99 M',
        }, 
        {
            'lineIndex' : 5,
            'text' : 'KASSE 2 23/9/14 13:44',
        }
    ];

    var sample_text_10 = [ 
        {
            "lineIndex" : 1,
            "text" : "Conrad Electronic SE"
        }, 
        {
            "lineIndex" : 2,
            "text" : "SchloBs1raBe 34w3B"
        }, 
        {
            "lineIndex" : 3,
            "text" : "12163 Berlin-Steglitz"
        }, 
        {
            "lineIndex" : 4,
            "text" : "01805 564445"
        }, 
        {
            "lineIndex" : 5,
            "text" : "14 ct/min inkl. MwSt. Festnetz"
        }, 
        {
            "lineIndex" : 6,
            "text" : "max. 42 ct/nin inkl.MwSt. Mobilfunk"
        }, 
        {
            "lineIndex" : 7,
            "text" : "bffnunsszeiten 2"
        }, 
        {
            "lineIndex" : 8,
            "text" : "Mo.-Do. von 10200 bis 20x00 Uhr"
        }, 
        {
            "lineIndex" : 9,
            "text" : "Fr.-Sa. von 10100 bis 21100 Uhr"
        }, 
        {
            "lineIndex" : 10,
            "text" : "uuu.conrad.de"
        }, 
        {
            "lineIndex" : 11,
            "text" : "1 x 350729 SPEHKH SPCH1 2-UEGE LHU1 19.95UR"
        }, 
        {
            "lineIndex" : 12,
            "text" : "1 X 436014 12ER KLEBEST1CKS U 7 X 1 4 1,69HR"
        }, 
        {
            "lineIndex" : 13,
            "text" : "1 X 893415 UNIVERSRLDUBEL UX BX50 R 3.79UR"
        }, 
        {
            "lineIndex" : 14,
            "text" : "1 x"
        }, 
        {
            "lineIndex" : 15,
            "text" : "890232 PHTTEX TEXTIL 200 4.49UR"
        }, 
        {
            "lineIndex" : 16,
            "text" : "Gusautsunne I EUR 29,92"
        }, 
        {
            "lineIndex" : 17,
            "text" : "Bar in 1 EUR 30,00"
        }, 
        {
            "lineIndex" : 18,
            "text" : "Rnckueld EUR 0,08"
        }, 
        {
            "lineIndex" : 19,
            "text" : "n 19,007, Mwst. 25.14 *k EUR 4,76"
        }, 
        {
            "lineIndex" : 20,
            "text" : "Bondatuni Liefer-/Leistunasdalun"
        }, 
        {
            "lineIndex" : 21,
            "text" : "2,99 Mobilfunk-Freininuien"
        }, 
        {
            "lineIndex" : 22,
            "text" : "Knsses 3 Bedienerz 3206 Bons 612"
        }, 
        {
            "lineIndex" : 23,
            "text" : "Un1um1 06.04.2013 UhrZ8i1z 17206"
        }, 
        {
            "lineIndex" : 24,
            "text" : "Bondatunz Liefer.-/1Leist.- Datum"
        }, 
        {
            "lineIndex" : 25,
            "text" : "S1.NF.2 201/115/20063"
        }, 
        {
            "lineIndex" : 26,
            "text" : "Vie1en Dank fUr Ihren Einkauf"
        }, 
        {
            "lineIndex" : 27,
            "text" : "Umtausch nur mit giltigem Kassenbun"
        }
    ];

    var sample_text_11 = [ 
        {
            'lineIndex' : 0,
            'text' : ' lid1',
        }, 
        {
            'lineIndex' : 1,
            'text' : ' L1DL lohnt sich',
        }, 
        {
            'lineIndex' : 2,
            'text' : '12163 SchloBs1raBe 34w3B',
        }, 
        {
            'lineIndex' : 3,
            'text' : 'Berlin-Steglitz',
        }, 
        {
            'lineIndex' : 3,
            'text' : '1 Sprite 2l 1,90 1',
        }, 
        {
            'lineIndex' : 4,
            'text' : '1 Saure Sahne 0,68',
        },
        {
            'lineIndex' : 5,
            'text' : '2 x Oreo Cook 8 3,19',
        },
        {
            'lineIndex' : 6,
            'text' : 'NETTO: 2.54',
        }
    ];
	
	var sample_text_y = [ 
        {
            'lineIndex' : 0,
            'text' : ' lid1',
        }, 
        {
            'lineIndex' : 1,
            'text' : ' L1DL lohnt sich',
        }, 
        {
            'lineIndex' : 2,
            'text' : '12163 SchloBs1raBe 34w3B',
        }, 
        {
            'lineIndex' : 3,
            'text' : 'Berlin-Steglitz',
        }, 
        {
            'lineIndex' : 3,
            'text' : 'NETTO: 2.54',
        }
    ];

    var sample_text_z = [ 
        {
            'lineIndex' : 0,
            'text' : ' lid1',
        }, 
        {
            'lineIndex' : 1,
            'text' : ' L1DL lohnt sich',
        }, 
        {
            'lineIndex' : 2,
            'text' : '12163 SchloBs1raBe 34w3B',
        }, 
        {
            'lineIndex' : 3,
            'text' : 'Berlin-Steglitz',
        }, 
        {
            'lineIndex' : 3,
            'text' : 'NETTO: 2.54',
        }
    ];

    /*
    , 
        {
            'lineIndex' : 4,
            'text' : '01805 564445',
        }, 
        {
            'lineIndex' : 5,
            'text' : '14 ct/min inkl. MwSt. Festnetz',
        }, 
        {
            'lineIndex' : 6,
            'text' : 'max. 42 ct/nin inkl.MwSt. Mobilfunk',
        }, 
        {
            'lineIndex' : 7,
            'text' : 'bffnunsszeiten 2',
        }, 
        {
            'lineIndex' : 8,
            'text' : 'Mo.-Do. von 10200 bis 20x00 Uhr',
        }, 
        {
            'lineIndex' : 9,
            'text' : 'Fr.-Sa. von 10100 bis 21100 Uhr',
        }, 
        {
            'lineIndex' : 10,
            'text' : 'uuu.conrad.de',
        }, 
        {
            'lineIndex' : 11,
            'text' : '1 x 350729 SPEHKH SPCH1 2-UEGE LHU1 19.95UR',
        }, 
        {
            'lineIndex' : 12,
            'text' : '1 X 436014 12ER KLEBEST1CKS U 7 X 1 4 1,69HR',
        }, 
        {
            'lineIndex' : 13,
            'text' : '1 X 893415 UNIVERSRLDUBEL UX BX50 R 3.79UR',
        }, 
        {
            'lineIndex' : 14,
            'text' : '1 x',
        }, 
        {
            'lineIndex' : 15,
            'text' : '890232 PHTTEX TEXTIL 200 4.49UR',
        }, 
        {
            'lineIndex' : 16,
            'text' : 'Gusautsunne I EUR 29,92',
        }, 
        {
            'lineIndex' : 17,
            'text' : 'Bar in 1 EUR 30,00',
        }, 
        {
            'lineIndex' : 18,
            'text' : 'Rnckueld EUR 0,08',
        }, 
        {
            'lineIndex' : 19,
            'text' : 'n 19,007, Mwst. 25.14 *k EUR 4,76',
        }, 
        {
            'lineIndex' : 20,
            'text' : 'Bondatuni Liefer-/Leistunasdalun',
        }, 
        {
            'lineIndex' : 21,
            'text' : '2,99 Mobilfunk-Freininuien',
        }, 
        {
            'lineIndex' : 22,
            'text' : 'Knsses 3 Bedienerz 3206 Bons 612',
        }, 
        {
            'lineIndex' : 23,
            'text' : 'Un1um1 06.04.2013 UhrZ8i1z 17206',
        }, 
        {
            'lineIndex' : 24,
            'text' : 'Bondatunz Liefer.-/1Leist.- Datum',
        }, 
        {
            'lineIndex' : 25,
            'text' : 'S1.NF.2 201/115/20063',
        }, 
        {
            'lineIndex' : 26,
            'text' : 'Vie1en Dank fUr Ihren Einkauf',
        }, 
        {
            'lineIndex' : 27,
            'text' : 'Umtausch nur mit giltigem Kassenbun',
        }
    */
	
    // Sample data and function simulation
	var sample_city = 'berlin-de';
	var sample_zips = [10117,10969,12168,12163];
    var sample_streetnames = [ 'Arkonaplatz', 'Bernauer-Strasse', 'Charlottenstrasse', 'Charlotte', 'Schloßstraße', 'Friedrichstrasse'];
    var sample_products = [ 'Speaker', 'Beans', 'Bohnen', 'Nutella', 'Tomaten', 'Gouda'];
    var not_products = [ 'Pfand', 'total'];
    var total_tags = [ 'summe', 'total'];

   var sellers = [ { _id: "53253da5c6ed8f36f7b145b1",
    locale: 'de_DE',
    name: 'dm',
    pos_ids: [],
    identifiers: [],
    tags: [] },
  { _id: "534d2c4582492cad73b24f16",
    locale: 'de_DE',
    name: 'restaurant ruz',
    pos_ids: [],
    identifiers: [],
    tags: [] },
  { _id: "534d326782492cad73b24f1c",
    locale: 'de_DE',
    name: 'saturn',
    pos_ids: [],
    identifiers:
     [ 'saturn electro handels',
       'saturn electro handels gmbh',
       'saturn' ],
    tags: null },
  { _id: "534d32d082492cad73b24f1d",
    locale: 'de_DE',
    name: 'müller',
    pos_ids: [],
    identifiers: [ 'mueller', 'muller', 'müller' ],
    tags: null },
  { _id: "534d32f282492cad73b24f1e",
    locale: 'de_DE',
    name: 'lyly',
    pos_ids: [],
    identifiers: [ 'ly ly', 'lyly' ],
    tags: null },
  { _id: "534d336082492cad73b24f1f",
    locale: 'de_DE',
    name: 'kamps',
    pos_ids: [],
    identifiers: [ 'kamps', 'kamps backstube', 'kamps' ],
    tags: null },
  { _id: "534d33c282492cad73b24f20",
    locale: 'de_DE',
    name: 'rossman',
    pos_ids: [],
    identifiers: [ 'rossman gmbh', 'dirk rossman', 'dirk rossman gmbh', 'rossman' ],
    tags: null },
  { _id: "534d35a482492cad73b24f22",
    locale: 'de_DE',
    name: 'cafe godot',
    pos_ids: [],
    identifiers: [ 'cafe-godot-berlin', 'cafe godot' ],
    tags: null },
  { _id: "534d36d482492cad73b24f23",
    locale: 'de_DE',
    name: 'dolores',
    pos_ids: [],
    identifiers: [ 'dolores berlin de', 'www.dolores-berlin.de' ],
    tags: null },
  { _id: "534d73b782492cad73b24f2d",
    locale: 'de_DE',
    name: 'lidl',
    pos_ids: [],
    identifiers: [ 'lidl lohnt sich', 'www.lidl-rezeptideen.de', 'lidl' ],
    tags: null },
  { _id: "534dfc4782492cad73b24f31",
    locale: 'de_DE',
    name: 'netto',
    pos_ids: [],
    identifiers: [ 'einfach besser', 'www.netto-online.de', 'netto' ],
    tags: null },
  { _id: "53253d41c6ed8f36f7b145ab",
    locale: 'de_DE',
    name: 'bauhaus',
    pos_ids: [],
    identifiers: [ 'bauhaus.de', 'bauhaus' ],
    tags: null },
  { _id: "534d2cd682492cad73b24f17",
    locale: 'de_DE',
    name: 'bio company',
    pos_ids: [],
    identifiers: [ 'bio company' ],
    tags: null },
  { _id: "53253d29c6ed8f36f7b145a8",
    locale: 'de_DE',
    name: 'kaisers',
    pos_ids: [],
    identifiers: [ 'kaisers.de', 'kaisers' ],
    tags: null },
  { _id: "53253d4dc6ed8f36f7b145ad",
    locale: 'de_DE',
    name: 'penny',
    pos_ids: [],
    identifiers: [ 'einmal zu penny', 'test', 'cats', 'this and that' ],
    tags: null },
  { _id: "534d26f182492cad73b24f14",
    locale: 'de_DE',
    name: 'media markt',
    pos_ids: [],
    identifiers: [ 'ich bin doch nicht blöd' ],
    tags: null },
  { _id: "53253d49c6ed8f36f7b145ac",
    locale: 'de_DE',
    name: 'rewe',
    pos_ids: [],
    identifiers: [ 'r e w e', 'rewe', 'jeden tag ein bisschen' ],
    tags: null },
  { _id: "53253d9ec6ed8f36f7b145b0",
    locale: 'de_DE',
    name: 'kik',
    pos_ids: [],
    identifiers: [ 'texttil-discount', 'textil-discount' ],
    tags: null },
  { _id: "534cc2e782492cad73b24f0c",
    locale: 'de_DE',
    name: 'euroshop',
    pos_ids: [],
    identifiers: [ 'euroshop' ],
    tags: null },
  { _id: "53253d90c6ed8f36f7b145af",
    locale: 'de_DE',
    name: 'mcdonalds',
    pos_ids: [],
    identifiers: [ 'ich liebe es', 'mcdonalds' ],
    tags: null },
  { _id: "53253d1ec6ed8f36f7b145a6",
    locale: 'de_DE',
    name: 'edeka',
    pos_ids: [],
    identifiers: [ 'edeka', 'fiebig' ],
    tags: null },
  { _id: "53253d39c6ed8f36f7b145aa",
    locale: 'de_DE',
    name: 'conrad',
    pos_ids: [],
    identifiers: [ 'www.conrad.de', 'conrad' ],
    tags: null },
{ _id: "53253d39c6ed8f36f7b145aa",
    locale: 'de_DE',
    name: 'lidl',
    pos_ids: [],
    identifiers: [ 'lidl', 'lohnt sich' ],
    tags: null } ];

    function getStreetNamesRegex_derp(sample_city,sample_zips){
        // just an example
        if(typeof(sample_city) != 'undefined' && sample_city.length > 1){
            var streetnames = [ 'Arkonaplatz', 'Bernauer-Strasse', 'Choriner-Strasse', 'Danziger-Strasse', 'Fuerstenberger-Strasse', 'Granseer-Strasse', 'Griebenowstrasse', 'Hagenauer-Strasse', 'Hasenheide', 'Husemannstrasse', 'Kastanienallee', 'Knaackstrasse', 'Kollwitzplatz', 'Kollwitzstrasse', 'Kremmener-Strasse', 'Marthashof', 'Oderberger-Strasse', 'Rheinsberger-Strasse', 'Schloßstraße', 'Schlossstrasse', 'Schoenhauser-Allee', 'Schwedter-Strasse', 'Sredzkistrasse', 'Swinemuender-Strasse', 'Wolliner-Strasse', 'Woerther-Strasse'];
            // return '('+streetnames.join('|')+')';
            return new RegExp('('+streetnames.join('|')+')','g');
        }
    };

    function getStreetNamesRegex(){
        return new RegExp('('+sample_streetnames.join('|')+')','g');
    };

	var tax_pattern = '//';
	var zip_pattern = new RegExp(''+sample_zips.join('|')+'','g');
    var streetnames_pattern = new RegExp('('+sample_streetnames.join('|')+')','g');

    var text_pattern = new RegExp('Tomaten','g');
    var multi_text_pattern = new RegExp('pp','g');

    var house_number_pattern = new RegExp('[0-9]+[AaBcCc]?','g');
    var float_pattern = new RegExp('[0-9]{1,4}[,\.][0-9]{1,4}','g');
    var float2d_pattern = new RegExp('[0-9]{1,4}[,\.][0-9]{2}','g');
    var date_pattern = new RegExp('([0]?[1-9]|[1|2][0-9]|[3][0|1])[.-\/]([0]?[1-9]|[1][0-2])[-A-Z.\\\/]+(20[0-9]{2}|[0-9]{2})','g');

	var total_pattern = new RegExp('(?:Su[mnh]{1,2}e|SU[MNH]{1,2}E EUR|Bar EUR|Verzeh|zu zahlen|zu zen1en|zu [zZ]a[hn][1il]en|zu [zZ]a[hn][1il]en EUR[0o]{0,1}|Betrag|tota[l1]|[Kk]arte|kartenzahlung|ec-karte|visa|mastercard)(?:[ ]*[\w-]*)*[ ]+([0-9]?[0-9]?[0-9]?[0-9]?[,.][0-9][0-9]?)','i');
	
	var url_pattern = '^[a-zA-Z0-9][a-zA-Z0-9-_]{0,61}[a-zA-Z0-9]{0,1}\.([a-zA-Z]{1,6}|[a-zA-Z0-9-]{1,30}\.[a-zA-Z]{2,3})$';
	var domain_pattern = '^[a-zA-Z0-9][a-zA-Z0-9-_]{0,61}[a-zA-Z0-9]{0,1}\.([a-zA-Z]{1,6}|[a-zA-Z0-9-]{1,30}\.[a-zA-Z]{2,3})$';
	var email_pattern = '\b[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}\b';

    var priceLine_pattern = new RegExp('(([\*A-Za-z0-9\.-]+[^\S\n])*)(?:[0-9]*[^\S\n])*([0-9]{1,8}[,.][0-9]{2}?[ ]?[0-9]?)(?:([^\S\n]EUR|[^\S\n][NAB 1]{1,2}))*','g');

    function trim(text){
        // fast trimming
        text = text.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
        return text;
    };
	
	var sample_expect_0 = [{'simpletext' : { 'regEx' : text_pattern }}];
	
    var sample_expect_1 = [
        {
            'simpletext' : { 
                'regEx' : text_pattern
            }
        }
    ];

    var sample_expect_2 = [
        {
            'simpletext_multi' : { 
                'regEx' : multi_text_pattern
            }
        }
    ];


// the expectation name is based on how it is formatted: float with 2 digits
    var sample_expect_3 = [
        {
            'float2d' : { 
                'regEx' : float2d_pattern, 
                'formatFunction' : function(n){
                    n=trim(n); n=n.replace(/,/g, ".");
                    return parseFloat(n);
                }
            }
        }
    ];

    var sample_expect_4 = [
        {
            'float2d' : { 
                'regEx' : float2d_pattern, 
                'formatFunction' : function(n){
                    n=trim(n); n=n.replace(/,/g, ".");
                    return parseFloat(n);
                }
            }
        }
    ];

    var sample_expect_f = [
        {
            'float2d' : { 
                'regEx' : float2d_pattern, 
                'formatFunction' : function(n){
                    n=trim(n); n=n.replace(/,/g, ".");
                    return parseFloat(n);
                }
            }
        },
        {
            'postcodes' : { 
                'regEx': zip_pattern
            }
        }
    ];
    
    var sample_expect_5 = [
        {
            'zips' : { 
                'regEx': zip_pattern
            }
        }
    ];

    // getStreetNamesRegex('berlin-de',sample_zips)
    var sample_expect_6 = [
        {
            'zips' : { 
                'regEx': zip_pattern,
                'children': [ 
                    {
                        'streetnames' : { 
                            'match_in_array' :  sample_streetnames,
                            'lines': 1,
                            'threshold' : 0.4
                        }
                    }
                ]
            }
        }
    ];

    // getStreetNamesRegex('berlin-de',sample_zips)
    var sample_expect_7 = [
        {
            'zips' : { 
                'regEx': zip_pattern,
                'children': [ 
                    {
                        'streetnames' : { 
                            'match_in_array' :  sample_streetnames,
                            'lines': 1,
                            'threshold' : 0.4,
                            'children': [ 
                                {
                                    'house_numbers' : { 
                                        'regEx': house_number_pattern
                                    }
                                }
                            ]
                        }
                    }
                ]
            }
        }
    ];

    var sample_expect_8 = [
        {
            'zips' : { 
                'regEx': zip_pattern,
                'children': [ 
                    {
                        'streetnames' : { 
                            'match_in_array' :  sample_streetnames,
                            'lines': 1,
                            'threshold' : 0.4,
                            'children': [ 
                                {
                                    'house_numbers' : { 
                                        'regEx': house_number_pattern,
                                        'lines': 1
                                    }
                                }
                            ]
                        }
                    }
                ]
            }
        }
    ];

    var sample_expect_9 = [
        {
            'zips' : { 
                'regEx': zip_pattern,
                'children': [ 
                    {
                        'streetnames' : { 
                            'match_in_array' :  sample_streetnames,
                            'lines': 1,
                            'threshold' : 0.4,
                            'children': [ 
                                {
                                    'house_numbers' : { 
                                        'regEx': house_number_pattern,
                                        'lines': 20
                                    }
                                }
                            ]
                        }
                    }
                ]
            }
        }
    ];

    var sample_expect_10 = [
        {
            'sellerName' : {
                'supported_match_objects' : sellers,
                'match_property_name' : 'name',
                'support_array_property_name' : 'identifiers',
                'threshold' : 0.33,
            }
        },
        {
             'zips' : { 
                'regEx': zip_pattern,
                'children': [ 
                    {
                        'streetnames' : { 
                            'match_in_array' :  sample_streetnames,
                            'lines': 1,
                            'threshold' : 0.33,
                            'children': [ 
                                {
                                    'house_numbers' : { 
                                        'regEx': house_number_pattern,
                                        'lines': 1
                                    }
                                }
                            ]
                        }
                    }
                ]
            }
        }
    ];

    var sample_expect_11 = [
        {
            'priceLines' : {
                'regEx': priceLine_pattern,
                'match_group_labels' : ['content',false,'itemPrice'],
                'children': [ 
                    {
                        'float2d' : { 
                            'regEx' : float_pattern, 
                            'lines' : 1,
                            'formatFunction' : function(n){
                                n=trim(n); n=n.replace(/,/g, ".");
                                return parseFloat(n);
                            }
                        }
                    }
                ]
            },
        }
    ];
	
	var sample_expect_12 = [
        {
            'domain' : { 
                'regEx' : tax_pattern, 
                'children': [
                    {
                        'email' : { 
                            'regEx' : email_pattern 
                        }
                    },
                    {
                        'url': {
                            'regEx' : domain_pattern 
                        }
                    }
                ]
            }, 
        }
	];

console.log('Start Tests...');

test('Sanity check', function(){
	ok(true, 'Test process should start testing ppp.js');
    ok(ppp, 'ppp is in namespace');
});

test('ppp: Bad parameters (expectation required)', function(){

	equal(JSON.stringify(ppp()),'1','Calling with no params returns 1');
	equal(JSON.stringify(ppp(null)),'1','Passing 1 null returns 1');
    equal(JSON.stringify(ppp('')),'1','Passing 1 empty string returns 1');
    equal(JSON.stringify(ppp({})),'1','Passing 1 empty object returns 1');
    equal(JSON.stringify(ppp([])),'1','Passing 1 empty array returns 1');
    equal(JSON.stringify(ppp(null, null)),'1','Passing 2 nulls returns 1');
    equal(JSON.stringify(ppp('', '')),'1','Passing 2 empty strings returns 1');
    equal(JSON.stringify(ppp({}, {})),'1','Passing 2 empty objects returns 1');
    equal(JSON.stringify(ppp([], [])),'1','Passing 2 empty arrays returns 1');
    equal(JSON.stringify(ppp(null, null, null)),'1','Passing 3 nulls returns 1');
    equal(JSON.stringify(ppp('', '', '')),'1','Passing 3 empty strings returns 1');
    equal(JSON.stringify(ppp({}, {}, {})),'1','Passing 3 empty objects returns 1');
    equal(JSON.stringify(ppp([], [], [])),'1','Passing 3 empty arrays returns 1');
    equal(JSON.stringify(ppp(null, null, null, null)),'1','Passing 4 nulls returns 1');
	equal(JSON.stringify(ppp('', '', '')),'1','Passing 4 empty strings returns 1');
	equal(JSON.stringify(ppp({}, {}, {}, {})),'1','Passing 4 empty objects returns 1');
	equal(JSON.stringify(ppp([], [], [], [])),'1','Passing 4 empty arrays returns 1');

    equal(JSON.stringify(ppp(sample_text_0)),'1','Calling with expect array only returns 1');
	equal(JSON.stringify(ppp([], sample_text_0)),'1','Calling with empty expect array and possible line(s)');
	equal(JSON.stringify(ppp([],['test'])),'1','Calling with possible expect and line(s) as simple array returns 1');
    equal(JSON.stringify(ppp(0, sample_text_0)),'1','Calling with 0 as expect array and possible line(s) returns 1');
    equal(JSON.stringify(ppp(null, sample_text_0)),'1','Calling with null as expect array and possible line(s) returns 1');
    equal(JSON.stringify(ppp([], sample_text_0)),'1','Calling with empty expect array and possible line(s) returns 1');
});

test('ppp: Limited calls using regEx (no results)', function(){
    equal(JSON.stringify(ppp(sample_expect_0, sample_text_0)),'{}','Finds nothing: Calling with expect array and possible line returns empty array');
});

test('ppp: Good calls, using regEx', function(){
    equal(JSON.stringify(ppp(sample_expect_1, sample_text_1)),
    '{"simpletext":[{"content":"Tomaten","offset":0,"lineIndex":0}]}',
    'Finds a product: Calling with expect array and possible line returns array with "simpletext"');
    equal(JSON.stringify(ppp(sample_expect_2, sample_text_2)),
    '{"simpletext_multi":[{"content":"pp","offset":2,"lineIndex":0},{"content":"pp","offset":16,"lineIndex":0}]}',
    'Finds multiple products in line: Calling with expect array and possible line returns array with "simpletext_multi"');
});

test('ppp: Good calls, using regEx and formatting', function(){
    equal(JSON.stringify(ppp(sample_expect_3, sample_text_3)),
    '{"float2d":[{"content":3.16,"offset":6,"lineIndex":0},{"content":0.89,"offset":13,"lineIndex":1}]}',
    'Finds a float: Calling with possible line and expect array returns array with "2dfloats"');
    equal(JSON.stringify(ppp(sample_expect_4, sample_text_4)),
    '{"float2d":[{"content":1.25,"offset":5,"lineIndex":0},{"content":1.39,"offset":11,"lineIndex":0},{"content":0.89,"offset":13,"lineIndex":1}]}',
    'Finds multiple floats in line: Calling with possible line and expect array returns array with "2dfloats"');
    equal(JSON.stringify(ppp(sample_expect_f, sample_text_f)),
    '{"float2d":[{"content":0.99,"offset":10,"lineIndex":2},{"content":1.19,"offset":13,"lineIndex":3}],"postcodes":[{"content":"10969","offset":1,"lineIndex":1}]}',
    'Finds multiple floats and simpletext in line: Calling with possible line and expect array returns array with "2dfloats"');
});

test('ppp: Good calls, using regEx with explicit set', function(){
    equal(JSON.stringify(ppp(sample_expect_5, sample_text_5)),
    '{"zips":[{"content":"10969","offset":2,"lineIndex":2}]}',
    'Finds a zip code: Calling with possible line and expect array returns array with "zip"');
});

// , null, null, new Array()
test('ppp: Good calls, using regEx and expectation children', function(){
    equal(JSON.stringify(ppp(sample_expect_6, sample_text_6)),
    '{"zips":[{"content":"10969","offset":2,"lineIndex":2}],"streetnames":[{"content":"Charlottenstrasse","offset":3,"lineIndex":1}]}',
    'Finds a street and zip: Calling with possible line and expect array returns array with "zip"');

    equal(JSON.stringify(ppp(sample_expect_7, sample_text_7)),
    '{"zips":[{"content":"10117","offset":8,"lineIndex":0}],"streetnames":[{"content":"Friedrichstrasse","offset":1,"lineIndex":1}],"house_numbers":[{"content":"5","offset":18,"lineIndex":1}]}',
    'Finds a zip, street and house number');

    equal(JSON.stringify(ppp(sample_expect_8, sample_text_8)),
    '{"zips":[{"content":"12163","offset":7,"lineIndex":1}],"streetnames":[{"content":"Schloßstraße","offset":1,"lineIndex":2}],"house_numbers":[{"content":"12163","offset":7,"lineIndex":1},{"content":"5","offset":20,"lineIndex":1}]}',
    'Finds a zip, street and house numbers within 1 line');

    equal(JSON.stringify(ppp(sample_expect_9, sample_text_9)),
    '{"zips":[{"content":"12163","offset":7,"lineIndex":1}],"streetnames":[{"content":"Schloßstraße","offset":1,"lineIndex":2}],"house_numbers":[{"content":"12163","offset":7,"lineIndex":1},{"content":"5","offset":20,"lineIndex":1},{"content":"1","offset":0,"lineIndex":4},{"content":"29","offset":10,"lineIndex":4},{"content":"99","offset":13,"lineIndex":4},{"content":"2","offset":6,"lineIndex":5},{"content":"23","offset":8,"lineIndex":5},{"content":"9","offset":11,"lineIndex":5},{"content":"14","offset":13,"lineIndex":5},{"content":"13","offset":16,"lineIndex":5},{"content":"44","offset":19,"lineIndex":5}]}',
    'Finds a zip, street and any house numbers within 20 lines');

});

test('ppp: Good calls, using supported matches', function(){
    // expect(0);
    equal(JSON.stringify(ppp(sample_expect_10, sample_text_10)),
    '{"sellerName":[{"content":"conrad","hits":[]}],"zips":[{"content":"12163","offset":0,"lineIndex":2}],"streetnames":[{"content":"Schloßstraße","offset":0,"lineIndex":1}],"house_numbers":[{"content":"1","offset":7,"lineIndex":1},{"content":"34","offset":13,"lineIndex":1},{"content":"3B","offset":16,"lineIndex":1}]}',
    'Finds a zip, street, house number and seller');
    });

test('ppp: Good calls, using regEx and match group labels', function(){
    equal(JSON.stringify(ppp(sample_expect_11, sample_text_11)),
    '{"priceLines":[{"content":"1 Sprite 2l ","itemPrice":"1,90 1","offset":0,"lineIndex":4},{"content":"1 Saure Sahne ","itemPrice":"0,68","offset":0,"lineIndex":5},{"content":"2 x Oreo Cook 8 ","itemPrice":"3,19","offset":0,"lineIndex":6},{"content":"NETTO:","itemPrice":"2.54","offset":0,"lineIndex":7}],"float2d":[{"content":2.54,"offset":7,"lineIndex":7}]}',
    'Finds price line containing text and price: Calling with possible line and expect array returns array of priceLines with "itemPrice"');
    });
