var exec = require("child_process").exec
	execSync = require('exec-sync')
	, fs = require('fs')
	, test_params = require("/home/payprd/test/_params.json");

// var _testServerIp = 'localhost';
var _testServerIp = test_params['targetIp']; // '192.168.32.128'

// just a dummy
var imagefile_0 = "/home/payprd/test/fixtures/images/1x1.png"; // a7512d87be78f404bd4874d4bd979d8d48397d79

// global namespace level temp variable for the trip object and the expected id
var tempTripObject='';

console.log('Logging out (make sure cURL cookie jar is empty)');
var logout = execSync('curl -s http://'+_testServerIp+'/logout');

console.log('Getting access object...');
var token = execSync('curl -s -F "username=local" -F "password=local" http://'+_testServerIp+'/rest/token');
if(token){
    var accessObject = JSON.parse(token);
    console.dir('Got access object: ' + accessObject);
} else {
    // console.dir('Failed to authenticate test user local/local');
	// process.exit();
}

console.log('Deleting old tests...');
// cURL DELETE method: -k -X DELETE
var commandText = 'curl -s -k -X DELETE https://'+_testServerIp+'/rest/user/'+accessObject.userId+'/trip/36473bed42d4c40a70092b564e61ebbf090b0863?token='+accessObject.token;
var curlResponse = execSync(commandText);
console.log('Deletion: '+curlResponse);

// for checking the trips later
var pollInterval;
var pollResult;
var pollIntervalDelay = 250;
pollTrip = function () {
	var pollTripCommand = 'curl -s -k https://'+_testServerIp+'/rest/user/'+accessObject.userId+'/trip/'+pollTrip.imagechecksum+'?token='+accessObject.token;
	console.log('pollTrip polling: '+pollTripCommand);
	var pollTripResult = execSync(pollTripCommand);
	// console.log('pollTrip result: '+checkTripResult + ' - type: '+typeof(pollTripResult));
	if(pollTripResult !== '[]'){ // parseCurlResult(checkTripResult)
		console.log("Got response with some data");
		
		var pollTripResult = parseCurlResult(pollTripResult);

		console.dir(pollTripResult);
		if(pollTripResult[0].foundGenericData){ 
		    clearInterval(pollInterval);
		    pollTrip.result = pollTripResult;
		    return;
		} else {
			console.log('\r.');
			return;
		}
	} else {
		console.log('\r.');
		return;
	}
}

//  for parsing a json from curl console output
parseCurlResult = function(content){
// preserve newlines, etc - use valid JSON
	var result = content.replace(/\\n/g, "\\n")
				   .replace(/\\r\\n/g, "\\n")  
				   .replace(/\\'/g, "\\'")
				   .replace(/\\"/g, '\\"')
				   .replace(/\\&/g, "\\&")
				   .replace(/\\r/g, "\\r")
				   .replace(/\\t/g, "\\t")
				   .replace(/\\b/g, "\\b")
				   .replace(/\\f/g, "\\f");
	// remove unprintable & invalid characters in JSON
	result = result.replace(/[\u0000-\u0019]+/g,""); 

	return JSON.parse(''+result);
}

test('Sanity checks', function(){
	ok(true, 'Test process should start testing trip.js');
    ok(trip, 'trip is in namespace');
});

test('Request using cURL and exec sync', function(){
	var triptext = "cats 3/2/1 \r\n banana 1.45 aldi";
	var commandText = 'curl -s -F "apiKey='+accessObject.apiKey+'" -F trip_pic=@'+imagefile_0+' -F "triptext='+triptext+'"  http://'+_testServerIp+'/add';

	console.log(commandText);

	var result = execSync(commandText);
	
	// http://192.168.66.128/rest/user/54d9788dda6b059f0be733d7/trip/5b7fe64fdefbec44fea9f0581a801129808d2584?token=81d42b41e36e7edd43a4ea67bf469dc3d207bbfc

	// console.log(result);

	tempTripObject = parseCurlResult(result);

	// check the object parsed properly and has an ID
	// console.log(tempTripObject);
	ok(tempTripObject['_id'],'Got id from response');
	
	var checkTripCommand = 'curl -s -k https://'+_testServerIp+'/rest/user/'+accessObject.userId+'/trip/'+tempTripObject.imagechecksum+'?token='+accessObject.token;
    console.log(checkTripCommand);
	var checkTripResult = execSync(checkTripCommand);
	console.log('checkTripResult: ' + checkTripResult);
	ok(checkTripResult,'Got a response from API with ID');
	
	equal(tempTripObject.imagechecksum, 
    '36473bed42d4c40a70092b564e61ebbf090b0863',
    'Checksum is as expected');

    expect(3);

});

test("Check for parsed result(async test)", function () {
    expect(1);
    stop();
    setTimeout(function () {
    	var apollTripCommand = 'curl -s -k https://'+_testServerIp+'/rest/user/'+accessObject.userId+'/trip/36473bed42d4c40a70092b564e61ebbf090b0863?token='+accessObject.token;
		console.log('apollTrip polling: '+apollTripCommand);
	    var apollTripResult = execSync(apollTripCommand);
	    apollTripResult = parseCurlResult(apollTripResult);
		equal(apollTripResult[0].foundGenericData.dates[0].content,'3/2/1','Got a response from API with correct generic data');

        start();
    }, 100);
});

/*
test('Request using cURL and exec sync', function(){

	pollInterval = setInterval(pollTrip(tempTripObject.imagechecksum), pollIntervalDelay);

	// pollResult = pollTrip(tempTripObject.imagechecksum);

	console.log(pollResult);
	
	ok(pollResult,'Got a response from API with ID');

	expect(1);
});
*/
/*
	console.log('Test upload (image path: ' + imagefile_0 +')');
	// "-F trip_text=ciuaewhfhvdzufhv sduhf sudhfvu lidl sadnhfashd bananas    1,77 EUR \r\n BOHNEN     2,33 EUR \r\n fuahsnvdkufhnsaudhnfvsaduhnfkuashdufkh asudfuwhn4euz"

	// e9925ece1af26165915f550a55f8da9e84549021
	// var triptext = "Bauhaus Sonnenallee 12045 Berlin 16/10/2014 \r\n Polyfilla     6:89 EUR \r\n Polyfilla     6:89 EUR \r\n Polyfilla     6:89 \r\n  EUR BAUHAUS";
	

	equal(JSON.stringify(ppp(sample_expect_3, sample_text_3)), 
		'{"float2d":[{"content":3.16,"offset":6,"lineIndex":0},{"content":0.89,"offset":13,"lineIndex":1}]}',
		'Finds a float: Calling with possible line and expect array returns array with "2dfloats"');
	
*/
