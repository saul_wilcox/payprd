var exec = require("child_process").exec
	execSync = require('exec-sync')
	, fs = require('fs')
	, test_params = require("/home/payprd/test/_params.json");

var _testServerIp = test_params['targetIp']; // '192.168.32.128'

var sellernames= new Array();

/*
sellernames.push('lidl');
sellernames.push('rewe');
sellernames.push('kaufland');
sellernames.push('kaisers');
sellernames.push('netto');
*/
sellernames.push('ssp');
sellernames.push('dm');
sellernames.push('extra');
sellernames.push('aldi');

var textfile_0 = "/home/payprd/test/fixtures/train/ssp/ssp_1.txt";
var imagefile_0 = "/home/payprd/test/fixtures/train/ssp/ssp_1.jpg";

var textfile_1 = "/home/payprd/test/fixtures/train/lidl/lidl_1.txt";
var imagefile_1 = "/home/payprd/test/fixtures/train/lidl/lidl_1.jpg";

var textfile_2 = "/home/payprd/test/fixtures/train/extra/extra_2.txt";
var imagefile_2 = "/home/payprd/test/fixtures/train/extra/extra_2.jpg";

var textfile_3 = "/home/payprd/test/fixtures/train/rewe/rewe_1.txt";
var imagefile_3 = "/home/payprd/test/fixtures/train/rewe/rewe_1.jpg";

function file_get_contents(f){
	fs.readFile(f, 'utf8', function(err, data) {
	  if (err) throw err;
	  console.log('OK: ' + f);
	  console.log(data)
	});
}

console.log('Logging out (make sure cURL cookie jar is empty)');
var logout = execSync('curl -s http://'+_testServerIp+'/logout');

console.log('Getting access object...');
var token = execSync('curl -s -F "username=local" -F "password=local" http://'+_testServerIp+'/rest/token');
var accessObject = JSON.parse(token);
console.dir('Got access object: ' + accessObject);

console.log('Deleting old tests...');
// cURL DELETE method: -k -X DELETE
var commandText = 'curl -s -k -X DELETE https://'+_testServerIp+'/rest/user/'+accessObject.userId+'/trip/e9925ece1af26165915f550a55f8da9e84549021?token='+accessObject.token;
var curlResponse = execSync(commandText);

test('Sanity checks', function(){

	ok(true, 'Test process should start testing trip.js');
    ok(trip, 'trip is in namespace');

});

var commandText = 'curl -s -F "apiKey='+accessObject.apiKey+'" -F trip_pic=@'+imagefile_0+' -F "triptext='+textfile_0+'" http://'+_testServerIp+'/add';

test('Request using cURL and exec sync', function(){
	console.log('Test trainging data (image path: ' + imagefile_0 +')');
	// "-F trip_text=ciuaewhfhvdzufhv sduhf sudhfvu lidl sadnhfashd bananas    1,77 EUR \r\n BOHNEN     2,33 EUR \r\n fuahsnvdkufhnsaudhnfvsaduhnfkuashdufkh asudfuwhn4euz"
	var textdata = fs.readFileSync(textfile_0);
	// e9925ece1af26165915f550a55f8da9e84549021
	var commandText = 'curl -s -F "apiKey='+accessObject.apiKey+'" -F trip_pic=@'+imagefile_0+' -F "triptext='+textdata+'" http://'+_testServerIp+'/add';

	console.log('About to do: '+commandText);

	var result = execSync(commandText);

	console.log('Done: '+commandText);
});

test('Request using cURL and exec sync', function(){
	console.log('Test trainging data (image path: ' + imagefile_1 +')');
	// "-F trip_text=ciuaewhfhvdzufhv sduhf sudhfvu lidl sadnhfashd bananas    1,77 EUR \r\n BOHNEN     2,33 EUR \r\n fuahsnvdkufhnsaudhnfvsaduhnfkuashdufkh asudfuwhn4euz"
	var textdata = fs.readFileSync(textfile_1);
	// e9925ece1af26165915f550a55f8da9e84549021
	var commandText = 'curl -s -F "apiKey='+accessObject.apiKey+'" -F trip_pic=@'+imagefile_1+' -F "triptext='+textdata+'" http://'+_testServerIp+'/add';

	console.log('About to do: '+commandText);

	var result = execSync(commandText);

	console.log('Done: '+commandText);
});

test('Request using cURL and exec sync', function(){
	console.log('Test trainging data (image path: ' + imagefile_2 +')');
	// "-F trip_text=ciuaewhfhvdzufhv sduhf sudhfvu lidl sadnhfashd bananas    1,77 EUR \r\n BOHNEN     2,33 EUR \r\n fuahsnvdkufhnsaudhnfvsaduhnfkuashdufkh asudfuwhn4euz"
	var textdata = fs.readFileSync(textfile_2);
	// e9925ece1af26165915f550a55f8da9e84549021
	var commandText = 'curl -s -F "apiKey='+accessObject.apiKey+'" -F trip_pic=@'+imagefile_2+' -F "triptext='+textdata+'" http://'+_testServerIp+'/add';

	console.log('About to do: '+commandText);

	var result = execSync(commandText);

	console.log('Done: '+commandText);
});

test('Request using cURL and exec sync', function(){
	console.log('Test trainging data (image path: ' + imagefile_3 +')');
	// "-F trip_text=ciuaewhfhvdzufhv sduhf sudhfvu lidl sadnhfashd bananas    1,77 EUR \r\n BOHNEN     2,33 EUR \r\n fuahsnvdkufhnsaudhnfvsaduhnfkuashdufkh asudfuwhn4euz"
	var textdata = fs.readFileSync(textfile_3);
	// e9925ece1af26165915f550a55f8da9e84549021
	var commandText = 'curl -s -F "apiKey='+accessObject.apiKey+'" -F trip_pic=@'+imagefile_3+' -F "triptext='+textdata+'" http://'+_testServerIp+'/add';

	console.log('About to do: '+commandText);

	var result = execSync(commandText);

	console.log('Done: '+commandText);
});

/*
for(i=0,sc=sellernames.length;i<sc;i++){
	var imagecounter=1;
	var loopdone = false; 
	while(!loopdone) {
	    if(imagecounter > 999){
	    	console.log('Loop overrun');
	    	break;
	    } else {
	    	var imagepath = "/home/payprd/test/fixtures/train/"+sellernames[i]+"/"+sellernames[i]+"_"+imagecounter+".jpg";
		    // console.log('IMG PATH: '+imagepath);
		    var textpath = "/home/payprd/test/fixtures/train/"+sellernames[i]+"/"+sellernames[i]+"_"+imagecounter+".txt";
		    // console.log('TEXT PATH: '+textpath);

		    if(fs.existsSync(imagepath) || fs.existsSync(textpath)){
		    	var textdata = fs.readFileSync(textpath);
		    	console.log('TEXT: '+textdata);
		    	
				var commandText = 'curl -s -F "apiKey='+accessObject.apiKey+'" -F trip_pic=@'+imagepath+' -F "triptext='+textdata+'" http://'+_testServerIp+'/add';

				console.log(commandText);

				var result = execSync(commandText);

		    	/*
		    	test('Request using cURL and exec sync', function(){
					console.log('Test upload (image path: ' + imagepath +')');

					var textdata = fs.readFileSync(textpath);

					var commandText = 'curl -s -F "apiKey='+accessObject.apiKey+'" -F trip_pic=@'+imagepath+' -F "triptext='+textdata+'" http://'+_testServerIp+'/add';

					console.log(commandText);

					var result = execSync(commandText);

				});
				* /

		    } else {
		    	loopdone=true;
		    }
	    }
	    imagecounter++;
	}
}
*/

/*
test('Request using cURL and exec sync', function(){
	console.log('Test upload (image path: ' + imagefile_0 +')');
	// "-F trip_text=ciuaewhfhvdzufhv sduhf sudhfvu lidl sadnhfashd bananas    1,77 EUR \r\n BOHNEN     2,33 EUR \r\n fuahsnvdkufhnsaudhnfvsaduhnfkuashdufkh asudfuwhn4euz"

	// e9925ece1af26165915f550a55f8da9e84549021
	var commandText = 'curl -s -F "apiKey='+accessObject.apiKey+'" -F trip_pic=@'+imagefile_0+' -F "triptext=Bauhaus Sonnenallee 12045 Berlin 16/10/2014 \r\n Polyfilla     6:89 EUR \r\n Polyfilla     6:89 EUR \r\n Polyfilla     6:89 \r\n  EUR BAUHAUS"  http://'+_testServerIp+'/add';

	console.log(commandText);

	var result = execSync(commandText);
});
*/

