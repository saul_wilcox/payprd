
/*
*	
*	ppp - payprd Power Parser
*
*   Flexible multiple parser/multi-regex application function
*
**
*	examine 		- 	txt to parse/examine for types as array of lineObjects
*	expect 			- 	expectation names ('things to find')
*   results			- 	holds results when recursing
*	recurse_indexes -   holds an array of line index(es) of last hit(s) when recursing, to target same line(s) as previous hits
*
*/

exports = ppp = function(expect, examine, foundData, recurse_indexes){
	var debug = true;
	if(
		(typeof(examine) !== 'undefined' && examine && examine.length >= 1)
		&& 
		(typeof(expect) !== 'undefined' && expect && expect.length >= 1)
	){
		if(typeof(foundData) === 'undefined'){
			var foundData = {};
		}
		// loop over expectations
		for(expi=0; expi < expect.length; expi++){

			// get expectation parameter keys+values
			var expectation_names = Object.keys(expect[expi]);
			var expectation_parameters = Object.keys(expect[expi][expectation_names[0]]);
			
			// track parameter name
			var expectation_name_index = 0;
			var expectation_name = expectation_names[expectation_name_index];
			if(debug){
				console.log('Expectation Name: '+expectation_name);
			}
			// loop over expectation paramters
			for(var exp_param in expectation_parameters) {
				var hit_indexes = [];

				// do something with each parameter depending on type
				// 'regEx' start
				// pre-regex: make a hit var, if it's empty, we don't bother with 'child' regexes
				var rex_hit = false;
				if(expectation_parameters[exp_param] === 'regEx'){
					// loop over each line in examine
					for(examine_line_counter=0; examine_line_counter < examine.length; examine_line_counter++){

 						var reg = expect[expi][expectation_name][expectation_parameters[exp_param]];

						// TODO:
						// currently using while() to cover multiple matches, compare with:
						// matches = reg.exec(examine[examine_line_counter].text);
						// if(Object.prototype.toString.call(matches) === '[object Array]' && matches.length >= 1){...}
						var match_count = 0;

						while ((matches = reg.exec(examine[examine_line_counter].text)) !== null){

							// we may be recursing and checking a specific index set from the hits
							// recurse_indexes could be:
							// undefined, meaning take all hits
							// index is in the list of hits - take any on the same line
							// index +/- the range in 'lines' is in the list of hits - take hits on these lines

							var keep_hit = false;

							if(typeof(recurse_indexes) === 'undefined' || recurse_indexes.indexOf(examine[examine_line_counter]['lineIndex']) > -1){
								
								keep_hit = true;
							} else if(expectation_parameters.indexOf('lines') > -1){
								for(r=1;r<=expect[expi][expectation_name]['lines'];r++){
									if(	recurse_indexes.indexOf((examine[examine_line_counter]['lineIndex'] - r)) > -1 ||
										recurse_indexes.indexOf((examine[examine_line_counter]['lineIndex'] + r)) > -1
									){
										keep_hit = true;
										break;
									}
								}
							}

							if(keep_hit){
								if(typeof(foundData[expectation_name]) === 'undefined'){
									// examine[examine_line_counter].
									foundData[expectation_name] = [];
								}

								// if there's a 'formatfunction' parameter, use it on the result now
								if(expectation_parameters.indexOf('formatFunction') >= 0){
									matches[0] = expect[expi][expectation_names[0]].formatFunction(matches[0]);
								}
								
								// add to foundData, possibly renaming 'content' field based on labelled regex match groups
								var foundDataObject={};
								if(expectation_parameters.indexOf('match_group_labels') > -1){
									match_group_labels = expect[expi][expectation_name]['match_group_labels'];
									for(match_group_labels_counter=0;match_group_labels_counter<(match_group_labels.length+1);match_group_labels_counter++){
										if(match_group_labels[match_group_labels_counter]){
											foundDataObject[match_group_labels[match_group_labels_counter]] = matches[(match_group_labels_counter+1)];
										}
									}
								} else {
									foundDataObject['content'] = matches[0];
								}

								foundDataObject['offset'] = matches['index'];
								foundDataObject['lineIndex'] = examine_line_counter;
								foundData[expectation_name].push(foundDataObject);
								
								// get the index of this line in case there is recursion
								hit_indexes = arrayUnique([examine_line_counter],hit_indexes);
								rex_hit = true;
							}

							if(debug && (expectation_name == 'zips' || expectation_name == 'streetnames')){
								console.log('---- zips while loop result: '+ matches[0]);
								console.dir(foundDataObject);
							}
						}
					}

					if(expectation_parameters.indexOf('children') > -1){
						if(rex_hit === true){
							return ppp(expect[expi][expectation_name]['children'], examine, foundData, hit_indexes);
						}
					} else {
						if(debug){
							console.log('Expectation '+ expectation_name +' has no children');
						}
					}
				} // 'regEx' end

				// 'match_in_array' start
				// pre-match: make a hit var, if it's empty, we don't bother with 'child' regexes
				var match_hit = false;
				if(expectation_parameters[exp_param] === 'match_in_array'){
					// make an array to store close matches, sort by rank later
					var matches_ranked = [];

					var threshold = expect[expi][expectation_name]['threshold'];
					var match_words_array = expect[expi][expectation_name][expectation_parameters[exp_param]];

					// loop over each line in examine
					for(examine_line_counter=0; examine_line_counter < examine.length; examine_line_counter++){
						// split into words
						var examine_line = examine[examine_line_counter].text;
						
						examine_words_array = examine_line.split(" ");

						// See TODO @ EOF
						// loop over each word in line
						for(examine_word_counter=0; examine_word_counter < examine_words_array.length; examine_word_counter++){
							// loop over each word in matches and compare
							for(match_word_counter=0; match_word_counter < match_words_array.length; match_word_counter++){
								
								// distance_length: the levenshtein distance (# of character replacements needed to make A = B) divided by the length of the word
								var distance_length = 0;
								distance_length = (levenshtein(examine_words_array[examine_word_counter].toLowerCase(), match_words_array[match_word_counter].toLowerCase()) / examine_words_array[examine_word_counter].length);

								if( distance_length < threshold ){
									if(debug){
										console.log('ExHit (ex: ' + expectation_name + ' @#: ' + examine_words_array[examine_word_counter].toLowerCase() + ') score ' + distance_length + ' for ' + match_words_array[match_word_counter].toLowerCase());
									}
									match_hit = true;
									// put into rank + apply modifiers + sort

									var rankModifier = 1;

									// add 1 so we can see effect of multipliers when rank is 0
									rank = distance_length + 1; 

									// get rank index for this entry if we've seen it before
									var rankIndex = null;
									for (var r=0, iLen=matches_ranked.length; r<iLen; r++) {
										if (matches_ranked[r].name == match_words_array[match_word_counter]){
											rankIndex = r;
											break;
										}
									}

									if(rankIndex){
										matches_ranked[rankIndex].rank = (matches_ranked[rankIndex].rank * rankModifier);
										rank = (rank * rankModifier);
									} else {
										rank = (rank * rankModifier);
									}
									var top_ranked = match_words_array[match_word_counter];
									matches_ranked.push(
										{
											'rank' : rank
											, 'text' : match_words_array[match_word_counter]
											, 'line_index' : examine_line_counter
											, 'offset' : examine[examine_line_counter].text.indexOf(examine_words_array[examine_word_counter])
										}
									);
								} // if above threshold parameter
							} // end for each match word in set being matched
						} // end for examine_word_counter
					} // end for examine_line_counter

					// console.dir(matches_ranked);

					if(matches_ranked.length >= 1){	
						// sort ranklist
						matches_ranked.sort(function(a, b) { 
							return a.rank - b.rank;
						});
						
						for (var r=0, r_try = 15; r < r_try; r++) {
							if(typeof(matches_ranked[r]) != 'undefined' ){
								// pick best match
								var found_match = matches_ranked[0];
							}
						}
						// make an object to contain this expectation type if it deosn't exist
						if(typeof(foundData[expectation_name]) === 'undefined'){
							foundData[expectation_name] = [];
						}

						// if there's a 'formatfunction' parameter, use it on the result now
						if(expectation_parameters.indexOf('formatFunction') >= 0){
							found_match.text = expect[expi][expectation_names[0]].formatFunction(found_match.text);
						}

						// add to 'found'
						foundData[expectation_name].push(
							{
								'content':found_match.text
								,'offset': found_match.offset
								,'lineIndex':found_match['line_index']
							}
						);
						if(expectation_name == 'zips'){
							console.log('--- pushed zip: '+ found_match.text);
						}
						// get the index of this line in case there is recursion
						hit_indexes = arrayUnique([found_match['line_index']],hit_indexes);

						// matches can take children if there's a result
						if(expectation_parameters.indexOf('children') > -1){
							if(match_hit === true){	
								return ppp(expect[expi][expectation_name]['children'], examine, foundData, hit_indexes);
							}
						}
					}
				} // 'match_in_array' end
				
				// if debug, show the top5
				if(debug && typeof(matches_ranked) != 'undefined' ){
					console.log('Pre-Support set matches for ' + expectation_name + ':');
					var mrl = matches_ranked.length;
					if(mrl > 5){
						mrl=5;
					}
					for (var mr=0; mr < mrl; mr++) {
						console.log(matches_ranked[r]);
					}
				}

				// 'supported_match_objects' start

				// pre-match: make a hit var, if it's empty, we don't bother with 'child' regexes
				var supported_match_hit = false;
				if(expectation_parameters[exp_param] === 'supported_match_objects'){
					var supported_matches_ranked = [];
					var found_supported_match = '';

					var threshold = expect[expi][expectation_name]['threshold'];
					var match_words_array = [];
					var match_words_supports_array = [];
					var support_sets = [];

					// build support sets from object set
					for(object_counter=0; object_counter < expect[expi][expectation_name]['supported_match_objects'].length; object_counter++){
						var support_set ={};

						support_set['name'] = expect[expi][expectation_name]['supported_match_objects'][object_counter][expect[expi][expectation_name]['match_property_name']];
						match_words_array.push(expect[expi][expectation_name]['supported_match_objects'][object_counter][expect[expi][expectation_name]['match_property_name']]);

						support_set['supports'] = expect[expi][expectation_name]['supported_match_objects'][object_counter][expect[expi][expectation_name]['support_array_property_name']];
						match_words_supports_array.push(expect[expi][expectation_name]['supported_match_objects'][object_counter][expect[expi][expectation_name]['support_array_property_name']]);

						support_set['rank'] = 1;
						support_set['hits'] = 0;
						support_set['support_hits'] = [];

						support_sets.push(support_set);
					}
					
					if(support_sets.length > 0){
					
						for(support_sets_counter=0; support_sets_counter < support_sets.length; support_sets_counter++){
							console.log("SupSet started:");
							console.dir(support_sets[support_sets_counter]);

							// loop over each word in support set
							for(support_set_strings_counter=0; support_set_strings_counter < support_sets[support_sets_counter]['supports'].length; support_set_strings_counter++){

								// loop over each line in examine
								for(examine_line_counter=0; examine_line_counter < examine.length; examine_line_counter++){
									// skip junk lines
									if(/([a-zA-Z0-9]{2,})/.test(examine[examine_line_counter].text) === false){
										// console.log('SKIPPED LINE: ' + examine[examine_line_counter].text );
										continue;
									}

									// split into words
									var examine_line = examine[examine_line_counter].text;
									examine_words_array = examine_line.split(/\s+/);

									// loop over each word in line
									for(examine_word_counter=0; examine_word_counter < examine_words_array.length; examine_word_counter++){
										var examine_string = examine_words_array[examine_word_counter].replace(/^\s+|\s+$/g);
										// content string should be as many words as the target

										var support_string_split = support_sets[support_sets_counter]['supports'][support_set_strings_counter].split(/\s+/);

										if(support_string_split.length > 1){
											// console.log('RANGED STRING: "' + support_sets[support_sets_counter]['supports'][support_set_strings_counter] + '" ON LINE "' + examine_line +'"');

											for(support_string_word_counter = 1; support_string_word_counter < support_string_split.length; support_string_word_counter++){
												// console.log('RANGED STRING WAS: "' + examine_string + '"');
												if(typeof(examine_words_array[examine_word_counter+support_string_word_counter]) !== 'undefined'){ //  && examine_words_array[examine_word_counter+support_string_word_counter].replace(/^\s+|\s+$/g, '') !== ''
													examine_string += ' ' +examine_words_array[examine_word_counter+support_string_word_counter];
													// console.log('RANGED STRING NOW: "' + examine_string + '"');
												} else {
													break;
												}
											}
											// console.log('JOINED STRING: "' + examine_string + '"' );
										}

										if(examine_string == ''){
											if(debug){
												console.log('EXAMINE STRING EMPTY' );
											}
											continue;
										}
	/*
										// skip if word has no 2-printable pairs
										if(/([a-zA-Z0-9]{2,})/.test(examine_words_array[examine_word_counter]) === false){
											console.log('SKIPPED: ' + examine_words_array[examine_word_counter] );
											continue;
										}
	*/

										var content_string = examine_string.replace(/-/g, ' ');

										var match_string = support_sets[support_sets_counter]['supports'][support_set_strings_counter].replace(/-/g, ' ');

										var distance_length = 0;
										distance_length = (levenshtein(match_string.toLowerCase(), content_string.toLowerCase()) / examine_words_array[examine_word_counter].length);

										if( distance_length < threshold || parseFloat(threshold) === 0 || typeof(threshold) === 'undefined'){
											if(debug){
												console.log("Support word matched: " + match_string.toLowerCase() + " ( "+ content_string.toLowerCase() +" )");
											}

											var rankModifier = 0.05;
											rankModifier += match_string.length / 3;

											if(debug){
												console.log('Rank of '+support_sets[support_sets_counter]['name']+' will move from '+support_sets[support_sets_counter]['rank']+' to '+(support_sets[support_sets_counter]['rank'] / rankModifier));
											}

											support_sets[support_sets_counter]['rank'] = support_sets[support_sets_counter]['rank'] / rankModifier;
											
											// store info about the match
											
	/*
											// support_sets[support_sets_counter]['support_hits'][support_set['name']].push(

											if(typeof(support_sets[support_sets_counter]['support_hits'][support_set['name']]) === 'undefined'){
												all_support_hits[support_set['name']] = [];
											}
											all_support_hits[support_set['name']].push(
												{
													'matched': match_string
													, 'text': content_string
													, 'lineIndex': examine_line_counter
													, 'offset': examine[examine_line_counter].text.toLowerCase().indexOf(content_string)
													, 'strength': distance_length
												}
											);
	*/

											// get the index of this line into a global in case there is recursion
											hit_indexes = arrayUnique([examine_line_counter],hit_indexes);
										}
									}
								}
							}
						}
						
						// sort ranklist
						support_sets.sort(function(a, b) { 
							return a.rank - b.rank;
						});

						// console.log('POST-SUPPORT & SORT: ');
						// console.dir(support_sets);

						var top_supported_match = support_sets[0];
						// top_supported_match['support_hits'] = all_support_hits[support_sets[0]['name']];

						if(typeof(foundData[expectation_name]) === 'undefined'){
							foundData[expectation_name] = [];
						}

						// if there's a 'formatfunction' parameter, use it on the result now
						if(expectation_parameters.indexOf('formatFunction') >= 0){
							top_supported_match = expect[expi][expectation_names[0]].formatFunction(top_supported_match);
						}

						// add to 'found'
						foundData[expectation_name].push({
							'content':top_supported_match['name'],
							'hits':top_supported_match['support_hits']
						});
						
					} else {
						// no support sets/hits
						if(Array.isArray(foundData[expectation_name])){
							foundData[expectation_name].push({
								'content':'-',
								'hits':0
							});
						}
					}

				} // 'supported_match_objects' end

				// the for/in doesn't track the key name, so increment it
				expectation_name_index ++;
			}
		}
	} else {
		console.log('ppp failed, bad params');
		if(typeof(examine) == 'undefined'){
			console.log('no examine');
		}
		if(typeof(expect) == 'undefined'){
			console.log('no expect');
		}
		return 1;
	}

	return foundData;

}
function levenshtein(s, t) {
    // Step 1
    var n = s.length;
    var m = t.length;

    if (n == 0) return m;
    if (m == 0) return n;
    if (s == t) return 0;

    //Create an array of arrays in javascript (a descending loop is quicker)
    var d = new Array(n + 1);

    // Step 2
    for (var i = n; i >= 0; i--) d[i]    = [i];
    for (var j = m; j >= 0; j--) d[0][j] = j;

    // Step 3
    for (var i = 1; i <= n; i++) {
        var s_i = s[i - 1];

        // Step 4
        for (var j = 1; j <= m; j++) {

            //Check the jagged ld total so far
            if (i == j && d[i][j] > 4) return n;

            var t_j = t[j - 1];
            var cost = (s_i === t_j) ? 0 : 1; // Step 5

            //Calculate the minimum
            var mi = d[i - 1][j] + 1;
            var b = d[i][j - 1] + 1;
            var c = d[i - 1][j - 1] + cost;

            if (b < mi) mi = b;
            if (c < mi) mi = c;

            d[i][j] = mi; // Step 6 
        }
    }

    // Step 7
    return d[n][m];
}

function arrayUnique(array) {
    var a = array.concat();
    for(var i=0; i<a.length; ++i) {
        for(var j=i+1; j<a.length; ++j) {
            if(a[i] === a[j])
                a.splice(j--, 1);
        }
    }

    return a;
};