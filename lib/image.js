
exports = image = {
	rotate : function (source_file, dest_file, callback){
		// TODO: refactor to image.rotate
		var rotate_cmd = "convert " + source_file + " -rotate 90 " + dest_file;
		exec(rotate_cmd, function (err, stdout, stderr){
			if(err){
				console.log("Clean "+ source_file +" failed!");
				callback(1);
			} else {
				callback(0);
			} // end if/else(err)
		}); // execute rotate
	},
	clean : function (source_file, dest_file, callback){
		// TODO: refactor to image.clean

		// Super-simple version:
		// reduce colourset and 'specklyness' with median:
		// convert 28tfh2r.jpg -median 1 out.tif

		var clean_cmd = "/home/payprd/bin/textcleaner.sh" // + source_file +" " + dest_file;

		// console.log('Start cleaning ' + target_path + '...');
		/*
		var spawn = require('child_process').spawn,
			cln  = spawn('/home/payprd/bin/textcleaner.sh', [source_file +" " + dest_file]);

		console.log('Spawned child pid: ' + cln.pid);
		cln.stdin.end();
		*/
		var execFile = require('child_process').execFile,
		child;
		var exec = require('child_process').exec,
		cp;
		
		/*
		child = execFile(clean_cmd, source_file +" "+ dest_file, '', function (err, stdout, stderr){
			if(err){
				console.log("Clean "+ source_file +" failed!");
				return (callback(false));
			} else {
				return (callback(true));
			} // end if/else(err)
		}); // textcleaner
		*/
		child = execFile(clean_cmd, source_file +" "+ dest_file, '', function (err, stdout, stderr){
			if(err){
				console.log("Clean "+ source_file +" failed!");
				return (callback(false));
			} else {
				return (callback(true));
			} // end if/else(err)
		}); // textcleaner
		
	},
	scan : function (userId, imagechecksum, filename, imagefile, callback){
		// TODO: refactor to image.scan
		var temptextfilename = imagefile + new Date().getTime();
		console.log('Start reading' + temptextfilename + ' into .tif...');

		var tess_cmd="tesseract /home/payprd/uploads/"+ userId +"/_cleaned/"+ filename +" /tmp/"+ temptextfilename +" -l deu nobatch letters_deu 1>/dev/null 2>&1 && cat /tmp/"+temptextfilename+".txt && rm -f /tmp/"+temptextfilename+".txt";

		console.log(tess_cmd);
		exec(tess_cmd, function (error, stdout, stderr){
			var lines = stdout;
			
			var linesArray = stdout.match(/.+/g);
			
			if(typeof(linesArray)!=='undefined'){
				// console.log('Got '+linesArray.length +' lines');
				if(typeof(linesArray) !== 'undefined' && linesArray != null){
					if(linesArray.length > 0){
						var lineData = [];
						var index = 0;
						linesArray.forEach(function(this_field) {
							lineData.push({'index':index,'text':this_field,'flags':[]});
							index++;
							
						});
					}
				}
			}
			// trip.update(userId, imagechecksum, {textresult: lines, $addToSet: {'lines':{$each: linesArray }}, $addToSet: {'lineData':{$each: lineData }}}, function(){
			trip.update(userId, imagechecksum, {textresult: lines, $addToSet: {'lines':{$each: linesArray }}, 'lineData': lineData }, function(){
				// old style before addToSet: {$push: {'lines':{$each: lines }}}
				
				console.log("Updated lines in trip: "+ imagechecksum + ", reading...");
				
				// trip.read(req.user.id, imagechecksum);
				callback(userId, imagechecksum);
				
			}); // add lines to trip
		}); // tesseract-ocr
	},
	addStamp : function(source_file, dest_file, stamp_path, callback){
		// TODO: refactor to image.addStamp
		// 'Stamp' the image given under stamp_path on the bottom right of the image
		console.log("Stamping "+ source_file +" with "+ stamp_path);

		var stamp_cmd = "composite -gravity SouthEast "+stamp_path+" "+source_file+": "+dest_file;
		// TODO: refactor to image.addStamp
		exec(stamp_cmd, function (err, stdout, stderr){
			if(err){
				console.log("addStamp "+ source_file +" failed!");
				console.log(stderr +", "+stdout);
				console.log(stamp_cmd);
				callback(1);
			} else {
				callback(0);
			} // end if/else(err)
		});
	},
	makeChecksum : function(imagefile, callback){
		// TODO: refactor to image.makeChecksum
		var imagechecksum = '';

		// Alternative method
		// TODO: test+benchmark this
		/*
			var crypto = require('crypto');
			var fs = require('fs');

			var algo = 'md5';
			var shasum = crypto.createHash(algo);

			var file = './kitten.jpg';
			var s = fs.ReadStream(file);
			s.on('data', function(d) { shasum.update(d); });
			s.on('end', function() {
				var d = shasum.digest('hex');
				console.log(d);
			});
		*/

		checksum.file(imagefile, function (err, sum) {
			// hash the checksum + include a salt to show the checksum was made by the app
			imagechecksum = (crypto.createHash("sha1")
			.update('0iiImAgESaLTtt0' + sum)
			.digest("hex"));
			// console.log("Sum: "+sum);
			callback(imagechecksum);
		});
	},
	makeThumbnail : function(source_file, dest_file, dimensions, callback){
		// TODO: refactor to image.makeThumbnail
		// Make a thumbnail
		console.log("Thumbing "+ source_file +"...");
		var thumb_cmd = "convert -size "+dimensions+" "+source_file+" -thumbnail 80x80 "+dest_file;
		exec(thumb_cmd, function (err, stdout, stderr){
			if(err){
				console.log("makeThumbnail "+ source_file +" failed!");
				console.log(stderr +","+stdout);
				console.log(thumb_cmd);
				callback(1);
			} else {
				callback(0);
			} // end if/else(err)
		});
	},
}