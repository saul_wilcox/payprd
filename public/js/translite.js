/*
Supersimple DOM textnode translator

Join some jsons together with language content
and then just run this function to replace text
nodes with matching ids in an html/xml tree

Rules:
id only, no classes
textnodes + inputs(with "value") only
*/

function transLite(l, m){
  /* replaces in html/DOM tree */
  /* get a json full of language content */
  $.getJSON( "public_params.json", function( data ) {
    /*  module(m): 'shared' or '[name of module]' */
    if(typeof(m) === 'undefined' || m.length <= 1 ){
      m = 'shared';
    }
    console.log("---GO---");
    htmlTree(null, data, l, m);
  });
}

function htmlTree(obj, data, l, m){
    var obj = obj || document.getElementsByTagName('body')[0];
    
    /* var str = "<ul><li> "+obj.id+" L:"+l+" M:"+m; */
    if(data['lang'][l][m][obj.id]){
        
        console.log(obj.id+" is translatable");
        var newValue = data['lang']['en'][m][obj.id];
        // typeof(data['lang'][l][m][obj.id]) !== 'undefined' && data['lang'][l][m][obj.id] !== null
        if(data['lang'][l][m][obj.id]){
        	newValue = data['lang'][l][m][obj.id];
        }

        if(obj.tagName == "INPUT"){
        	console.log(obj.value);
        	obj.value = newValue;
        } else {
        	for (var n = obj.childNodes.length; n--;) {
        		obj.childNodes[n].nodeValue = newValue;
        		console.log(obj.childNodes[n].nodeValue);
		        /*
		        if(typeof(obj.childNodes[n].nodeValue) !== 'undefined' && obj.childNodes[n].nodeValue !== null){
		        	/* console.log(obj.childNodes[n].nodeValue); * /
		        	
		        }
		        */
	        }
        }
    }

    if (obj.hasChildNodes()) {
        var child = obj.firstChild;
        while (child) {
            if (child.nodeType === 1) {
              htmlTree(child, data, l, m)
            }
            child = child.nextSibling;
        }
    }
}

