var mongoose = require('mongoose')
	, tools = require('./tools.js')
	, t = require('./token.js')
	, http = require('http');

var userTripMetaSchema = new mongoose.Schema({
	user: { type: String, required: true},
	imagefile: { 
		type: String 
	},
	imagechecksum: { 
		type: String,
		required: true,
		unique: true
	},
	tags: Array,
	lines: Array,
	add_date: {
		type: Date,
		"default": Date.now
	},
	date: {
		type: Date
	},
	location: {
		type: String
	},
	total: {
		type: String,
	},
	claims: Array
});