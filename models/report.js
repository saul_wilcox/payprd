var mongoose = require('mongoose')
	, tools = require('./tools.js')
	, t = require('./token.js')
	, http = require('http');

var priceReportSchema = new mongoose.Schema({
	city: {
		type: String
	},
	date: {
		type: Date,
		"default": Date.now
	},
	trip: { 
		type: String,
		required: true
		},
	product_id: {
		type: String
	},
	merchant_id: {
		type: String
	},
	price: {
		type: Number,
		required: true
	},
	unitType: {
		type: String
	}
});

exports = priceReport = {
	
}