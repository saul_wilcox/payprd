var mongoose = require('mongoose')
	, app_params = require('../public/public_params.json')
	, conf = require('../_conf_protected')
	, tools = require('./tools.js')
	, mail = require("nodemailer").mail
	, SimpleTimestamps = require( 'mongoose-simpletimestamps' ).SimpleTimestamps
	, http = require('http');

var messageSchema = new mongoose.Schema({
	tags: Array,
	add_date: {
		type: Date,
		"default": Date.now
	},
	name: { 
		type: String 
	},
	email: { 
		type: String 
	},
	topic: { 
		type: String 
	},
	message: { 
		type: String 
	},
	recipient: { 
		type: String,
		"default": "payprd"
	}
});

exports = message = {
	save : function(user, name, email, topic, message, callback){
		var Message = mongoose.model('Message', messageSchema);
		
		var newMessage = new Message({
			name: name,
			email: email,
			topic: topic,
			message: message
		});

		newMessage.save(function(err) {
			if(err){
				console.log("Couldn't save message");
				callback(err, 0);
			} else {
				console.log('Saved message ' + newMessage._id);
				callback(false, newMessage._id)
			}
		});
	},
	send : function(id, callback){
		console.log('Sending message '+id);
		
		var Message = mongoose.model('Message', messageSchema);
		
		Message.findOne({ '_id' : id }, function(err, messageDoc) {
			
			mail = require("nodemailer").mail;
							
			var nodemailer = require("nodemailer");
			
			var transport = nodemailer.createTransport("SMTP", {
				host: "smtp.gmail.com", // hostname
				secureConnection: true, // use SSL
				port: 465, // port for secure SMTP
				auth: {
					user: app_params['admin_contact_email'],
					pass: "-DUMMY-"
				}
			});
			
			mail({
				from: messageDoc.email,
				to: app_params['admin_contact_email'],
				subject: messageDoc.topic,
				text: messageDoc.message,
				html: '<table style="width: 640px; text-align: center;"><tbody><tr style="background-color: rgb(128, 0, 133);" ><td><img src="https://www.payprd.com/images/payprd_applogo.png" id="app-logo" style="background-image: url(https://www.payprd.com/images/payprd_applogo.png); width: 120px; height: 40px; display: inline-block; background-position: 0; background-repeat: no-repeat;"></td></tr></tbody></table><br><br>From:' + messageDoc.name + '<br><br>' + messageDoc.message,
			});
			
			callback(false);
		});
	}
}
