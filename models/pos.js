var mongoose = require('mongoose')
	, tools = require('./tools.js')
	, t = require('./token.js')
	, http = require('http');

var posSchema = new mongoose.Schema({
	seller: {
		type: String
	},
	name: {
		type: String
	},
	street: {
		type: String
	},
	streetName: {
		type: String
	},
	streetNumber: {
		type: String
	},
	zip: {
		type: String
	},
	city: {
		type: String 
	},
	locale: { 
		type: String 
	},
	status:{
		type: Number,
		"default": 0
	},
	source: {
		type: String
	},
	add_date: {
		type: Date,
		"default": Date.now
	}
});

exports = pos = {
	upsertPos : function (seller,name,streetName,zip,city,locale,source,callback){
		name = name.toLowerCase();
		if(typeof(locale) != 'undefined' || locale == ''){
			locale = 'de_DE';
		}

		var Pos = mongoose.model('Pos', posSchema);

		var d = new Date();

		Pos.findOneAndUpdate(
			{ $and: [	{'seller': seller.toLowerCase()},
						{'name': name.toLowerCase()},
						{'streetName': streetName.toLowerCase()},
						{'zip': zip.toLowerCase()}, 
						{'city': city.toLowerCase()}, 
						{'locale': locale}
					]
			},
			{ $set: { 
					'seller': seller.toLowerCase(), 
					'name': name.toLowerCase(), 
					'streetName': streetName.toLowerCase(), 
					'zip': zip.toLowerCase(), 
					'city': city.toLowerCase(),
					'locale': locale,
					'status': -1,
					'source': source,
					'add_date': d
					} 
			}, 
			{ upsert: true, "new": false }, 
			function(err, newPos){
				if (err){ console.log(err); return (err);}
				// console.log("findOneAndUpdate POS: "+newPos);
				callback(newPos);
			}
		);
	},
	getCityPos : function (locales, city, glue, callback){
		var Pos = mongoose.model('Pos', posSchema);
		
		// console.log("getCityPos "+locales +" / "+ city +" / "+ glue);
		
		// Pos.find({ 'locale' : {$in : locales}, 'city' : {$in : city} },  null, {sort: {add_date: -1}}, function(err, cityPos) {
		Pos.find({},  null, {sort: {add_date: -1}}, function(err, cityPos) {
			if (err) return callback(err, null);
			//console.log("getCityPos - Found " + cityPos.length + " POS");
			
			// cheap shot join e.g. with | for throwing into a regex
			if(typeof(glue) !== 'undefined' && glue !== ''){
				// console.log('glue!');
				var items_flat=new Array();
				for (var i=0, l=cityPos.length; i < l; i++){
					if(i===0){
						cityPos_flat+=cityPos[i]['name'];
					}else{
						cityPos_flat+=glue+cityPos[i]['name'];
					}
				}
				callback(err, cityPos_flat);
			} else {
				callback(err, cityPos);
			}
		});
	},
	getItemList : function (locales, city, zip, glue, callback){
		var Pos = mongoose.model('Pos', posSchema);
		
		// console.log("getCityPos "+locales +" / "+ city +" / "+ glue);
		
		// Pos.find({},  null, {sort: {add_date: -1}}, function(err, cityPos) {
		Pos.find({ 'locale' : {$in : locales}, 'city' : {$in : city} },  null, {sort: {add_date: -1}}, function(err, cityPos) {
			if (err) return callback(err, null);
			//console.log("getCityPos - Found " + cityPos.length + " POS");
			
			// cheap shot join e.g. with | for throwing into a regex
			if(typeof(glue) !== 'undefined' && glue !== ''){
				// console.log('glue!');
				var items_flat=new Array();
				for (var i=0, l=cityPos.length; i < l; i++){
					if(i===0){
						cityPos_flat+=cityPos[i]['name'];
					}else{
						cityPos_flat+=glue+cityPos[i]['name'];
					}
				}
				callback(err, cityPos_flat);
			} else {
				callback(err, cityPos);
			}
		});
	}
}