// generic tools
exports = toolkit = {
	extract_tags : function(tags) {
		var cleaned = [];
		if(typeof(tags)=='undefined'){ return cleaned;}
		var tags_array = tags.split(/[\s,;#]+/);
		for (var i = 0; i < tags_array.length; i++) {
			if ((cleaned.indexOf(tags_array[i]) == -1) && tags_array[i] != "") {
				cleaned.push(tags_array[i].replace(/\s/g,''));
			}
		}
		return cleaned;
	},
	format_date : function(d){
		var cur_d = d.getDate();
		var cur_m = d.getMonth() + 1; //Months are zero based
		var cur_y = d.getFullYear();
		// var s=locale.datesp;
		var s='.';
		return (cur_d + s + cur_m + s + cur_y);
	}
},
lev = function(a,b) {
	var cost;
	
	// get values
	var m = a.length;
	var n = b.length;
	
	// make sure a.length >= b.length to use O(min(n,m)) space, whatever that is
	if (m < n) {
		var c=a;a=b;b=c;
		var o=m;m=n;n=o;
	}
	
	var r = new Array();
	r[0] = new Array();
	for (var c = 0; c < n+1; c++) {
		r[0][c] = c;
	}
	
	for (var i = 1; i < m+1; i++) {
		r[i] = new Array();
		r[i][0] = i;
		for (var j = 1; j < n+1; j++) {
			cost = (a.charAt(i-1) == b.charAt(j-1))? 0: 1;
			r[i][j] = mim(r[i-1][j]+1,r[i][j-1]+1,r[i-1][j-1]+cost);
		}
	}
	
	return r[m][n];
},
mim = function(x,y,z) {
	if (x < y && x < z) return x;
	if (y < x && y < z) return y;
	return z;
}

