var passport = require('passport')
	, conf = require('../_conf_protected')
	, app_params = require('../public/public_params.json')
	, LocalStrategy = require('passport-local').Strategy
	, GoogleStrategy = require('passport-google-oauth').OAuth2Strategy
	, LinkedInStrategy = require('passport-linkedin-oauth2').OAuth2Strategy
	, WindowsLiveStrategy = require('passport-windowslive').Strategy
	, bcrypt = require('bcrypt')
	, crypto = require('crypto')
	, SALT_WORK_FACTOR = 10
	, mongoose = require('mongoose')
	, SimpleTimestamps = require( 'mongoose-simpletimestamps' ).SimpleTimestamps
	, t = require('./trip.js')
	, exec = require("child_process").exec
	, fs = require('fs')
	, path = require('path')
	, mail = require("nodemailer").mail;

// User Schema
var userSchema = mongoose.Schema({
	country: { type: String, default: 'de' },
	lang: { type: String, default: 'en' },
	username: { type: String, required: true, unique: true },
	email: { type: String, unique: true }, // , required: true
	password: { type: String, required: true},
	roles: { type: Array },
	cities: { type: Array },
	profiledata: {},
	activation: { type: String, required: true},
	preferences: { type: Object, required: true, default: 
		{
		'ui_language' : "en"
		,'ui_resultsPerPage' : 10
		,'ui_layout' : "list"
		}
	}
});

userSchema.plugin(SimpleTimestamps);

// Bcrypt middleware
userSchema.pre('save', function(next) {
	var user = this;

	if(!user.isModified('password')) return next();

	bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
		if(err) return next(err);

		bcrypt.hash(user.password, salt, function(err, hash) {
			if(err) return next(err);
			user.password = hash;
			next();
		});
	});
});

// Password verification
userSchema.methods.comparePassword = function(candidatePassword, cb) {
	bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
		if(err) return cb(err);
		cb(null, isMatch);
	});
};

var User = mongoose.model('User', userSchema);

passport.serializeUser(function(user, callback) {
  callback(null, user.id);
});

passport.deserializeUser(function(id, callback) {
	User.findById(id, function(err, user) {
		callback(err, user);
	});
});

passport.use(new LocalStrategy({passReqToCallback: true},function(req, username, password, callback) {
	User.findOne({ username: username }, function(err, user) {
		if (err) { return callback(err); }
		if (!user) {
			return callback(false, 'bad_user');
		}
		user.comparePassword(password, function(err, isMatch) {
			if (err){
				console.log('Error checking password');
				return callback(err, 'error');
			}
			if(isMatch) {
				console.log('Password OK.');
				if(user.activation === '0'){
					return callback(null, user);
				} else {
					return callback(false, 'inactive');
				}
			} else {
				return callback(false, 'bad_pw');
			}
		});
	});
}));

passport.use(new GoogleStrategy({
	passReqToCallback: true,
	clientID: conf.google_oauth_appid,
	clientSecret: conf.google_oauth_secret,
	callbackURL: "http://" + conf.webhost + "/auth/google/callback",
	scope: "https://www.googleapis.com/auth/userinfo.email"
},	
function(req, accessToken, refreshToken, profile, done) {
	process.nextTick(function() {
		
	var email =	profile.emails[0].value;

		var User = mongoose.model('User', userSchema);
        User.findOne({ 'email' : email }, function(err, userSearch) {
            if (err)
                return done(err);
            if (userSearch) {

				req.logIn(userSearch, function(err) {
					if (err) { return next(err); }
					return done(null, userSearch);
				});

            } else {
				user.externalLogin(req, email, profile, '', function(err, user) {
					if (err) { return next(err); }
					return done(null, user);
				});
            }
        });
    });
}
));

passport.use(new LinkedInStrategy({
	passReqToCallback: true,
	clientID: conf.linkedin_oauth_appid,
	clientSecret: conf.linkedin_oauth_secret,
	callbackURL: conf.webhost+"/auth/linkedin/callback",
	scope: ["r_emailaddress", "r_basicprofile"],
},	
function(req, accessToken, refreshToken, profile, done) {
	process.nextTick(function() {
		var email =	profile.emails[0].value;
		var User = mongoose.model('User', userSchema);
        User.findOne({ 'email' : email }, function(err, userSearch) {
            if (err)
                return done(err);
            if (userSearch) {
				req.logIn(userSearch, function(err) {
					if (err) { return next(err); }
					return done(null, userSearch);
				});
            } else {
				user.externalLogin(req, email, profile, '', function(err, user) {
					if (err) { return next(err); }
					return done(null, user);
				});
            }
        });
    });
}
));

passport.use(new WindowsLiveStrategy({
	passReqToCallback: true,
	clientID: conf.windows_oauth_appid,
	clientSecret: conf.windows_oauth_secret,
	callbackURL: conf.webhost+"/auth/windowslive/callback",
},	
function(req, accessToken, refreshToken, profile, done) {
	process.nextTick(function() {
		var email =	profile.emails[0].value;
		var ext_locale = profile._json.locale;
		console.log(ext_locale);
		var User = mongoose.model('User', userSchema);
        User.findOne({ 'email' : email }, function(err, userSearch) {
            if (err)
                return done(err);
            if (userSearch) {
				req.logIn(userSearch, function(err) {
					if (err) { return next(err); }
					return done(null, userSearch);
				});
            } else {
				user.externalLogin(req, email, profile, ext_locale, function(err, user) {
					if (err) { return next(err); }
					return done(null, user);
				});
            }
        });
    });
}
));

exports = user = {
	tokenRequest: function(req, res, callback) {
		console.log("user.tokenRequest...");
		passport.authenticate('local', function(err, _user) {
			if (err) { 
				return null; 
			}
			req.logIn(_user, function(err) {
				if (err) {
					res.status(403).send('403');
					return callback(err); 
				} else {
					res.type('application/json');
					res.status(200).send(user.getApiToken(_user.id));
				}
			});
		})(req, res, callback);
	},
	getApiToken: function(userId) {
		console.log("Getting API token for user ...");

		var apiKey = (crypto.createHash("sha1")
			.update(userId + conf.appSecret + conf.apiKeySalt)
			.digest("hex"));

		var api_access = {
			userId: ''+userId,
			apiKey: ''+apiKey,
			token: ''+token.makeTimedToken('api', userId)
		}
		return api_access;
	},
	locallogin: function(req, res, callback) {
		passport.authenticate('local', function(err, user) {
			// return callback(err);
			console.log('User login check: ');
			console.dir(user);
			if(err) {
				return callback('Error logging in.',false);
			} else if (!user) {
				return callback("User and password don't match.",false);
			} else if (user == 'inactive') {
				return callback('Not activated yet, check your email for an activation link.',false);
			}
			req.logIn(user, function(err) {
				if (err) {
					return callback("User and password don't match.",false);
				} else {
					return res.redirect('/my');
				}
			});
		})(req, res, callback);
	},
	logout: function(req, res){
		req.logout();
		res.redirect('/');
	},
	externalLogin:function(req, em, profile, ext_locale, callback){
		var locale = 'en_US';
		if(ext_locale != ''){
			locale = ext_locale;
		}
		
		var rnd_p = Math.floor(Math.random() * 90000) + 10000;
		var dum_pw = (crypto.createHash("sha1")
			.update(em + conf.appSecret + rnd_p)
			.digest("hex"));
		
		var newUser = new User({ 'locale': locale, username: em, email: em, password: dum_pw, cities: [], profiledata: profile, activation: 0});
		newUser.save(function(err) {
			if(err) {
				console.dir(err);
				var ui_error_message = "Couldn't save this user.";
				if (11000 === err.code || 11001 === err.code) {
					ui_error_message = "The username or email is already taken.";
				}
				return done(err, err.code + ": Duplicate");
			} else {
				console.log('User: ' + newUser.username + ' / ' + newUser._id.toString() + " saved.");
				console.log(newUser._id.toString());
				var newUserid = newUser._id.toString();
				user.createFolders(newUserid);
				req.logIn(newUser, function(err) {
					if (err) { callback(err, newUser); }
					callback(err, newUser);
				});
			}
		})
	},
	create: function(username, password, passwordconf, email, city, res, callback){
		console.log('Join attempt:'+username);
		if (!username || username.length < 8) {
			console.log('Join aborted - short username');
			ui_error_message = app_params['lang']['en']['shared']['ui_error_usernameTooShort'];
			callback(ui_error_message, false);
		} else if (!password || password.length < 9) {
			console.log('Join aborted - short password');
			ui_error_message = app_params['lang']['en']['shared']['ui_error_passwordTooShort'];
			callback(ui_error_message, false);
		} else if(password !== passwordconf){
			console.log('Join aborted - password mismatch');
			ui_error_message = app_params['lang']['en']['shared']['ui_error_passwordMismatch'];
			callback(ui_error_message, false);
		} else if(username.length > 7 && password.length > 8 && password === passwordconf ){
			var User = mongoose.model('User', userSchema);
			// Simple city->locale
			// TODO: Move to public params
			var locale = 'en_US';
			if(city == 'berlin' || city == 'nuernberg'){
				locale = 'de_DE';
			}
			
			var rnd_c = Math.floor(Math.random() * 990) + 9;
			var activate_c = (crypto.createHash("sha1")
				.update(username + conf.appSecret + rnd_c)
				.digest("hex"));
			
			var newUser = new User({ 'locale': locale, username: username, email: email, password: password, cities: [city], activation: activate_c });

			newUser.save(function(err) {
				if(err) {
					console.dir(err);
					var ui_error_message = "Couldn't save this user.";
					ui_error_message = app_params['lang']['en']['shared']['ui_error_errorSavingUser'];
					if (11000 === err.code || 11001 === err.code) {
						ui_error_message = app_params['lang']['en']['shared']['ui_error_usernameTaken'];
					}
					res.render('join', { title: 'Join', ui_error: ui_error_message});
				} else {
					console.log('User: ' + newUser.username + " saved.");
					var activationlink = "https://www.payprd.com/activate?u="+ newUser.id +'&c='+ activate_c;
					console.log(activationlink); // 'https://www.payprd.com/activate?u='+ newUser.id +'&c='+ activate_c
					if(true){
						
						console.log("Sending welcome mail...");
						mail = require("nodemailer").mail;
						
						var nodemailer = require("nodemailer");
						
						var transport = nodemailer.createTransport("SMTP", {
							host: "smtp.gmail.com", // hostname
							secureConnection: true, // use SSL
							port: 465, // port for secure SMTP
							auth: {
								user: "gmail.user@gmail.com",
								pass: "userpass"
							}
						});
						
				/*
				// create reusable transport method (opens pool of SMTP connections)
				var smtpTransport = nodemailer.createTransport("SMTP",{
					service: "Gmail",
					auth: {
						user: "gmail.user@gmail.com",
						pass: "userpass"
					}
				});

				// setup e-mail data with unicode symbols
				var mailOptions = {
					from: "Fred Foo ✔ <foo@blurdybloop.com>", // sender address
					to: "bar@blurdybloop.com, baz@blurdybloop.com", // list of receivers
					subject: "Hello ✔", // Subject line
					text: "Hello world ✔", // plaintext body
					html: "<b>Hello world ✔</b>" // html body
				}

				// send mail with defined transport object
				smtpTransport.sendMail(mailOptions, function(error, response){
					if(error){
						console.log(error);
					}else{
						console.log("Message sent: " + response.message);
					}

					// if you don't want to use this transport object anymore, uncomment following line
					//smtpTransport.close(); // shut down the connection pool, no more messages
				});
				*/
						
						mail({
							from: "payprd Support <support@payprd.com>",
							to: email,
							subject: "Activate your account",
							text: 'Hello,\r\n\An account was created at payprd with this email used in the registration.\r\n\r\nThe username was: '+username+'\r\n\To activate the account, use this link:'+ activationlink +'\r\n\r\nBye for now!\r\n\r\nThe payprd robots.',
							html: '<table style="width: 640px; text-align: center;"><tbody><tr style="background-color: rgb(128, 0, 133);" ><td><img src="https://www.payprd.com/images/payprd_applogo.png" id="app-logo" style="background-image: url(https://www.payprd.com/images/payprd_applogo.png); width: 120px; height: 40px; display: inline-block; background-position: 0; background-repeat: no-repeat;"></td></tr></tbody></table><br><br>Hello,<br><br>An account was created at payprd with this email used in the registration.<br><br>The username was:<br><br><strong> '+username+' </strong><br><br>To activate the account, use this link: <br><br><a href="'+activationlink+'" target="_blank" >'+activationlink+'</a><br><br>Bye for now!<br><br><br>The payprd robots.',
						});
						/*
						text: "Hello,\r\n\An account was created at payprd with this email used in the registration.\r\n\r\nThe username was: '+username+'\r\n\To activate the account, use this link: https://www.payprd.com/activate?u='+ newUser.id +'&c='+ activate_c +'\r\n\r\nBye for now!\r\n\r\nThe payprd robots.", // plaintext body
							html: '<html><head></head><body><style type="text/css">body{line-height:100%}label{padding:1px}.appTop{width:100%;height:45px;text-align:center;}.purple{background:#800085}.mailbody{padding:2em 1em 1em 1em}#app-logo{background:url("https://www.payprd.com/images/payprd_applogo.png") no-repeat top left; width: 120px; height:40px; display:inline-block;}</style><div class="purple appTop"><div id="app-logo"></div></div><div class="mailbody">Hello,<br />\An account was created at payprd with this email used in the registration.<br /><br />The username was: '+username+'<br />\To activate the account, use this link: <a href="'+activationlink+'">activationlink</a><br /><br />Bye for now!<br /><br /><br />The payprd robots.</div><br /></body></html>'
						
						*/
					}
					console.log("User document timestamp: "+newUser.createdAt);
					return(callback('Check your email for a link to activate your account.', true));
				}
			})
		} else {
			ui_error_message = "The password is too short.";
			callback(ui_error_message, false);
		}
	},
	createFolders : function(userid){
		// TODO: Risky letting fs operations run async like this
		console.log('Creating folders for '+userid+'...');
		exec("mkdir /home/payprd/uploads/"+userid);
		exec("mkdir /home/payprd/uploads/"+userid+"/_thumbs");
		exec("mkdir /home/payprd/uploads/"+userid+"/_cleaned");
	},
	savePreferences : function(userid, prefs, callback){
		User.findByIdAndUpdate(userid, { $set: { preferences: prefs }}, function(err, userDoc) { // id, set, null, callback
			 if (err || !userDoc){
				console.log('savePreferences aborted - no user!');
				callback("Not logged in or invalid user.", false);
			} else {
				console.log('Updating preferences for user, id: '+userid);
				console.dir(userDoc.preferences);
				// langcontent = app_params['lang'][prefs['ui_language']];
				console.log('Updated ui language: '+prefs['ui_language']);
				// console.dir(langcontent);
				callback(0, true);
			}
		});
	},
	changePass : function(userid, oldPass, newPass, confPass, callback){
		User.findOne({ '_id' : userid }, function(err, userDoc) {
            if (err || !userDoc){
				console.log('Change aborted - no user!');
				callback("Not logged in or invalid user.", false);
			} else {
				userDoc.comparePassword(oldPass, function(err, isMatch) {
					if (err) return done(err);
					if(isMatch) {
						if(newPass.length > 8){	
							if(newPass === confPass){
								confPass = null;
								console.log("Trying to update password for UID:" + userid," FIELD: password");

								userDoc.password = newPass;

								userDoc.save(function(err) {
									if(err){
										callback(err);
									} else {
										console.log('Updated password for user, id: '+userid);
										// TODO: notify user
										// send a mail if required
										callback(0, true);
									}
								});
							} else {
								console.log('Change aborted - passwords different');
								ui_error_message = app_params['lang'][userDoc['preferences']['ui_language']]['shared']['ui_error_passwordMismatch'];
								callback(ui_error_message, false);
							}
						} else {
							console.log('Change aborted - password too short');
							ui_error_message = app_params['lang'][userDoc['preferences']['ui_language']]['shared']['ui_error_passwordTooShort'];
							callback(ui_error_message, false);
						}
					} else {
						console.log('Password does not match');
						ui_error_message = app_params['lang'][userDoc['preferences']['ui_language']]['shared']['ui_error_wrongPassword'];
						callback(ui_error_message, false);
					}
				});
            }
        });
	},
	activate : function(userid, check, callback){
		User.findOneAndUpdate(
			{ $and: [{'_id' : userid},{'activation' : check}]  },
			{ $set: {'activation': 0} }, 
			{ }, 
			function(err){
				if (err){
					console.log(err);
					callback('The user cannot be activated.', false);
				}
				user.createFolders(userid);
				console.log('Activated user: '+ userid);
				callback('Account activated - enter your username and password to get started.', true);
			}
		);
	},
	getUserTripTags : function (callback){
		var User = mongoose.model('User', userSchema);
		
		User.find({},  null, {sort: {add_date: -1}}, function(err, items) {
			if (err) return callback(err, null);
			console.log("getUsers - Found " + items.length + " users");
			callback(err, items);
		});
	},
	getLocaleUsers : function (locales, callback){
		var User = mongoose.model('User', userSchema);
		
		User.find({ 'locale' : {$in : locales} },  null, {sort: {add_date: -1}}, function(err, items) {
			if (err) return callback(err, null);
			console.log("getLocaleUsers - Found " + items.length + " users");
			callback(err, items);
		});
	},
	getUsers : function (callback){
		var User = mongoose.model('User', userSchema);
		
		User.find({},  null, {sort: {add_date: -1}}, function(err, items) {
			if (err) return callback(err, null);
			console.log("getUsers - Found " + items.length + " users");
			callback(err, items);
		});
	}
};
