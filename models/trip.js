var mongoose = require('mongoose')
	, conf = require('../_conf_protected')
	, app_params = require('../public/public_params.json')
	, crypto = require('crypto')
	, checksum = require('checksum')
	, ExifImage = require('exif').ExifImage
	, tools = require('./tools.js')
	, l = require('levenshtein')
	, t = require('./token.js')
	, i = require('./item.js')
	, r = require('./report.js')
	, path = require('path')
	, http = require('http')
	, exec = require("child_process").exec
	, fs = require('fs')
	, gm = require('gm');

var tripSchema = new mongoose.Schema({
	user: { 
		type: String, 
		required: true
	},
	imagechecksum: { 
		type: String,
		required: true,
		/* unique: true */ // Disabled during beta phase (will always be unique per-user)
	},
	imagefile: { 
		type: String 
	},
	tags: { 
		type:Array 
	},
	textresult: { 
		type:String 
	},
	lines: { 
		type:Array 
	},
	lineData: { 
		type:Array 
	},
	foundGenericData: { 
		type:Object 
	},
	foundLocationData: { 
		type:Object 
	},
	items: { 
		type:Array 
	},
	add_date: {
		type: Date,
		"default": Date.now
	},
	date:{
		type: Date
	},
	dateString:{
		type: String
	},
	url:{
		type: String
	},
	company:{
		type: String
	},
	vat_id:{ 
		type: String
	},
	tax_id:{
		type: String
	},
	seller:{
		type: String,
		"default": ''
	},
	city:{
		type: String,
		"default": ''
	},
	zips:{
		type: Array
	},
	streetName : {
		type: String,
		"default": ''	
	},
	streetNumber : {
		type: String,
		"default": ''	
	},
	status:{
		type: Number,
		"default": 0
	},
	location: {
		type: String
	},
	total: {
		type: Number,
	},
	exifdata : {},
	scanned: Array
});

exports = trip = tripModel = {
	/*
	add : function (req, res, callback){
		// TODO: if / else for catching text files
		trip.createFromImage(req, res, function(){
	
			// callback();
	
		});
	},
	*/
	checkDuplicate : function(userId, imagechecksum, res, context, callback){
		var Trip = mongoose.model('Trip', tripSchema);
		/* 'start'  check duplicate */
		Trip.find({ 'user' : userId, 'imagechecksum' : imagechecksum }, 'imagechecksum', {}, function(err, trips) {
			if (err) {
				callback(err, null);
			}
			if(trips.length > 0){
				// ui_error = 'Seems we already have this file.';
				
				// ui_error_alreadyuploaded = 'You already uploaded this file.';
				
				// error message example
				console.log(app_params['lang']['de']['ui_error_alreadyuploaded']);
				if(context === 'web-ui'){
					res.status(200).send(app_params['lang']['de']['ui_error_alreadyuploaded'])
				} else {
					res.send(200, 'Already exists.');
				}

				return(null);
			} else {
				console.log('Duplicate checksums found: '+trips.length);
				callback(trips.length);
				return true;
			} // end if duplicate
		}); // end find duplicate
	},
	createDocument : function (userId, imagechecksum, filepath, context, callback){ // text, userId, res, context, callback
console.log('createDocument... filepath: "'+ imagechecksum +'" (extension: '+path.extname(filepath).toLowerCase()+' )');

		var Trip = mongoose.model('Trip', tripSchema);
				
		console.log('Creating trip: '+ userId +'/'+imagechecksum+'...');
		var trip_added = new Date();

		var trip_tags = [];

		Trip.create({
			"user": userId,
			"imagefile": filepath,
			"imagechecksum": imagechecksum,
			"tags": trip_tags,
			"add_date": trip_added,
			"total":null,
			"exifdata": {} // exifData
		},
		function(err, tripDoc){
			if (err){ 
				console.log(err);
				if(context === 'web-ui'){
					res.send(302, app_params['lang']['de']['ui_error_alreadyuploaded']);
				} else {
					// res.send(302, app_params['lang']['de']['ui_error_alreadyuploaded']);
					callback(err, null);
				}
				// callback(err, null);
			} else {
				console.log("Inserted trip: " + imagechecksum + " id: " + tripDoc._id);
					
				callback(err, tripDoc);
			}
		}); // insert + callback
	},
	populateFromText : function (text, imagefile, userId, res, context, callback){
		console.log('populateFromText start '+ new Date());
		var imagename = '';

		if(imagefile.size == 0){
			// only continue if there is a folder to work with
			fs.exists('/home/payprd/uploads/'+ userId, function (userFolderExists) {
				if(userFolderExists === true){
					// do smth w file
				} else {
					console.log('No user folder!');
				}
			});
		}

		
		/*var checksum = (crypto.createHash("sha1")
			.update('1ttTExTSaLTtt1' + text)
			.digest("hex"));
		*/
		checksum.file(imagefile.path, function (err, sum) {
			// hash the checksum + include a salt to show the checksum was made by the app
			imagechecksum = (crypto.createHash("sha1")
			.update('0iiImAgESaLTtt0' + sum)
			.digest("hex"));
			console.log("Sum: "+imagechecksum);

			trip.createDocument(userId, imagechecksum, imagename, context, function(err, tripDoc){ // text, userId, res, context, callback
				// respond immediately over HTTP with something the requester can use to locate the resource
				if(!tripDoc && err){
					if(err.code === 11000 || err.code === 11001){
						// already exists
						// res.send(302, app_params['lang']['de']['ui_error_alreadyuploaded']);
						res.end(app_params['lang']['de']['ui_error_alreadyuploaded']);
					} else {
						res.end('Unknown error. REF: wx23ay981z507rn98e74z5g3j6t');
					}
				} else {
					res.send(200, JSON.stringify(tripDoc));
					// res.send(200, 'Created trip: ' + imagechecksum + ' id: ' + tripDoc._id);
					
					var linesArray = text.match(/.+/g);
					
					if(typeof(linesArray)!=='undefined'){
						console.log('Text: '+linesArray.length +' lines');
						if(typeof(linesArray) !== 'undefined' && linesArray != null){
							if(linesArray.length > 0){
								var lineData = [];
								var index = 0;
								linesArray.forEach(function(this_field) {
									lineData.push({'lineIndex':index,'text':this_field});
									index++;
								});
							}
						}
					}

					// CLEANUP
					var target_path = '/home/payprd/uploads/'+userId+'/'+ imagechecksum + path.extname(imagefile.path).toLowerCase();
console.log(imagefile.name);
					// Move file from temporary location to target location
					console.log('Moving: "'+ imagefile.path +'" (extension: '+path.extname(imagefile.path).toLowerCase()+' ) to: "'+ target_path +'"');

					var source = fs.createReadStream(imagefile.path);
					source.on('error', function(err) { 
						console.log('Move failed (read).');
						return(res.send(500, "File handling error")); 
					});
					
					var dest = fs.createWriteStream(target_path);
					dest.on('error', function(err) { 
						console.log('Move failed (write).');
						if(context === 'web-ui'){
							res.redirect('/add', {});
						}
					});
					
					// source.on('end', function() {
					dest.on('finish', function(err) {
						if(err){
							console.log('Move file '+ imagefile.path +' from tmp to uploads failed.');
						}
						var thumb_destination = "/home/payprd/uploads/"+ userId +"/_thumbs/"+ imagechecksum + ".png" // path.extname(imagefile.path).toLowerCase();
						trip.makeThumbnail(target_path, thumb_destination, '80x80', function(success){
							if(success === true){
								if(context === 'web-ui'){
									res.redirect('/my', {});
								} else {
									res.send(200, '{ "_id" : "'+tripDoc._id+'", "imagechecksum" : "'+imagechecksum+'"}');
								}

								// res.redirect('/my', {});
								
								// console.log('File moved to: '+ target_path +' - '+ _tf.size +' bytes');
								
								// Delete temp file

								fs.unlink(imagefile.path, function() {
									var filename = imagechecksum + path.extname(imagefile.path).toLowerCase(); // imagechecksum + path.extname(_tf.name).toLowerCase();
									
									var dest_path = "/home/payprd/uploads/"+ userId +"/_cleaned/"+ filename;
									console.log('prepare+scan...');
									trip.clean(target_path, dest_path, function(){
										
									}); // clean
								}); // end unlink
							} else {
								console.log('Failed to make thumbnail.');
								// res.redirect('/add', {});
							}
						}); // end thumb
					}); // end stream
					
					// Actually send
					source.pipe(dest);

					trip.update(userId, imagechecksum, {textresult: text, $addToSet: {'lines':{$each: linesArray }}, 'lineData': lineData }, function(){
						console.log("Updated lines in trip: "+ imagechecksum + ", reading...");
						trip.getFields(userId, imagechecksum, ['_id','user','lineData','imagechecksum'], function(err, tripDoc){
							if (err) {
								console.log('No text to parse - trip.getField error: ' + err, lineData);
								callback(true);
							} else {
								trip.parseText(tripDoc, function(success){
									res.send(200, 'DERP-00487567');
									if(success && context !== 'web-ui'){
										res.send(200, '');
									} else {
										res.send(502, 'Something went wrong.');
									}
								});
							} // end if/else trip.getfield err
						}); // end trip.getField
					}); // end update lines in trip
				} // end if/else document not false
			}); // end createDocument
		});

		
	}, 
	populateFromImage : function (imagefile, userId, res, context, callback){
		console.log('populateFromImage start '+ new Date());

		if(imagefile.size == 0){
			ui_error = {err:'File seems to be 0 bytes. Did you select one?'};
			if(context === 'web-ui'){
				res.send(200, ui_error);
			}
		}

		// var imagefile = path.basename(imagefile.path);
		
		// only continue if there is a folder to work with
		fs.exists('/home/payprd/uploads/'+ userId, function (userFolderExists) {
			if(userFolderExists === true){
				// check the metadata and see if it is rotated, then make checksum
				trip.getFileMetaData(imagefile.path, function(exifData){

					console.log('after getFileMetaData');

					trip.makeChecksum(imagefile.path, function(imagechecksum){

						console.log('after makeChecksum ('+imagechecksum+')');

						trip.checkDuplicate(userId, imagechecksum, res, context, function(duplicateCount){

							console.log('after checkDuplicate ('+duplicateCount+')');

							console.log('imagefile.path: '+imagefile.path);
							console.log('imagefile.path extension: '+path.extname(imagefile.originalname).toLowerCase());

							// Note that we now use the imagefile.name - the original name of this file before we checksum it
							trip.createDocument(userId, imagechecksum, imagefile.originalname, context, function(err, tripDoc){ // text, userId, res, context, callback

								console.log('after createDocument ('+tripDoc.imagechecksum+')');

								// CLEANUP
								var target_path = '/home/payprd/uploads/'+userId+'/'+ imagechecksum + path.extname(imagefile.originalname).toLowerCase();

								// Move file from temporary location to target location
								console.log('Moving: "'+ imagefile.path +'" (extension: '+path.extname(imagefile.path).toLowerCase()+' ) to: "'+ target_path +'"');

								var source = fs.createReadStream(imagefile.path);
								source.on('error', function(err) { 
									console.log('Move failed (read).');
									return(res.send(500, "File handling error")); 
								});
								
								var dest = fs.createWriteStream(target_path);
								dest.on('error', function(err) { 
									console.log('Move failed (write).');
									if(context === 'web-ui'){
										res.redirect(303, '/add')
									}
								});
								
								// source.on('end', function() {
								dest.on('finish', function(err) {
									if(err){
										console.log('Move file '+ imagefile.path +' from tmp to uploads failed.');
									}
									var thumb_destination = "/home/payprd/uploads/"+ userId +"/_thumbs/"+ imagechecksum + ".png" // path.extname(imagefile.path).toLowerCase();
									trip.makeThumbnail(target_path, thumb_destination, '80x80', function(success){
										if(success === true){
											if(context === 'web-ui'){
												res.redirect(303, '/my')
											} else {
												res.send(200, '{ "_id" : "'+tripDoc._id+'", "imagechecksum" : "'+imagechecksum+'"}');
											}

											// res.redirect('/my', {});
											
											// console.log('File moved to: '+ target_path +' - '+ _tf.size +' bytes');
											
											// Delete temp file

											fs.unlink(imagefile.path, function() {
												var filename = imagechecksum + path.extname(imagefile.path).toLowerCase(); // imagechecksum + path.extname(_tf.name).toLowerCase();
												
												var dest_path = "/home/payprd/uploads/"+ userId +"/_cleaned/"+ filename;
												console.log('prepare+scan...');
												trip.clean(target_path, dest_path, function(){
													trip.scan(userId, imagechecksum, filename, imagefile, function(){
														// the ui has already sent a response header, but an API call now needs to return something
														trip.getFields(userId, imagechecksum, ['_id','user','lineData','imagechecksum'], function(err, tripDoc){
															if (err) {
																console.log('No text to parse - trip.getField error: ' + err, lineData);
																callback(true);
															} else {
																trip.parseText(tripDoc, function(success){
																	/*
																	if(success && context !== 'web-ui'){
																		res.send(200, 'Cleaning, scanning and parsing completed. Have a 200.');
																	} else {
																		res.send(502, 'Something went wrong.');
																	}
																	*/
																});
															} // end if/else trip.getfield err
														}); // end trip.getField
														/* 
														if(context === 'web-ui'){
															trip.read(uid, imagechecksum);
														} else {
															res.send(200, 'Scanned.');
															trip.parseText(uid, imagechecksum);
														}
														*/
													}); // scan
												}); // clean
											}); // end unlink
										} else {
											console.log('Failed to make thumbnail.');
											// res.redirect('/add', {});
										}
									}); // end thumb
								}); // end stream
								
								// Actually send
								source.pipe(dest);

							}); // end createDocument
						}); // end checkDuplicate
					}); // end checksum
				}); // end get metadata
			} else {
				console.log('No user folder!');
				res.send(500, 'Error adding image');
				res.redirect(303, '/add')
			}
		});
	},
	scan : function (userId, imagechecksum, filename, imagefile, callback){
		// TODO: refactor to image.scan
		// console.log('imagefile: '+imagefile);
		// console.log('imagefile.name: '+imagefile.name);
		// console.log('filename: '+imagefile.path);

		var temptextfilename = "/tmp/"+ imagechecksum +"_" + new Date().getTime();
		console.log('Start reading ' + imagefile.name + ' into .tif...');

		var tess_cmd="tesseract /home/payprd/uploads/"+ userId +"/_cleaned/"+ filename +".tiff "+ temptextfilename +" -l deu nobatch letters_deu 1>/dev/null 2>&1 && cat "+temptextfilename+".txt && rm -f "+temptextfilename+".txt";

		console.log(tess_cmd);
		exec(tess_cmd, function (error, stdout, stderr){
			var lines = stdout;
			
			var linesArray = stdout.match(/.+/g);
			
			if(typeof(linesArray)!=='undefined'){
				// console.log('Got '+linesArray.length +' lines');
				if(typeof(linesArray) !== 'undefined' && linesArray != null){
					if(linesArray.length > 0){
						var lineData = [];
						var index = 0;
						linesArray.forEach(function(this_field) {
							lineData.push({'lineIndex':index,'text':this_field});
							index++;
						});
					}
				}
			}
			// trip.update(userId, imagechecksum, {textresult: lines, $addToSet: {'lines':{$each: linesArray }}, $addToSet: {'lineData':{$each: lineData }}}, function(){
			trip.update(userId, imagechecksum, {textresult: lines, $addToSet: {'lines':{$each: linesArray }}, 'lineData': lineData }, function(){
				// old style before addToSet: {$push: {'lines':{$each: lines }}}
				
				console.log("Updated lines in trip: "+ imagechecksum + ", reading...");
				
				// trip.read(req.user.id, imagechecksum);
				callback(userId, imagechecksum);
				
			}); // add lines to trip
		}); // tesseract-ocr
	},
	parseText : function (tripDoc, callback){
		var sample_city = 'berlin-de';

	    var sample_products = [ 'Speaker', 'Beans', 'Bohnen', 'Nutella', 'Tomaten', 'Gouda','Spaghetti'];
	    var not_products = [ 'Pfand', 'total'];

		var total_words = ["karte","EC","Gesamt","Gusautsunne","SUMME","S U M M E","Summe","Summe EUR","summe","total","T o t a l","netto","VISA","zahlen","zah1en","zu zahLen","zahLen","zen1en","ZWISCHENSUMME"];
		var total_identifiers = [
			{
				"name": "zahlen",
				"identifiers" : [
					"zu",
					"EUR"
				]
			},
			{
				"name": "EC karte",
				"identifiers" : [
					"e cash",
					"kartenzahlung",
					"nr"
				]
			},
			{
				"name": "VISA",
				"identifiers" : [
					"karte",
					"kartenzahlung"
				]
			},
			{
				"name": "gesamtbetrag",
				"identifiers" : [
					"EUR"
				]
			},
			{
				"name": "summe",
				"identifiers" : [
					"im EUR",
					"EUR"
				]
			},
			{
				"name": "s u m m e",
				"identifiers" : [
					"im EUR",
					"EUR"
				]
			},
			{
				"name": "gesamt",
				"identifiers" : [
					"im EUR",
					"EUR"
				]
			},
			{
				"name": "total",
				"identifiers" : [
					"EUR"
				]
			},
			{
				"name": "zahlung",
				"identifiers" : [
					"zu",
					"EUR"
				]
			},
			{
				"name": "betrag",
				"identifiers" : [
					"EUR"
				]
			},
			{
				"name": "ZWISCHENSUMME",
				"identifiers" : [
					"EUR"
				]
			}
		];

	    function trim(text){
	        // fast trimming
	        text = text.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
	        return text;
	    };

		var tax_pattern = '//'; // TODO
	    var house_number_pattern = new RegExp('[0-9]+[AaBcCc]?','g');
	    var float_pattern = new RegExp('([0-9]{1,4}[,\.][0-9]{1,4})','g');
	    var twoDigitFloat_pattern = new RegExp('[0-9]{1,4}[,\.][0-9]{2}','g');
	    // var date_pattern =  new RegExp('([0]?[1-9]|[1|2][0-9]|[3][0|1])[.-\/]([0]?[1-9]|[1][0-2])[-A-Z.\\\/]+(20[0-9]{2}|[0-9]{2})','g');
	    var date_pattern =  new RegExp('([0-9]{1,4}[.-\/][0-9]{1,2}[.-\/][0-9]{1,4})','g');
		var url_pattern = new RegExp('\b[a-zA-Z0-9][a-zA-Z0-9-_]{0,61}[a-zA-Z0-9]{0,1}\.([a-zA-Z]{1,6}|[a-zA-Z0-9-]{1,30}\.[a-zA-Z]{2,3})\b','g');
		var domain_pattern = new RegExp('\b[a-zA-Z0-9][a-zA-Z0-9-_]{0,61}[a-zA-Z0-9]{0,1}\.([a-zA-Z]{1,6}|[a-zA-Z0-9-]{1,30}\.[a-zA-Z]{2,3})\b','g');
		var email_pattern = new RegExp('\b[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}\b','g');

		// var priceLine_pattern_0 = new RegExp('(([\*A-Za-z0-9\.-]+[^\S\n])*?)(?:[0-9]*[^\S\n])*([0-9]{1,8}[,.][0-9]{2}?[ ]?[0-9]?)(?:([^\S\n]EUR|[^\S\n][NAB 1]{1,2}))*','g');
		// var priceLine_pattern_1 = '([\*A-Za-z0-9\. !-]+[^\S\n])+?([0-9]{1,8}[,.][0-9]{2})''
		var priceLine_pattern = new RegExp('(([-\.\*A-Za-z0-9 !]+)( [0-9]+[,.][0-9]{2}))','g');

		seller.getLocaleSellerNames(app_params.supported_locales, '', function(err, sellers){
			zip.findByCity(sample_city, function(err, found_city_zips){
				var zip_list = [];
				// console.log("CZ: ");console.dir(city_zips);
				for(var z=found_city_zips.length;z>0;z--){ 
					zip_list.push(found_city_zips[(z-1)].zip); 
				};
				
				// Generic regexes
				var r = zip_list.join('|');

				var zips_regex = new RegExp(r,"g");
				/*
					{
			            'domains' : { 
			                'regEx': domain_pattern   
			            },
			        },
				*/

				var genericExpectationArray = [
		        	{
			            'zips' : { 
			                'regEx': zips_regex   
			            }
			        },
			        {
			            'dates' : { 
			                'regEx': date_pattern 
			            }
			        },
					{
						"total_identifiers" : {
							"supported_match_objects" : total_identifiers,
							"match_property_name" : "name",
							"support_array_property_name" : "identifiers",
							"threshold" : 0.63
						}
					},
					{
						"total_words": {
							'match_in_array' :  total_words,
							'threshold' : 0.8
						}
					},
			        {
			            'priceLines' : {
			                'regEx': priceLine_pattern,
			                'match_group_labels' : ['content','name','itemPrice'],
			                'children': [ 
			                    {
			                        'float2d' : { 
			                            'regEx' : float_pattern, 
			                            'lines' : 1,
			                            'formatFunction' : function(n){
			                                n=trim(n); n=n.replace(/,/g, ".");
			                                return parseFloat(n);
			                            }
			                        }
			                    }
			                ]
			            },
			        }
			    ];
			    /*
			     ,
			                'formatFunction' : toolkit.format_date
			    */
			    /*

					format_date : function(d){
						var cur_d = d.getDate();
						var cur_m = d.getMonth() + 1; //Months are zero based
						var cur_y = d.getFullYear();
						// var s=locale.datesp;
						var s='.';
						return (cur_d + s + cur_m + s + cur_y);
					}

					function(d){
				        if( typeof( d ) === "string" ) {
							var dateParts = d.split(".");
							if(typeof(dateParts[2]) != 'undefined'){
								if(dateParts[2].length < 3 && !isNaN(dateParts[2])){
									// non 21st century, fix
									dateParts[2] = '20'+dateParts[2];
								}
							}
							var formattedDate = new Date(dateParts[2], (dateParts[1] - 1), dateParts[0]);
							var today = new Date();
							if(formattedDate < today){
								return formattedDate;
							} else {
								// date in future
								return null;
							}
						} else {
							// can't format
							return null;
						}
				    } 
			    */
			    
			    var _ppp = require('../lib/ppp.js');

				// console.log("before PPP generics");
			    var pppGenericResult = ppp(genericExpectationArray, tripDoc[0]['lineData']);
			    // console.log("after PPP generics, pppGenericResult: ");
			    // console.dir(pppGenericResult);

				// save now, then move on
				trip.saveGenericData(tripDoc[0]['imagechecksum'], tripDoc[0]['user'], pppGenericResult, function (){

				    if(typeof(pppGenericResult['zips']) !== 'undefined'){
				    	// console.log("pppGenericResult['zips'] is NOT undefined");
					    var Trip = mongoose.model('Trip', tripSchema);

						var all_found_zips = [];
						// console.log("before pre find city zips");
						for (var found_zips_length = pppGenericResult['zips'].length, found_zips_counter=0; found_zips_counter < found_zips_length; found_zips_counter++) {
							all_found_zips.push(pppGenericResult['zips'][found_zips_counter]['content']);
						}

						// zip.findByCity(sample_city, function(err, found_city_zips){
						zip.getStreetNames(sample_city, all_found_zips, function(err, street_names){

							// var housenumberpattern = new RegExp('[0-9]+[AaBcCc]?','g');

							// console.log('streets list:');
					    	// console.dir(street_names);

							var locationExpectationArray = [
			                    {
			                        'streetnames' : { 
			                            'match_in_array' :  street_names,
			                            'threshold' : 0.75,
			                            'children': [ 
			                                {
			                                    'house_numbers' : { 
			                                        'regEx': house_number_pattern,
			                                        'lines': 1
			                                    }
			                                },
			                                {
			                                	'sellerName' : {
									                'supported_match_objects' : sellers,
									                'match_property_name' : 'name',
									                'support_array_property_name' : 'identifiers',
									                'threshold' : 0.62
									            }
								            }
			                            ]
			                        }
			                    }
						    ];

						    var pppLocationResult = ppp(locationExpectationArray, tripDoc[0]['lineData']);
						    trip.saveLocationData(tripDoc[0]['imagechecksum'], tripDoc[0]['user'], pppLocationResult, function (){
						    	// done
						    });

						    // auto-insert pos
						    /*
						    console.log('POS');
						    console.log(pppLocationResult['sellerName'][0]['content']);
						    console.log(pppLocationResult['streetnames'][0]['content']);
							console.log(pppGenericResult['zips'][0]['content']);
							*/
							/*
						    if(
						    	typeof(pppGenericResult['sellerName']) != 'undefined' && pppLocationResult['sellerName'][0]['content']
						    	&& typeof(pppGenericResult['streetnames']) != 'undefined' && pppLocationResult['streetnames'][0]['content'] 
						    	&& typeof(pppGenericResult['zips']) != 'undefined' && pppGenericResult['zips'][0]['content'] 
						    ){
							    pos.upsertPos( 
						    		pppLocationResult['sellerName'][0]['content'],
						    		'',
						    		pppLocationResult['streetnames'][0]['content'],
						    		pppGenericResult['zips'][0]['content'],
						    		sample_city,
						    		'de_DE',
						    		imagechecksum,
						    		function(pos){
										// console.log("Upserting POS: "+newPos);
										/*
										if(typeof(newPos) !== "undefined"){
											if(newPos !== null){
												console.log("New POS: "+newPos);
											} else {
												console.log("Known POS: "+newPos.seller + " / " + newPos.zip + " / " + newPos.streetName);
											}
										}

										var posExpectationArray = [
						                    {
						                        'items' : { 
						                            'match_in_array' :  pos_item_names,
						                            'threshhold' : 0.75,
						                            'children': [ 
						                                {
						                                    'float2d' : { 
						                                        'regEx': house_number_pattern,
						                                        'lines': 1
						                                    }
						                                },
						                            ]
						                        }
						                    }
									    ];

									    var pppLocationResult = ppp(locationExpectationArray, result[0].lineData);
								}); // end pos.upsertPos()
								
							}); // end if seller + zip + street
							*/
						}); // end zip.getStreetNames
					
					} else {
						console.log("Done w/o location data (no zip)");
						// call success
						callback(true);
					} // end if zips found
				}); // end save generic data
			}); // end get zips
		}); // end get sellers
	},
	// publish is setting status to 1(public) or -1 (reject spam content)
	saveGenericData : function(imagechecksum, userId, pppGenericResult, callback){
		var Trip = mongoose.model('Trip', tripSchema);
		Trip.findOne({ 'user' : userId, 'imagechecksum' : imagechecksum }, function (err, tripDoc){
			if (err || tripDoc === null) {
				console.log('trip.saveGenericData error: ' + err, tripDoc);
				callback();
			} else {
				if(typeof(pppGenericResult['zips']) !== "undefined" && 'zips' in pppGenericResult && pppGenericResult['zips'][0]['content'] !== ''){
					console.log('trip.js: zips');
					tripDoc.zips = pppGenericResult['zips'][0]['content'];
				}
				if(typeof(pppGenericResult['domains']) !== "undefined" && 'domains' in pppGenericResult && pppGenericResult['domains'][0]['content'] !== ''){
					console.log('trip.js: domains');
					tripDoc.domains = pppGenericResult['domains'][0]['content'];
				}
				if(typeof(pppGenericResult['dates']) !== "undefined" && 'dates' in pppGenericResult && pppGenericResult['dates'][0]['content'] !== ''){
					console.log('date');

					if(typeof(pppGenericResult['dates'][0]['content']) !== "undefined"
						&& pppGenericResult['dates'][0]['content'] != ''
					){
						var dateObject = Date.parse(pppGenericResult['dates'][0]['content']);
						tripDoc.date = dateObject;
					} else {
						tripDoc.date = Date.now();
					}
					
					var dateString = pppGenericResult['dates'][0]['content'];
					tripDoc.dateString = dateString;
					
					//doc.date = pppGenericResult['dates'][0]['content'];
				}
				
				if(
					typeof(pppGenericResult['priceLines']) !== "undefined" 
					&& 'priceLines' in pppGenericResult 
					&& pppGenericResult['priceLines'][0]['content'] !== ''
				){
					console.log('trip.js: ' + pppGenericResult['priceLines'].length + ' priceLines');
					tripDoc.items = pppGenericResult['priceLines'];
				}
				
				if(
				    typeof(pppGenericResult['total_words']) !== "undefined" 
					&& 'total_words' in pppGenericResult 
					&& pppGenericResult['total_words'][0]['content'] !== ''
				){

					/*
					for(line_counter=0; line_counter < tripDoc.lines.length; line_counter++) {
						if(){
							console.log();
						}
					}
					*/
					var lineIndex_total = pppGenericResult['total_words'][0]['lineIndex'];
					console.log("Possible subtotal at line#" + lineIndex_total + ": " + tripDoc.lines[lineIndex_total]);

					var matches_array = new Array();
					matches_array[0] = 0;
					
					var floatPattern = new RegExp('([0-9]{1,4}[, \.][0-9]{1,4})','g');
					if(
						typeof(tripDoc.lines) !== "undefined"
						&& Array.isArray(tripDoc.lines)
						&& typeof(tripDoc.lines[lineIndex_total]) !== "undefined"
					){
						var matches_array = tripDoc.lines[lineIndex_total].match(floatPattern);
					}

					console.log('Match array:');
					console.dir(JSON.stringify(matches_array));
					
					if(typeof(matches_array) !== "undefined"
						&& matches_array != null
						&& matches_array.length > 0
						&& matches_array[0] !== 0
					) {
						n = matches_array[0];
						n = n.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
						n = n.replace(/,/g, ".");
						n = parseFloat(n);
						tripDoc.total = n;
					}
					
					// Found priceline we like, but regex doesn't find anything decent.
					// Use end of string (spaces etc.)
					if(matches_array == null){
						var tot_len = tripDoc.lines[lineIndex_total].length;
						
						console.log('tot_len:' + tot_len);
						
						var total_substring = tripDoc.lines[lineIndex_total].substring(tot_len, 9)
						
						console.log('total_substring:' + total_substring);
						
						total_substring = total_substring.replace(/[^\d.,]+/g, "");
						total_substring = total_substring.replace(/,/g, ".");
						
						console.log('total_substring replaced:' + total_substring);
						
						n = parseFloat(total_substring);
						
						console.log('total_substring:' + n);
						
						tripDoc.total = n;
					}
				}
				tripDoc.foundGenericData = pppGenericResult;
				tripDoc.save();

				console.log('id: '+imagechecksum+' updated with generic data');

				// call success
				callback(tripDoc);
			}
		});
	},
	saveLocationData : function(imagechecksum, userId, pppLocationResult, callback){
		var Trip = mongoose.model('Trip', tripSchema);
		Trip.findOne({ 'user' : userId, 'imagechecksum' : imagechecksum }, function (err, tripDoc){

			if('streetnames' in pppLocationResult){
				console.log('streetName');
				tripDoc.streetName = pppLocationResult['streetnames'][0]['content'];
			}
			if('house_numbers' in pppLocationResult){
				console.log('streetNumber');
				tripDoc.streetNumber = pppLocationResult['house_numbers'][0]['content'];
			}
			if('sellerName' in pppLocationResult && typeof(pppLocationResult['sellerName'][0]['content']) != 'undefind'){
				console.log('seller');
				tripDoc.seller = pppLocationResult['sellerName'][0]['content'];
			}

			// console.dir(pppGenericResult);

			/*
			for (var key in pppLocationResult) {
				console.log('KEY: '+key);
			    // if (pppGenericResult.hasOwnProperty(key)) {
			   	    if(typeof(pppGenericResult[key]) !== "undefined"){
			   	    	// console.log('DOC: '+doc.foundData[key]);
			   	    	console.log('GEN: '+pppGenericResult[key]);
			        }
			        if(typeof(pppLocationResult[key]) !== "undefined"){
			   	    	// console.log('PPP: '+pppLocationResult[key]);
			   	    	console.log('LOC: '+pppLocationResult[key]);
			        }
			        // var obj = pppLocationResult[key];

			        pppGenericResult[key] = [];
			        pppGenericResult[key] = pppGenericResult[key].concat( pppLocationResult[key] );
			}
			*/

			// console.log('AFTER: ');
			// console.dir(pppGenericResult);

			tripDoc.foundLocationData = pppLocationResult;
			tripDoc.save();

			console.log('id: '+imagechecksum+' updated with location data');

			// call success
			callback(tripDoc);
		});
	},
	read : function (userId, imagechecksum){
		// Recursive reading

		trip.getField(userId, imagechecksum, 'lineData', function(err, result){
			if (err) {
				console.log('trip.getField error: ' + err, result);
				callback();
			} else {
				// TODO: catch no result / empty array / null / undefined

				// console.log("LINES ");
				// console.dir(result[0].textresult);
				// var text = result[0].textresult;

				trip.findTotal(userId, imagechecksum, function(){
					console.log("findTotal complete (trip): "+ imagechecksum);
					var Trip = mongoose.model('Trip', tripSchema);
					Trip.update({ 'user' : userId, 'imagechecksum' : imagechecksum },{ $addToSet : { 'scanned' : 'total' } }, {}, function(err, result) {});
				});

				trip.findUrl(userId, imagechecksum, function(){
					console.log("findUrl complete (trip): "+ imagechecksum);
					trip.findSellerName(userId, imagechecksum, function(seller){
						// , 'berlin', result[0].textresult,
						console.log("read.findSellerName complete for trip "+ imagechecksum);
					});
				});

				/*
				trip.findCity(userId, imagechecksum, result[0].textresult, function(trip_city){
					console.log("findCity complete for trip "+ imagechecksum);
				});
				*/

				trip.findItemCandidates(userId, imagechecksum, function(){
					console.log("trip.findItemCandidates complete for "+ imagechecksum);
					var Trip = mongoose.model('Trip', tripSchema);
					Trip.update({ 'user' : userId, 'imagechecksum' : imagechecksum },{ $addToSet : { 'scanned' : 'items' } }, {}, function(err, result) {});
				});

				trip.findZips(userId, imagechecksum, function(zips){
				
					console.log("findZip complete for trip "+ imagechecksum);
					
					trip.findStreetAddress(userId, imagechecksum, zips, function(street, number){

						console.log("findStreetAddress complete for trip "+ imagechecksum);
						
						// Data Gold
						/*
						trip.findStore(userId, imagechecksum, function(){
							console.log("trip.findStore complete for "+ imagechecksum);
							var Trip = mongoose.model('Trip', tripSchema);
							Trip.update({ 'user' : userId, 'imagechecksum' : imagechecksum },{ $addToSet : { 'scanned' : 'store' } }, {}, function(err, result) {});
						});
						*/
					});
				});

				trip.findDate(userId, imagechecksum, function(){
					console.log("findDate complete (trip): "+ imagechecksum);
					// Get price reports
					// NYI
				});
			}
		});
	},
	checkTripCreation : function (user, trip){
		return true;
	},
	checkUploadLimit : function (userId, callback){

		// TODO: refactor to user.checkUploadLimit
		
		console.log('Checking uploadcount for user '+userId+'...');
		var today = new Date();

		var tomorrow = new Date();
		tomorrow.setDate(today.getDate() + 1);

		var yesterday = new Date();
		yesterday.setDate(today.getDate() - 1);
		
		var Trip = mongoose.model('Trip', tripSchema);
		Trip.find({ 'user' : userId, 'add_date' : {$gte: yesterday, $lt: tomorrow}}, 'user', {}, function(err, items) {
			if (err) return callback(err, null);
			var limit = 0;
			if(items[0].roles.indexOf("admin") >= 0 || items[0].roles.indexOf("premium") >= 0){
				limit = 250;
				console.log('Admin/Premium. Uploads: '+items.length+' / '+ limit);
			}else{
				limit = 11;
			}
			if (items.length < limit){
				console.log(items.length);

			} else { // from if (within trip upload limit)...
				console.log('Warn user: '+userId+': More than '+limit+' additions on same day');
				ui_error = 'Too many new uploads today! Your limit is:'+limit;

				res.render('add', { title: 'Add', ui_error : ui_error, user : req.user.username});
				return(null);
			} // end if/else (within trip/pic upload limit)
		}); // end check upload count
	},
	makeChecksum : function(imagefile, callback){
		// TODO: refactor to image.makeChecksum
		var imagechecksum = '';

		// Alternative method
		// TODO: test+benchmark this
		/*
			var crypto = require('crypto');
			var fs = require('fs');

			var algo = 'md5';
			var shasum = crypto.createHash(algo);

			var file = './kitten.jpg';
			var s = fs.ReadStream(file);
			s.on('data', function(d) { shasum.update(d); });
			s.on('end', function() {
				var d = shasum.digest('hex');
				console.log(d);
			});
		*/

		checksum.file(imagefile, function (err, sum) {
			// hash the checksum + include a salt to show the checksum was made by the app
			imagechecksum = (crypto.createHash("sha1")
			.update('0iiImAgESaLTtt0' + sum)
			.digest("hex"));
			console.log("Sum: "+sum);
			callback(imagechecksum);
		});
	},
	makeThumbnail : function(source_file, dest_file, dimensions, callback){
		// TODO: refactor to image.makeThumbnail
		// Make a thumbnail
		console.log("Thumbing "+ source_file +"...");
		var thumb_cmd = "convert -size "+dimensions+" "+source_file+" -thumbnail 80x80 "+dest_file;
		exec(thumb_cmd, function (err, stdout, stderr){
			if(err){
				console.log("makeThumbnail "+ source_file +" failed!");
				console.log(stderr +","+stdout);
				console.log(thumb_cmd);
				return(callback(false));
			} else {
				return(callback(true));
			} // end if/else(err)
		});
	},
	addStamp : function(source_file, dest_file, stamp_path, callback){
		// TODO: refactor to image.addStamp
		// 'Stamp' the image given under stamp_path on the bottom right of the image
		console.log("Stamping "+ source_file +" with "+ stamp_path);

		var stamp_cmd = "composite -gravity SouthEast "+stamp_path+" "+source_file+": "+dest_file;
		// TODO: refactor to image.addStamp
		exec(stamp_cmd, function (err, stdout, stderr){
			if(err){
				console.log("addStamp "+ source_file +" failed!");
				console.log(stderr +", "+stdout);
				console.log(stamp_cmd);
				callback(1);
			} else {
				callback(0);
			} // end if/else(err)
		});
	},
	getFileMetaData : function (imagefile, callback){
				
		console.log("getFileMetaData "+ imagefile);
		// returns dummy
		// Some major issues here.
		
		exifData = {};
		exif_orientation = 0;
		callback(exifData);

		// TODO: refactor to image.getFileMetaData
		/*
		new ExifImage({ image : imagefile }, function (error, exifData) {
			if (error){
				console.log('Error getting exif data: '+error.message);
				exifData = {};
				exif_orientation = 0;
				callback(exifData);
			} else {
				// cut out the padding, it has no meaning
				exifData['exif']['Padding'] = null;
				exifData['image']['Padding'] = null;
				// console.log('EXIF orientation: ' + exifData['image']['Orientation']);
				exif_orientation = exifData['image']['Orientation'];
				// console.log(exifData);
				callback(exifData);
				/*
				var orientation = '';

				// obtain the size of an image
				gm(_tf.imagefile).size(function (err, size) {
					if (!err) {
						// console.log('Image width = ' + size.width);
						// console.log('Image height = ' + size.height);
						// Landscape or Portrait?
						if(size.width > size.height){
							orientation = 'L';
						}else{
							orientation = 'P';
						}
						// console.log('Format: ' + orientation);
					}

					if(exif_orientation === 1 && orientation == 'L' ){
						// var deg = exifData['image']['Orientation'] + 0;
						// console.log('Rotating '+(deg)+' degrees...');
						console.log('Rotating 1/L ...');
						// var rotate_cmd = "convert "+ _tf.path +" -rotate 90 "+_tf.path;
					} else if(exif_orientation && orientation == 'L'){
						console.log('Rotating 2/L ...');
						// var rotate_cmd = "convert "+ _tf.path +" -rotate -90 "+_tf.path;
					}
					// callback(0,orientation,exifData);
					
				}); // end get image dimensions
				* /
			}
		});
		
		/*
		// having some problems with this usage
		try {
		} catch (error) {
			console.log('Error trying to get EXIF data: ' + error.message);
		} // end try/catch get imagefile metadata
		*/
	},
	rotate : function (source_file, dest_file, callback){
		// TODO: refactor to image.rotate
		var rotate_cmd = "convert " + source_file + " -rotate 90 " + dest_file;
		exec(rotate_cmd, function (err, stdout, stderr){
			if(err){
				console.log("Clean "+ source_file +" failed!");
				callback(1);
			} else {
				callback(0);
			} // end if/else(err)
		}); // execute rotate
	},
	clean : function (source_file, dest_file, callback){
		// TODO: refactor to image.clean

		// Super-simple version:
		// reduce colourset and 'specklyness' with median:
		// convert 28tfh2r.jpg -median 1 out.tif

		var clean_cmd = "/home/payprd/bin/textcleaner.sh "+ source_file +" " + dest_file + ".tiff";

		// console.log('Start cleaning ' + target_path + '...');

		exec(clean_cmd, function (err, stdout, stderr){
			if(err){
				console.log("Clean "+ source_file +" failed!");
				callback(1);
			} else {
				callback(0);
			} // end if/else(err)
		}); // textcleaner
	},

	findZips : function(userId, imagechecksum, callback){
		var Trip = mongoose.model('Trip', tripSchema);
		Trip.findOne({ 'user' : userId, 'imagechecksum' : imagechecksum }, function (err, tripObject){
			// trip.getField(userId, imagechecksum, 'lineData', function(err, tripObject){
			if (err) {
				console.log(err);
			} else {
				var found_zips = [];
				var trip_city = '';
				if(tripObject.city <= 1){trip_city = 'berlin-de';}
				
				zip.findByCity(trip_city, function(err, found_city_zips){
				
					var zip_list = [];
					// console.log("CZ: ");console.dir(city_zips);
					for(var z=found_city_zips.length;z>0;z--){ zip_list.push(found_city_zips[(z-1)].zip); };
					
					// Full-list regex
					var r = zip_list.join('|');
					var re = "("+r+")";
					var reg = new RegExp(re,"g");
					
					// console.log("ZIPRE: ");console.dir(re);
					
					for(tldi=0; tldi < tripObject.lineData.length; tldi++){
						// console.log("Z? ");console.dir(tripObject.lineData[tldi].text);
						var zip_matches = tripObject.lineData[tldi].text.match(reg);
						
						if( Object.prototype.toString.call( zip_matches ) === '[object Array]') {
							
							// TODO: all zips in line not just first,
							found_zips.push(zip_matches[0]);
							
							// tripObject.lineData[tldi].flags.push('has_zip');
							
							console.log("Zip: "); console.dir(zip_matches[0]);

						}
					}
					console.log("ZM: "); console.dir(found_zips);
					if(found_zips.length > 0){
						Trip.findOne({ 'user' : userId, 'imagechecksum' : imagechecksum }, function (err, doc){
							doc.lineData = tripObject.lineData;
							doc.zips = found_zips;
							doc.save();
							console.log('findZips updated id: '+imagechecksum);
							callback(found_zips);
						});
					}
					/*
					tripObject.save();
					console.log('findZips updated id: '+imagechecksum);
					*/
				});
			}
		});
	},
	findStreetAddress : function(userId, imagechecksum, zips, callback){
		// trip.findStreetAddress(userId, imagechecksum, 'berlin-de', function(street, number){
		
		var Trip = mongoose.model('Trip', tripSchema);
		Trip.findOne({ 'user' : userId, 'imagechecksum' : imagechecksum }, function (err, tripObject){
			// console.log('TO:');console.dir(tripObject);

			var trip_city = '';
			if(tripObject.city.length <= 1){trip_city = 'berlin-de';}
			
			zip.getStreetNames(trip_city, zips, function(err, street_names){
				if(err){
					console.log('Error getting StreetName: '+err);
					callback('');
				} else {			
					var rank = null;
					var found_street_name = '';
					var streetnameranks=[];
					var rankModifier = 1;
					
					for(tldi=0; tldi < tripObject.lineData.length; tldi++){
						for (var snl = street_names.length, sn=0; sn < snl; sn++) {

							var ltext = tripObject.lineData[tldi].text;
							ltext = ltext.replace(/[0-9*]/g, '');
							
							// console.log(  street_names[sn]+' vs '+ltext);
							
							var lev = new l( street_names[sn], ltext );
							var ldl = ( street_names[sn].length / (lev.distance+1) );
							
							if(lev.distance < 6 && ldl > 1.25){
								rank = lev.distance + 1; // add 1 so we can see effect of multipliers when rank is 0
								
								var rankIndex = null;
								for (var r=0, iLen=streetnameranks.length; r<iLen; r++) {
									if (streetnameranks[r].name == street_names[sn]){
										rankIndex = r;
										break;
									}
								}
								// tripObject.lineData[tldi].flags.push('has_zipstreet');
								
								/*
								var line = tripObject.lineData[tldi].text;
								var linewords = line.split("/,|.|-| /");
								console.dir(linewords);
								
								var street_number_match = trip_words[tw+1].match(/[0-9]+[,.-]?[0-9]+?/);
								*/
								var street_number_match = tripObject.lineData[tldi].text.match(/([0-9]+)[,.-]?[0-9]{0,5}/g);

								if(street_number_match && typeof(street_number_match[0]) !== 'undefined'){
									console.log('Got a street number:');console.dir(street_number_match);
									var street_number = street_number_match[street_number_match.length-1];
								}
								
								if(rankIndex){
									streetnameranks[rankIndex].rank = (streetnameranks[rankIndex].rank * rankModifier);
									rank = (rank * rankModifier);
									streetnameranks.push({'rank' : rank, 'name' : street_names[sn], "ldl" : ldl, "number" : street_number });
								} else {
									rank = (rank * rankModifier);
									streetnameranks.push({'rank' : rank, 'name' : street_names[sn], "ldl" : ldl, "number" : street_number });
								}
								rankModifier = 1;
							}
						}
					}
					if(streetnameranks.length >= 1){
						// console.log("Sort...");
						streetnameranks.sort(function(a, b) { 
							return a.rank - b.rank;
						});
						for (var r=0, r_try = 15; r < r_try; r++) {
							if(typeof(streetnameranks[r]) != 'undefined' ){
								console.log(streetnameranks[r].rank + " : " + streetnameranks[r].name +'('+streetnameranks[r].ldl+')');
								var found_street_name = streetnameranks[0].name;
								if(typeof(streetnameranks[r].number) != 'undefined' && streetnameranks[r].number != ''){
									var found_street_number = streetnameranks[0].number;
								}
							}
						}
					}
					// console.log('Found Street: "' + found_street_name); if(found_street_number){console.log(' Number: ' +found_street_number);}
					Trip.findOne({ 'user' : userId, 'imagechecksum' : imagechecksum }, function (err, doc){
						doc.lineData = tripObject.lineData;
						doc.streetName = found_street_name; 
						if(typeof(found_street_number) !== 'undefined'){
							doc.streetNumber = found_street_number;
						}
						doc.save();
						console.log('findZips updated id: '+imagechecksum);
						callback(found_street_name, found_street_number);
					});
				}
				// console.log("findStreetAddress failed for "+imagechecksum);
				// callback('');
			});
		}); // tripdoc
	},
	findItemCandidates : function(userId, imagechecksum, callback){
		var Trip = mongoose.model('Trip', tripSchema);
		Trip.findOne({ 'user' : userId, 'imagechecksum' : imagechecksum }, function (err, tripObject){
			if (err || tripObject.length < 1) {
				console.log(err, tripObject);
				console.log('trip.findItemCandidates aborted');
				callback();
			} else {
			
				
			
				var match = '';
				var item_blacklist=['bar','netto-warenwert','total','gesamtsumme','saldo','gesamt','summe','rabatt','zu zahlen','incl','inkl','rechnung','datum','rech nr.','zurueck','zuruck','ruckgeld','rueckgeld','total eur','eur','euro','zwischensumme','mwst','umsatz','umsatz von','vom:','vomz','pfand','zzg pfand','pfandbon','zahlung','karte','kartenzahlung','ec-karte','visa'];
				var tripitems = new Array();
				
				// Single line version, no \n at start
				var rex = /(([\*A-Za-z0-9\.-]+[^\S\n])*)(?:[0-9]*[^\S\n])*([0-9]{1,8}[,.][0-9]{2}?[ ]?[0-9]?)(?:([^\S\n]EUR|[^\S\n][NAB 1]{1,2}))*/;

				for(tii=0; tii<tripObject.lineData.length; tii++){
					var keep_item = null;

					// if(tripObject[0].lineData[tii].flags.item == true){continue}
					
					match = rex.exec(tripObject.lineData[tii].text);

					if(Object.prototype.toString.call( match ) === '[object Array]' && match.length >= 1){
					
						// tripObject.lineData[tii].flags.push('has_price');

						if(match[1] !== ''){
							tripItemName = match[1].replace(/^\s\s*/, '').replace(/\s\s*$/, '');
						} else {
							// Dirty...but we take *something* for now, humans can clean it ;)
							tripItemName = tripObject.lineData[tii].text;
							tripItemName = tripItemName.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
						}
						
						var temptext = tripItemName.replace(/[0-9. ]+/g, '');
						// console.log(temptext);
						
						for (var ibl = item_blacklist.length, ibi=0; ibi < ibl; ibi++) {
							
							var iblev = new l( temptext.toLowerCase(), item_blacklist[ibi] );
							var ibldl = ( temptext.length / (iblev.distance+1) );
							
							if(iblev.distance > 3 && ibldl < 1.32 && temptext.length > 2){
								keep_item = true;
							} else {
								console.log('skipped non-item:'+tripItemName);
								keep_item = false;
								break;
							}
						}
						if(keep_item === true){
							tripItemPrice = match[3].replace(/^,\s\s*/, '').replace(/\s\s*$/, '').replace(/,/g, ".");
							tripItemPrice = parseFloat(tripItemPrice).toFixed(2);
							
							if(tripItemName != '' && isNaN(tripItemPrice) === false){
							
								// Try to get a count?
								// TODO: these should have special known overrides for specific sellers/identities
								// string start:  "^([0-9l]{1,4})(?:x| |X|stck|stk|st|pc|pcs|.)";
								// string end:  "([0-9l]{1,4})(?:x| |X|stck|stk|st|pc|pcs|.)$";
								
								var re = "^([0-9l ]{1,4})(?:x|X| |stck|stk|st|pc|pcs) ?"
								var reg = new RegExp(re,"g");
								// var itemCountMatch = parseInt(tripItemName.match(reg));
								var itemCountMatch = tripItemName.match(reg);
								console.dir(itemCountMatch);
								if(itemCountMatch != null){
									// cut the number off the original string
									tripItemName = tripItemName.substr(itemCountMatch[0].length);
									console.log('ICM: '+itemCountMatch[0]);
									
									// clean anything that's not a number
									var itemCount = itemCountMatch[0].replace(/^[0-9lkg]/, '');
									console.log('IC: '+itemCount[0]);
									
								}
								
								console.log('Item: "' + tripItemName + '" Price: ' +tripItemPrice);
							
								// Give the items a 'hybrid' id to reference the item during approval stage
								var tripItemId = 	imagechecksum + "-" + tripObject.lineData[tii].index;

								var itemObject = {id:tripItemId, count:itemCount, name:tripItemName, price:tripItemPrice};

								tripitems.push(itemObject);
							}
						}
					}
				}

				/*
				console.log("LD: ");
				for(ldi=0; ldi<tripObject[0].lineData.length; ldi++){
					console.dir(tripObject[0].lineData[ldi]);
				}
				*/
				
				if(tripitems.length > 0){
					Trip.findOne({ 'user' : userId, 'imagechecksum' : imagechecksum }, function (err, doc){
						doc.lineData = tripObject.lineData;
						doc.items = tripitems;
						doc.save();
						console.log('findItemCandidates updated id: '+imagechecksum);
					});
				} else {
					callback();
				}
			}
		});
	},
	approveTripItem : function(iid, uid, tid, callback) {
		var Trip = mongoose.model('Trip', tripSchema);
		
		Trip.find({'user' : uid, '_id' : tid }, 'items', {}, function(err, items) {
			if (err) {
				return callback(err, null);
			}
			if(items){
				console.log("trip.approveTripItem(item_id: "+ iid +") Found: " + items.length);
				console.dir(items);
				
				var newItemArray = items[0]['items'];
				
				// Find index of item that was approved
				function getId(array, id) {
					for (var i = 0, len = array.length; i < len; i++) {
						if (array[i].id === id) {
							return i;
						}
					}
					return null;
				}
				var target_item_index = getId(newItemArray, iid);
				newItemArray[target_item_index].approved = 1;

				Trip.update({ 'user' : uid, 'imagechecksum' : tid },{ 'items' : newItemArray }, {},	function(err, items) {
					if (err) {
						return callback(items, err);
					} else {
						callback(items);
					}
				});
				// callback(err, items);
			} 
		});
		callback(err, items);
	},
	findDate : function(userId, imagechecksum, callback){
		var Trip = mongoose.model('Trip', tripSchema);

		trip.getField(userId, imagechecksum, 'textresult', function(err, data){
			if (err) {
				console.log(err, data);
				callback();
			} else if(typeof(data[0]) != 'undefined'){
				// console.dir(data);

				rex = /([0]?[1-9]|[1|2][0-9]|[3][0|1])[.-\/]([0]?[1-9]|[1][0-2])[-A-Z.\\\/]+(20[0-9]{2}|[0-9]{2})/;
				// rex = /([0]?[1-9]|[1|2][0-9]|[3][0|1])[.\/]([0]?[1-9]|[1][0-2])[.\/]*(20[0-9]{2}|[0-9]{2})/;
				var lineContent = data[0]['textresult'];

				var date = lineContent.match(rex);

				if( Object.prototype.toString.call( date ) === '[object Array]' ) {

					var dateParts = date[0].split(".");
					if(typeof(dateParts[2]) != 'undefined'){
						if(dateParts[2].length < 3 && !isNaN(dateParts[2])){
							console.log('Date() defaulted - not in 21st century, fixing...');
							dateParts[2] = '20'+dateParts[2];
						}
					}

					console.log('Creating date object from "'+date[0]+'" (params: '+dateParts[2]+','+(dateParts[1] - 1)+','+dateParts[0]+')');
					var tripDate = new Date(dateParts[2], (dateParts[1] - 1), dateParts[0]);

					var today = new Date();

					console.log('findDate found: '+date[0]);

					if(tripDate < today){
						trip.updateField (userId, imagechecksum, 'date', tripDate, function() {
							if (err) {
								console.log(err);
								callback();
							} else {
								// console.log('findDate updated id: '+imagechecksum);
								callback();
							}
						});
					} else {
						console.log('Date in future, not updated.');
					}
				} else {
					console.log('No match.');
					callback();
				}
			}
		});
	},
	dateFromString : function(date, callback){
		if( typeof( date ) === "string" ) {

			var dateParts = date.split(".");
			if(typeof(dateParts[2]) != 'undefined'){
				if(dateParts[2].length < 3 && !isNaN(dateParts[2])){
					console.log('Date() defaulted - not in 21st century, fixing...');
					dateParts[2] = '20'+dateParts[2];
				}
			}

			console.log('Creating date object from "'+date+'" (params: '+dateParts[2]+','+(dateParts[1] - 1)+','+dateParts[0]+')');
			var formattedDate = new Date(dateParts[2], (dateParts[1] - 1), dateParts[0]);

			if(formattedDate < today){
				callback(formattedDate);
			} else {
				console.log('Date in future, not updated.');
				callback(null);
			}
		} else {
			console.log('No match.');
			callback(null);
		}
	},
	findSellerName : function(userId, imagechecksum, callback){
		
		// TODO: filter seller names by locale
		// Currently including ALL seller names
		seller.getLocaleSellerNames(app_params.supported_locales, '', function(err, sellers){
			
			// var trip_words = trip_text.match(/[^\s.]+/g);
			// console.log('Sellers digging: '+trip_words.length+' words.');
			
			var rank = null;
			var found_seller = null;
			// var ranklist=[];
			var nameranks=[];
			var rankModifier = 1;
			
			// first get url
			trip.getFields(userId, imagechecksum, ['textresult','url'], function(err, tripData){
			// getField(userId, imagechecksum, 'url', function(err, tripUrl){ // , checkName
				if (err) {
					console.log(err);
					callback(err);
				} else {
					var trip_words = tripData[0].textresult.match(/[^\s.]+/g);
					
					for (var snl = sellers.length, sn=0; sn < snl; sn++) {
						// console.log('Matching: '+sellers[sn].name);

						// Apply modifiers
						var brandWordCount = (sellers[sn].name.split(" ").length - 1 );
						// blind url matching
						if(typeof(tripData[0].url) != "undefined"){
							var lev = new l( sellers[sn].name.toLowerCase(), tripData[0].url.toLowerCase() );
							if(lev.distance < 3){ // tripUrl[0].url.indexOf(sellers[sn].name) > -1
								console.log("Matched a brand name in a URL: " + sellers[sn].name + " ~~ " + tripData[0].url);
								rankModifier = (rankModifier * 0.82);
							}
						}
						// TODO: sn inside tw
						for (var twl = trip_words.length, tw=0; tw < twl; tw++) {
							if(trip_words[tw].length < 2 || parseFloat(trip_words[tw])==trip_words[tw]){
								continue;
							}
							
							var content_word = trip_words[tw].toLowerCase();
							// when looking for brands with 2 words, just "look ahead" by comparing against the 
							if(brandWordCount > 0){
								for(wo =0; wo < brandWordCount; wo++){
									if(typeof(trip_words[tw+wo+1]) !== 'undefined'){
										content_word += ' ' +trip_words[tw+wo+1].toLowerCase();
									}
									// if(sellers[sn].name.toLowerCase() == "media markt"){
										// console.log('Now comparing "'+ sellers[sn].name.toLowerCase() +'" with "'+content_word+'"');
									// }
								}
							}

							var lev = new l( sellers[sn].name.toLowerCase(), content_word );
							var ldl = ( sellers[sn].name.length / (lev.distance+1) );
							
							// console.log('Compare "'+ sellers[sn].name.toLowerCase() +'" with "'+content_word+'" : '+lev.distance);
							
							if(lev.distance < 7 && ldl > 1.32){ // lev.distance < (sellers[sn].name.length / 3) 
								
								
								// ranklist[rank] = sellers[sn].name;

								// console.log('LD/L:'+(sellers[sn].name.length / lev.distance));
								
								rank = lev.distance + 1; // add 1 so we can see effect of multipliers when rank is 0
								
								/*
								if(typeof(trip_words[tw+1]) != "undefined"){
									rexCompany = /(?:GMBH|[G6gC][mnn][6bhn][hnMH]|Ltd.?|[IL][t+][Dd].?)/i;
									var companyMatch = trip_words[tw+1].toLowerCase().match(rexCompany);
									if ( Object.prototype.toString.call( companyMatch ) === '[object Array]'){
										console.log("Matched a company name: " + trip_words[tw+1] + " ~~ " + companyMatch[0]);
										rankModifier = (rankModifier * 0.65);
									}
								}
								*/
								
								if ( trip_words[tw].toLowerCase() == 'netto' || trip_words[tw].toLowerCase() == 'total' ){
									console.log("Netto/Total: " + trip_words[tw]);
									rankModifier = (rankModifier * 1.01);
								}
								
								var rankIndex = null;
								for (var r=0, iLen=nameranks.length; r<iLen; r++) {
									if (nameranks[r].name == sellers[sn].name){
										rankIndex = r;
										break;
									}
								}
								
								if(rankIndex){
									nameranks[rankIndex].rank = (nameranks[rankIndex].rank * rankModifier);
									rank = (rank * rankModifier);
									nameranks.push({'rank' : rank, "ldl" : ldl, 'name' : sellers[sn].name, 'identifiers' : sellers[sn].identifiers,});
								} else {
									rank = (rank * rankModifier);
									nameranks.push({'rank' : rank, "ldl" : ldl, 'name' : sellers[sn].name, 'identifiers' : sellers[sn].identifiers,});
								}
								rankModifier = 1;
							};
						}
					}
					
					
					// console.log('Pre - Weighting sort:'); console.dir(nameranks);
					
					if(nameranks.length >= 1){
						for (var nrl = nameranks.length, nri=0; nri < nrl; nri++) {
							// console.log('Rank weighting:'+nameranks[nri].rank+'/'+nameranks[nri].name+'/'+nameranks[nri].ldl);
							// console.log('Check identifiers ('+nameranks[nri].identifiers.length+ ')');
							// console.log(trip_words);
							
							for (var snil = nameranks[nri].identifiers.length, sni=0; sni < snil; sni++) {
								for (var id_twl = trip_words.length, id_tw=0; id_tw < id_twl; id_tw++) {
									if(trip_words[id_tw].length < 2 || parseFloat(trip_words[id_tw])==trip_words[id_tw]){
										continue;
									}
									if(typeof(nameranks[nri].identifiers[sni]) !== 'string' ){
										console.log('Bad Identifier');
										continue;
									}
									var id_content_word = trip_words[id_tw].toLowerCase();
									var idWordCount = (nameranks[nri].identifiers[sni].split(" ").length );
									// console.log('idWordCount: '+idWordCount);
									if(idWordCount > 1){
										for(wo =0; wo < idWordCount; wo++){
											if(typeof(trip_words[id_tw+wo+1]) !== 'undefined'){
												id_content_word += ' ' +trip_words[id_tw+wo+1].toLowerCase();
											}
										}
									}
									var idlev = new l( nameranks[nri].identifiers[sni].toLowerCase(), id_content_word );
									var idldl = ( nameranks[nri].identifiers[sni].length / (lev.distance+1) );
									
									// console.log(id_content_word+':'+nameranks[nri].identifiers[sni]+'-'+nameranks[nri].rank+'/'+idlev+'/'+idldl);
									
									if(idlev.distance < 3 && idldl > 1.5){
										console.log('Hit: "'+ nameranks[nri].identifiers[sni].toLowerCase() +'" with "'+id_content_word+'" : '+idlev.distance+'/'+idldl);
										// nameranks[nri].rank = (nameranks[nri].rank * 0.75);
										nameranks[nri].rank = (nameranks[nri].rank / (nameranks[nri].identifiers[sni].length / 4));
									}
								}
							}
						}
					}
					
					// console.log('Post - Weighting sort:'); console.dir(nameranks);
					
					if(nameranks.length >= 1){
						console.log("Sort...");
						nameranks.sort(function(a, b) { 
							return a.rank - b.rank;
						});
						for (var r=0, r_try = 15; r < r_try; r++) {
							if(typeof(nameranks[r]) != 'undefined' ){
								// if(typeof(nameranks[r].rank) > 0 && nameranks[r].name != ''){}
								console.log(nameranks[r].rank + " : " + nameranks[r].name +'('+nameranks[r].ldl+')');
								if(nameranks[0].rank <= 2){
									found_seller = nameranks[0].name;
								}
							}
						}
					}
					if(found_seller !== null){
						trip.updateField (userId, imagechecksum, 'seller', found_seller, function() {
							if (err) {
								console.log(err);
								callback(1);
							} else {
								// console.log('findSellerName updated id: '+imagechecksum);
								callback(found_seller);
							}
						});
					} else {
						callback('');
					}
				} // if / else err on getField
			});
		});
	},
	findTotal : function(userId, imagechecksum, callback){
		console.log('legacy FT called');
		var Trip = mongoose.model('Trip', tripSchema);

		trip.getField(userId, imagechecksum, 'textresult', function(err, data){
			if (err) {
				console.log(err, data);
				callback();
			} else {

				rex = /(?:Su[mnh]{1,2}e|SU[MNH]{1,2}E EUR|Bar EUR|Verzeh|zu zahlen|zu zen1en|zu [zZ]a[hn][1il]en|zu [zZ]a[hn][1il]en EUR[0o]{0,1}|Betrag|tota[l1]|[Kk]arte|kartenzahlung|ec-karte|visa|mastercard)(?:[ ]*[\w-]*)*[ ]+([0-9]?[0-9]?[0-9]?[0-9]?[,.][0-9][0-9]?)/i;

				var total = data[0]['textresult'].match(rex);

				if( Object.prototype.toString.call( total ) === '[object Array]' ) {

					var totalFloat = total[0].match(/[0-9]+[,.][0-9]+/);

					if(totalFloat && typeof(totalFloat[0]) !== 'undefined'){
						totalFloat = totalFloat[0].replace( /,/,"." );
						
						totalFloat = parseFloat(totalFloat);
						
						totalFloat = totalFloat.toFixed(2);
						
						// console.dir(totalFloat);

						trip.updateField (userId, imagechecksum, 'total', totalFloat, function() {
							if (err) {
								console.log(err, data);
								callback();
							} else {
								// console.log('findTotal updated id: '+imagechecksum);
								callback();
							}
						});
					}
				} else {
					console.log('No match.');
					callback();
				}
			}
		});
	},
	findItems : function(userId, imagechecksum, itemsArray, callback){
		// TODO: this was 'half refactored' out of findItemCandidates
		// Auto-approve
		
		// TODO:
		// Check blacklist ('Pfand','Total','Steuer','Cash'/'Change' etc.)
		
		console.log('trip.findItems getting item names for matching...');
		item.getLocaleItemNames(app_params.supported_locales, function(err, results){
			if(err){
				console.log('Error getting Items: '+err);
				callback();
			} else {
				if(results) {
					var rank = null;
					var ranklist=[];
					
					for (var ial = itemsArray.length, iai=0; iai < ial; iai++) {
						for (var inl = results.length, ini=0; ini < inl; ini++) {

							// Skip 1-letter texts or numbers
							if(itemsArray[iai].length < 2 || parseFloat(itemsArray[iai])==itemsArray[iai]){
								console.log('skipped some junk');
								continue;
							}

							// itemsArray[iai] = itemsArray[iai].toLowerCase();
							
							// console.log( 'C: ' + itemsArray[iai]+' vs '+results[ini].name );
							
							
							if((typeof(itemsArray[iai].name) !== 'undefined' && typeof(results[ini].name) !== 'undefined') && (itemsArray[iai].name != '' && results[ini].name != '')){
								var lev = new l( results[ini].name, itemsArray[iai].name);
								if(lev.distance < 4){
									rank = lev.distance;

									ranklist[rank] = results[ini].name;
									
									/*
									console.log( 'C: ');
									console.dir(itemsArray[iai].name);
									console.dir(results[ini].name);
									*/
									/*
									if (lev.distance == 0){ 
										break;
									}
									*/
									// console.log(results[sn].name + " # "+trip_words[tw].toLowerCase()+" = "+lev.distance)
								};
							}
						}
						console.dir(ranklist);
						
						console.log('trip.findItems: inner');
						callback();
						// console.dir(ranklist);
						/*
						for (var r=0, r_try =1; r < r_try; r++) {
							if(ranklist[r] != '' && typeof(ranklist[r]) != 'undefined'){
								// console.dir(ranklist[r]);
								var found_item = ranklist[r];
								/*
								trip.approveTripItem(itemsArray[iai].id, userId, imagechecksum, function(err, result){
									console.log("Auto-approved item: "+itemsArray[iai].name+" / "+imagechecksum);
								});
								* /
							} else {
								r_try++;
							}
						}
						*/
					}
				}
			}
		});
	},
	findUrl : function(userId, imagechecksum, callback){
		var Trip = mongoose.model('Trip', tripSchema);	
		trip.getField(userId, imagechecksum, 'lineData', function(err, tripdata){
			if (err) {
				console.log(err);
			} else {
			
				var match = '';
				var tripitems = new Array();
				var item_index = 0;
				var tripItemId = '';
			
				// Creds: @imme_emosol
				// rex = /(https?|ftp)://(-\.)?([^\s/?\.#-]+\.?)+(/[^\s]*)?$/iS;
				// rex = /(https?):\/\/(-\.)?([^\s/?\.#-]+\.?)+(\/[^\s]*)?$/i;
				// (\(https?:\/\/|ftp:\/\/|www\.|@\.).+?(\.\w+)+
				
				// (https?://|ftp://|www\.|[^\s:=]+@www\.).*?

				// (\(?https?[:]\/\/www.|www\.|@\.|@|)([\.A-Za-z0-9_-]+)?(\.\w+)+

				rex = /(\(?https?:\/\/[wvmu]{2,4}\.|https?:\/\/|[wvmu]{2,4}\.|@\.|@|)+([-A-Za-z0-9_]{2,})[\.]+(de|com|co.uk|net|org|biz|fr|es|pt|info|ca|jp|us|ru|ch|it|nl|se|no|at)/;
				for(tii=0; tii<tripdata[0].lineData.length; tii++){
				
					// console.log ("Rexing URL: "+tripdata[0].lines[tii].toLowerCase());
					if(typeof(tripdata[0].lineData[tii].text) == 'string'){
						match = rex.exec(tripdata[0].lineData[tii].text.toLowerCase());

						if(Object.prototype.toString.call( match ) === '[object Array]' && match.length >= 1){
							console.log("findUrl Got: " + match[0]);
							console.dir(match);
							trip.updateField (userId, imagechecksum, 'url', match[0], function() {
								if (err) {
									console.log(err, data);
									callback();
								} else {
									console.log('findUrl updated id: '+imagechecksum);
									callback();
								}
							});
						}
					}
				}
			}
			callback();
		});
	},
	findTime : function(userId, imagechecksum, callback){
		var Trip = mongoose.model('Trip', tripSchema);

		trip.getField(userId, imagechecksum, 'textresult', function(err, data){
			if (err) {
				console.log(err, data);
				callback();
			} else {
				// console.dir(data);

				// rex = /([0]?[1-9]|[1|2][0-9]|[3][0|1])[.-\/]([0]?[1-9]|[1][0-2])[-A-Z.\\\/]+(20[0-9]{2}|[0-9]{2})/;
				rex = /([0-1][0-9]|2[0-3]):([0-5][0-9])(:?[0-5][0-9]))/;
				// rex = /([0]?[1-9]|[1|2][0-9]|[3][0|1])[.\/]([0]?[1-9]|[1][0-2])[.\/]*(20[0-9]{2}|[0-9]{2})/;
				var lineContent = data[0]['textresult'];

				var time = lineContent.match(rex);

				if( Object.prototype.toString.call( time ) === '[object Array]' ) {
					console.log('findTime found: '+time[0]);
					console.dir(time);
					var timeParts = time[0].split("/:| /");
					
					trip.getField(userId, imagechecksum, 'date', function(err, data){
						if(data){
							console.log('Updating time object: "'+data[0].date+'" (params: '+timeParts[2]+','+(timeParts[1] - 1)+','+timeParts[0]+')');
							// var tripDate = new Date(dateParts[2], (dateParts[1] - 1), dateParts[0]);
						} else {
							var today = new Date();
							var tripDate = new Date(dateParts[2], (dateParts[1] - 1), dateParts[0]);
						}

						trip.updateField (userId, imagechecksum, 'date', tripDate, function() {
							if (err) {
								console.log(err);
								callback();
							} else {
								// console.log('findDate updated id: '+imagechecksum);
								callback();
							}
						});
					});
				} else {
					console.log('No match.');
					callback();
				}
			}
		});
	},
	findCity : function(userId, imagechecksum, textresult, callback){

		// old city regex
		// rex = /(?:Ber[li1I]{1,2}n|BERL[Iil1]N|N[uüe]{1,2}rnberg|Hersbruck|London)/i;
		
		// fixed city list
		var cities = app_params.supported_cities;
		
		var trip_words = textresult.match(/[^\s.]+/g);

		// console.log('City digging: '+trip_words.length+' words.');
		
		var rank = null; ranklist=[];
		
		/* Loop through the words in the ocr result and get a levenshtein distance to gauge similarity */
		for (var twl = trip_words.length, tw=0; tw < twl; tw++) {
			for (var snl = cities.length, sn=0; sn < snl; sn++) {

				if(trip_words[tw].length < 3 || parseFloat(trip_words[tw])==trip_words[tw]){continue;}

				var lev = new l( cities[sn], trip_words[tw].toLowerCase() );

				if(lev.distance < 6){
				// console.log(sellers[sn] + " # "+trip_words[tw].toLowerCase()+" = "+lev.distance)
				};

				rank = lev.distance;

				ranklist[rank] = cities[sn];
			}
		}
		
		
		/* Sort the ranked city results and apply any special rules */
		// console.dir(ranklist);
		for (var r=0, r_try =1; r < r_try; r++) {
			if(ranklist[r] != '' && typeof(ranklist[r]) != 'undefined'){
				// console.dir(ranklist[r]);
				var found_city = ranklist[r];
				trip.updateField (userId, imagechecksum, 'city', ranklist[r], function(err) {
					if (err) {
						console.log(err);
						callback(1);
					} else {
						// console.log('findCity updated id: '+imagechecksum);
						callback(found_city);
					}
				});
			} else {
				r_try++;
			}
		}
	},
	findCompany : function(userId, imagechecksum, callback){
		console.log('- trip.findCompany');
		var Trip = mongoose.model('Trip', tripSchema);
		trip.getField(userId, imagechecksum, 'lines', function(err, tripdata){
			if (err) {
				console.log(err);
			} else {
				var match = '';
				var tripitems = new Array();
				var item_index = 0;
				var tripItemId = '';

				rexCompany = /(?:GMBH|[G6gC][mnn][6bhn][hnMH]|Ltd.?|[IL][IL][G6gC]|[IL][t+][Dd].?)/i;
				for(tii=0; tii<tripdata[0].lines.length; tii++){
				
					console.log ("Rexing Company: "+tripdata[0].lines[tii].toLowerCase());
					
					match = rex.exec(tripdata[0].lines[tii].toLowerCase());

					if(Object.prototype.toString.call( match ) === '[object Array]' && match.length >= 1){
						console.log("findCompany Got: " + match[0]);
						console.dir(match);
						trip.updateField (userId, imagechecksum, 'company', match[0], function() {
							if (err) {
								console.log(err, data);
								callback();
							} else {
								console.log('findCompany updated id: '+imagechecksum);
								callback();
							}
						});
					}
				}
			}
			callback();
		});
	},
	findVatNumber : function(userId, imagechecksum, callback){
		console.log('- trip.findVatNumber');
		var Trip = mongoose.model('Trip', tripSchema);
		trip.getField(userId, imagechecksum, 'lines', function(err, tripdata){
			if (err) {
				console.log(err);
			} else {
				var match = '';
				var tripitems = new Array();
				var item_index = 0;
				var tripItemId = '';

				rexCompany = /((EE|EL|DE|PT)-?)?[0-9]{9}/i;
				// rex = /([0]?[1-9]|[1|2][0-9]|[3][0|1])[.-\/]([0]?[1-9]|[1][0-2])[-A-Z.\\\/]+(20[0-9]{2}|[0-9]{2})/;
				for(tii=0; tii<tripdata[0].lines.length; tii++){
				
					console.log ("Rexing VAT Number: "+tripdata[0].lines[tii].toLowerCase());
					
					match = rex.exec(tripdata[0].lines[tii].toLowerCase());

					if(Object.prototype.toString.call( match ) === '[object Array]' && match.length >= 1){
						console.log("findVatNumber Got: " + match[0]);
						console.dir(match);
						trip.updateField (userId, imagechecksum, 'vat_id', match[0], function() {
							if (err) {
								console.log(err, data);
								callback();
							} else {
								console.log('findVatNumber updated id: '+imagechecksum);
								callback();
							}
						});
					}
				}
			}
			callback();
		});
	},
	findTaxNumber : function(userId, imagechecksum, callback){
		console.log('- trip.findTaxNumber');
		var Trip = mongoose.model('Trip', tripSchema);
		trip.getField(userId, imagechecksum, 'lines', function(err, tripdata){
			if (err) {
				console.log(err);
			} else {
				var match = '';
				var tripitems = new Array();
				var item_index = 0;
				var tripItemId = '';

				rexCompany = /(?:Steuer\.Nr\.|St\.Nr|St\.-Nr|St\.Nr|StNr\.|Steuernummer.?):?()[.-\/ ])/i;
				// rex = /([0]?[1-9]|[1|2][0-9]|[3][0|1])[.-\/]([0]?[1-9]|[1][0-2])[-A-Z.\\\/]+(20[0-9]{2}|[0-9]{2})/;
				for(tii=0; tii<tripdata[0].lines.length; tii++){
				
					console.log ("Rexing Tax Number: "+tripdata[0].lines[tii].toLowerCase());
					
					match = rex.exec(tripdata[0].lines[tii].toLowerCase());

					if(Object.prototype.toString.call( match ) === '[object Array]' && match.length >= 1){
						console.log("findTaxNumber Got: " + match[0]);
						console.dir(match);
						trip.updateField (userId, imagechecksum, 'tax_id', match[0], function() {
							if (err) {
								console.log(err, data);
								callback();
							} else {
								console.log('findTaxNumber updated id: '+imagechecksum);
								callback();
							}
						});
					}
				}
			}
			callback();
		});
	},
	findStore : function(userId, imagechecksum, callback){
		var Trip = mongoose.model('Trip', tripSchema);
		Trip.find({ 'user' : userId, 'imagechecksum' : imagechecksum }, '', {}, function(err, tripResult) {
			if (err) {
				return callback(err, null);
			}
			if(tripResult){
				// console.log("Found: " + tripResult.length);
				// console.dir(tripResult);
				var tripDoc = tripResult[0];
				// check if enough data for a possible POS
				if(
					(typeof(tripDoc.seller) !== 'undefined' && tripDoc.seller != '') && 
					(typeof(tripDoc.zip) !== 'undefined' && tripDoc.zip != '') && 
					(typeof(tripDoc.streetName) !== 'undefined' && tripDoc.streetName != '') 
				){
					// console.log("POS matchable.");
					// console.log(tripDoc.seller + " / " + tripDoc.zip + " / " + tripDoc.streetName);
					
					pos.upsertPos( tripDoc.seller,'',tripDoc.streetName,tripDoc.zips.toString(),tripDoc.city,'de_DE',imagechecksum,function(newPos){
						// console.log("Upsert POS: "+newPos);
						if(typeof(newPos) !== null && typeof(newPos) !== 'undefined'){
							console.log("New POS: "+newPos);
						} else {
							console.log("Known POS: "+newPos.seller + " / " + newPos.zip + " / " + newPos.streetName);
						}
					});
				} else {
					console.log("Not enough info to identify POS for trip "+ imagechecksum);
					console.log(tripDoc.seller + " / " + tripDoc.zips.toString() + " / " + tripDoc.street);
				}
				callback();
			}
		});
	},
	updateField : function(userId, tid, fid, data, callback) {
		var Trip = mongoose.model('Trip', tripSchema);
		console.log("Trying to update UID:" + userId +", TID: "+ tid + " FIELD: "+ fid," DATA: "+data);

		if(fid === 'date' && typeof(data) === 'string'){
			var dateParts = data.split(".");
			if(typeof(dateParts[2]) != 'undefined'){
				if(dateParts[2].length < 3 && !isNaN(dateParts[2])){
					console.log('Date() defaulted - not in 21st century, fixing...');
					dateParts[2] = '20'+dateParts[2];
				}
			}
			console.log('Creating date object from "'+data+'" (params: '+dateParts[2]+','+(dateParts[1] - 1)+','+dateParts[0]+')');
			var data = new Date(dateParts[2], (dateParts[1] - 1), dateParts[0]);
		}
		
		if(fid == 'tags'){
			var new_tag_array = toolkit.extract_tags(data);

			Trip.update({ 'user' : userId, 'imagechecksum' : tid },{ $addToSet : { 'tags' : { $each : new_tag_array } } }, {},	function(err, items) {
				if (err) {
					return callback(null, err);
				} else {
					trip.getUserTripTags(userId, tid, function(err, uniqueTags){
						console.dir(uniqueTags.join());
						callback(null, uniqueTags.join());
					});
				}
			});
		} else {
			// can't use $set {fid : data} because fid is taken as literal
			var set = {};
			
			if(fid == 'total' && isNaN(data)){
				data = parseFloat(data.replace(/,/g, "").toFixed(2));
			}

			set[fid] = data;
			console.log('Updating: '+fid);

			Trip.update({ 'user' : userId, 'imagechecksum' : tid },{ $set : set }, {},	function(err, items) {
				if (err) {
					return callback(err);
				} else {
					console.log("Updated " + tid + " / " + fid);
					// console.dir(data);
					callback(data);
				}
			});
		}
		/*
		// This convention allows defaults, setters, validators and middleware triggered on save
		Trip.findOne({ 'imagechecksum' : tid }, function (err, doc){
			doc.fid = data;
			console.log("Doc will be saved as: ",doc);
			doc.save();
		});
		*/
	},
	update : function (userId, imagechecksum, tripdata, callback) {
		var Trip = mongoose.model('Trip', tripSchema);
		Trip.update({'imagechecksum' : imagechecksum}, tripdata, function(err, data) { // { $set: tripdata }
			if (err){
				callback(err, null);
			};
			console.log("Trip updated.");
			callback(data);
		});
	},
	removeTrip : function (userId, tid, callback){
		var Trip = mongoose.model('Trip', tripSchema);
		Trip.remove({ 'user' : userId, 'imagechecksum' : tid }, function(err, items) {
			if (err) return callback(err, null);
			console.log("removeTrip done!");
			callback();
		});
	},
	getField : function(userId, tid, fid, callback) {
		// console.log("getField: Trying UID:" + userId +", TID: "+ tid + "...",", FIELD: "+ fid);
		var Trip = mongoose.model('Trip', tripSchema);
		Trip.find({ 'user' : userId, 'imagechecksum' : tid },
		fid, {},
		function(err, items) {
			if (err) {
				return callback(err, null);
			}
			if(items){
				// console.log("Found: " + items.length);
				callback(err, items);
			}
		});
	},
	getFields : function(userId, tid, fields, callback) {
		var po={};
		var ps='';
		
		ps = fields.join(" ");

		// console.log("getFields: Trying UID:" + userId +", TID: "+ tid + "...",", FIELDS: "+ ps);
		var Trip = mongoose.model('Trip', tripSchema);
		Trip.find({ 'user' : userId, 'imagechecksum' : tid },
		ps, {},
		function(err, items) {
			if (err) {
				return callback(err, null);
			}
			if(items){
				callback(err, items);
			}
		});
	},
	getTrip : function(uid, tid, callback) {
		// console.log("getField: Trying UID:" + uid +", TID: "+ tid + "...",", FIELD: "+ fid);
		// { $or:[{'imagechecksum' : tid},{'_id' : tid}]} 
		var Trip = mongoose.model('Trip', tripSchema);
		Trip.find({ 'user' : uid, 'imagechecksum' : tid},
		{}, {},
		function(err, items) {
			if (err) {
				return callback(err, null);
			}
			if(items){
				callback(err, items);
			}
		});
	},
	getUserTripTags : function(user_id, imagechecksum, callback) {
		// could be global...! ;D
		// e.g. with counts
		// db.collection.aggregate([{$unwind:'$children'}, {$group:{_id:'$children.child_name', freq:{$sum:1}}}])
		var Trip = mongoose.model('Trip', tripSchema);
		var q={'user':user_id};
		if(imagechecksum != ''){
			q.imagechecksum=imagechecksum;
		}
		Trip.find(q, 'tags', {}, function(err, tr) {
			if (err) return callback(err, null);
			console.log("getUserTripTags ("+user.id+") - Found " + tr.length + " tags");
			
			var userTags=[];

			for(tri=0; tri<tr.length; tri++) {
				if(typeof(tr[tri]) != 'undefined' && Object.prototype.toString.call( tr[tri]['tags'] ) === '[object Array]' ){
					for(tti=0; tti < tr[tri]['tags'].length; tti++) {
						// uniques only
						if (userTags.indexOf(tr[tri]['tags'][tti]) === -1 && tr[tri]['tags'][tti] !== ''){
							userTags.push(tr[tri]['tags'][tti]);
						}
					}
				}
			}
			
			callback(err, userTags);
		});
	},
	getUserTrips : function(user, result_limit, skipCount, callback) {
		var Trip = mongoose.model('Trip', tripSchema);
		
		var query = Trip.find({ 'user' : user.id },  null).sort({add_date: -1}).skip(skipCount).limit(result_limit);

		query.exec(function(err, documents) {
			if (err) return callback(err, null);
			console.log("getUserTrips - Found " + documents.length + " trips");
			callback(err, documents);
		});
	},
	getUserTripTotal : function(user, query, callback) {
		var Trip = mongoose.model('Trip', trip.tripSchema);
		// { $match: query }, 
		Trip.aggregate({$group:{_id:"imagechecksum",total:{$sum: "$total"}}},{$project:{_id:0,'totalSpent': "$total"}},function(err, result){ 
			
			if((undefined !== result) && (null !== result)){
				// console.log('AG:');
				result[0]['totalSpent'] = parseFloat(result[0]['totalSpent']).toFixed(2);
				// console.dir(result);
				callback(err, result);
			} else {
				// result[0]['totalSpent'] = 0;
				result = new Array();
				result[0] = new Array();
				result[0]['totalSpent'] = 0;
				callback(err, result);
			}
		});
		/*
		Alternative aggregation

		{ $match: { 'user': user }},

		Model
		.aggregate({ $match: { age: { $gte: 21 }}})
		.unwind('tags')
		.exec(callback)

		*/
	},
	getUserTripTotalHistory : function(user, range, callback){
		// for UI update request, pre-processing for static representations or external use
		// Template builds chart info by itself

		var Trip = mongoose.model('Trip', tripSchema);
		var today = new Date();

		var tomorrow = new Date();
		tomorrow.setDate(today.getDate() + 1);

		var rangeStart = new Date();
		rangeStart.setDate(today.getDate() - range);

		// console.log(user);
		// , 'date' : {$gte: rangeStart, $lt: tomorrow}

		Trip.find({'user' : user.id}, 'date total',
			{},
			function(err, items) {
				if (err) return callback(err, null);
					console.log("getUserTripRunningTotal - Found " + items.length + " trips, formatting...");
					// console.dir(items);
					callback(err, items);
		});
	},
	getUserTripsByTag : function(user, tagsearch, result_limit, skipCount, callback) {
		var tags = [].concat( tagsearch );
		var Trip = mongoose.model('Trip', tripSchema);

		var query = Trip.find({ $or:[{tags : {$in : tags}},{'seller':{$in : tags}}], 'user' : user.id }, null).sort({add_date: -1}).skip(skipCount).limit(result_limit);

		query.exec(function (err, items) {
			if (err) return callback(err, null);
			console.log("getUserTripsByTag - Found " + items.length + " trips tagged "+ tagsearch.toString());
			callback(err, items);
		});
	},
	getTripGlobalMetadata : function(callback) {
		var Trip = mongoose.model('Trip', tripSchema);
		
		var tripGlobalMetadata = {};

		Trip.count({}, function(err, tripGlobalCount){
			// console.dir(tripGlobalMetadata);
			tripGlobalMetadata['globalTotalCount'] = tripGlobalCount;
			console.dir('globalTotalCount: '+tripGlobalCount);

			Trip.count({ "foundGenericData.dates" : {'$ne': null } }, function(err, tripGlobalDates){ // ISODate()

				tripGlobalMetadata['tripGlobalDates'] = tripGlobalDates;
				console.dir('tripGlobalDates: '+tripGlobalDates);

				Trip.count({ "foundGenericData.zips" : {'$ne': null } }, function(err, tripGlobalZips){

					tripGlobalMetadata['tripGlobalZips'] = tripGlobalZips;
					console.dir('tripGlobalZips: '+tripGlobalZips);

					callback(err, tripGlobalMetadata);
				});
			});
		});
		/*
		Trip.find({$or:[{'date' :{ $exists: false }},{'total' :{ $exists: false }},{'approved_items' :{ $exists: false }}]},  null, {sort: {add_date: -1}}, function(err, items) {
			if (err) return callback(err, null);
			console.log("Found " + items.length + " unapproved trips!");
			callback(err, items);
		});
		*/
	},
	getPendingTrips : function(result_limit, skipCount, callback) {
		var Trip = mongoose.model('Trip', tripSchema);

		var query = Trip.find({$or:[{'date' :{ $exists: false }},{'total' :{ $exists: false }},{'approved_items' :{ $exists: false }}]},  null, {sort: {add_date: -1}}).sort({$natural: -1}).skip(skipCount).limit(result_limit);

		query.exec(function (err, items) {
			if (err) return callback(err, null);
			console.log("Found " + items.length + " unapproved trips!");
			callback(err, items);
		});
	},
	getPendingItems : function(locales, result_limit, callback) {
		var Trip = mongoose.model('Trip', tripSchema);
		Trip.find(
			{$or:[	
				{'seller' :{ $exists: true }},
				{'items' :{ $exists: true }},
				{'zip' :{ $exists: true }},
				{'streetName' :{ $exists: true }}
			]}, 
			'items seller imagechecksum city',
			{sort: {add_date: -1}
			}, 
			function(err, result) {
				if (err) return callback(err, null);
				console.log("Found " + result.length + " trips...");
				
				for (var i = 0; i < result.length; i++) {
					// console.dir(result[i]['items']);
					// console.log("Found " + result.length + " items, filtering...");
					
					var filtered_items = new Array();
					
					for (var j = 0; j < result[i]['items'].length; j++) {
						if(result[i]['items'][j]['approved'] != 1){
							filtered_items.push(result[i]['items'][j]);
						}
					}
					
					// result[i]['items'] = [];
					result[i]['items'] = filtered_items;
					
					// console.log('.');
					/*
					var filtered_items = items[i]['items'].filter(function (el) {
						return (el['approved'] != 1);
					});
					*/
				}
				
				console.log("Filtered: " + result.length);
				
				callback(err, result);
			}
		);
	},
	getPriceLines : function(locales, result_limit, callback) {
		var Trip = mongoose.model('Trip', tripSchema);
		Trip.find(
			// {'foundData.house_numbers' :{ $exists: true }}
			/*
			$and : [
			        { 'foundData.priceLines' :{ $exists: true } },
			        { 
				        $or : [	
							{'seller' :{ $exists: true }},
							{'zip' :{ $exists: true }},
							{'streetName' :{ $exists: true }},
						] 
					}
			    ]
			*/
			{ 'foundData.priceLines' :{ $exists: true } }, 
			'imagechecksum foundData',
			{
				sort: {add_date: -1}
			}, 
			function(err, result) {
				// console.dir(result);
				if (err) return callback(err, null);
				console.log("Found " + result.length + " trips...");
				
				var filtered_items = new Array();

				for (var i = 0; i < result.length; i++) {	
					for (var j = 0; j < result[i]['foundData']['priceLines'].length; j++) {

						var item = {};
						item['id'] = result[i]['imagechecksum']+'_'+j;
						item['originChecksum'] = result[i]['imagechecksum'];

						item['itemPrice'] = result[i]['foundData']['priceLines'][j]['itemPrice'];
						item['lineIndex'] = result[i]['foundData']['priceLines'][j]['lineIndex'];
						item['offset'] = result[i]['foundData']['priceLines'][j]['offset'];
						item['content'] = result[i]['foundData']['priceLines'][j]['content'];

						filtered_items.push(item);
						console.log("pushed item #"+j);
						console.dir(item);
					}
				}
				console.log("getPriceLines: " + result.length + " trips, "+filtered_items.length+ " items.");
				callback(err, filtered_items);
			}
		);
	}, 
	getTripWords : function(user, imagechecksum, callback) {
		var Trip = mongoose.model('Trip', tripSchema);
		trip.getField(user, imagechecksum, 'textresult', res, function(err, data){
			if (err) {
				console.log(err, data);
			} else {
				var trip_words = data[0]['textresult'].match(/[^\s]+/g);
				console.log('getWords got '+ trip_words + ' words...');
				callback(trip_words);
			}
		});
	},
	showTripView : function(user, res, new_ids){
		var Trip = mongoose.model('Trip', tripSchema);
		Trip.find({'user' : user.id}, null, {sort: {add_date: -1}}, function(err, user_trips) {
			if (err){ console.log("Error getting trips after upload"); return callback(err, null);}
			
			console.log("Found " + user_trips.length + " trips ("+new_ids+")");

			// poll incomplete trips in frontend to show progress
			// polling url is unsafe to spit to frontend, make a timed user token
			var timed_key = token.makeTimedToken('trip', user.id);
			
			var tripViewParams = {};
			
			tripViewParams.title 	= 'My Trips';
			tripViewParams.user 	= user;
			tripViewParams.user_id 	= user.id;
			tripViewParams.trips 	= user_trips;
			tripViewParams.key 		= timed_key;
			tripViewParams.new_ids 	= [1,2,3];
			
			if(user_trips.length > 0){
				trip.getUserTripTotal(user.id, {'user' : user.id}, function(err, result){
					tripViewParams.triptotalever = String(result[0]['totalSpent']);
					tripViewParams.tripcountever = user_trips.length+1;
					
					res.render('my', tripViewParams);
				});
			} else {
				// console.log("This shouldn't really happen...");
				res.render('my', tripViewParams);
			}
		});
	}
}
