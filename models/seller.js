var mongoose = require('mongoose')
	, tools = require('./tools.js')
	, t = require('./trip.js')
	, p = require('./pos.js')
	, http = require('http');

var sellerSchema = new mongoose.Schema({
	tags: Array,
	add_date: {
		type: Date,
		"default": Date.now
	},
	name: { 
		type: String 
	},
	identifiers: { 
		type: Array 
	},
	locale: { 
		type: String 
	},
	pos_ids: { 
		type: Array 
	},
});

exports = seller = {
	upsertSeller : function (id, name, identifiers, tags, locale, callback){
		var name = name.toLowerCase();
		console.dir(id);
		var seller_id = mongoose.Types.ObjectId( id );
		if(typeof(tags) !== 'undefined' && tags !== ''){
			var tags = tags.match(/[^,]+/g);
		}
		if(typeof(identifiers) !== 'undefined' && identifiers !== ''){
			var identifiers = identifiers.match(/[^,]+/g);
			// identifiers.push(name);
		}

		if(typeof(locale) != 'undefined' || locale == ''){
			locale = 'de_DE';
		}

		var Seller = mongoose.model('Seller', sellerSchema);

		var d = new Date();

		Seller.findOneAndUpdate(
			{ $and: [{_id: seller_id } , {'name': name.toLowerCase()}, {'locale': locale}] },
			{ $addToSet: {'identifiers': { $each: identifiers} }, $set: { 'name': name.toLowerCase(), 'tags': tags, 'locale': locale } }, 
			{ upsert: true }, 
			function(err, newSeller){
				if (err){ console.log(err); return (err);}
				console.log("Upserted Seller: "+name);
				callback(err, newSeller);
			}
		);
	},
	matchInText : function (items, text, callback){
		
		/*
		 var zip_list = [];
		for(var z=zips.length;z>0;z--){ zip_list.push(zips[(z-1)].name); };
		
		var reg = "("+zip_list.join('|')+")";
		*/
		var reg = "("+items+")";
		var re = new RegExp(reg,"ig");

		var item_matches = text.match(re);
		
		if( Object.prototype.toString.call( item_matches ) === '[object Array]' ) {
			callback(null, item_matches);
		} else {
			callback('No seller matches.');
		}
	},
	getLocaleSellerNames : function (locales, glue, callback){
		var Seller = mongoose.model('Seller', sellerSchema);

		Seller.find({ 'locale' : {$in : locales} },  null, {sort: {add_date: -1}}, function(err, items) {
			if (err) return callback(err, null);
			console.log("getLocaleSellerNames - Found " + items.length + " sellers (locales: " + locales.join(",") +")");
			
			// cheap shot join e.g. with | for throwing into a regex
			if(typeof(glue) !== 'undefined' && glue !== ''){
				console.log('glue!');
				var items_flat=new Array();
				for (var i=0, l=items.length; i < l; i++){
					if(i===0){
						items_flat+=items[i]['name'];
					}else{
						items_flat+=glue+items[i]['name'];
					}
				}
				callback(err, items_flat);
			} else {
			callback(err, items);
			}
		});
	}
}
