var mongoose = require('mongoose')
	, conf = require('../_conf_protected')
	, crypto = require('crypto')
	, checksum = require('checksum')
	, bcrypt = require('bcrypt')
	, SALT_WORK_FACTOR = 10
	, tools = require('./tools.js')
	, path = require('path')
	, http = require('http')
	, fs = require('fs')
	, exec = require("child_process").exec;

exports = token = {
	tokenAuth : function(req, path, user_id, res){
		// session and cookie have no power here!
		// console.log('tokencheck (path,user,token): '+path+' , '+user_id+' , '+token);

		// Cycling key for each user/request combination

		//	 Check validity against the two hashes made to match 2 intervals rounded down to 5min
		//	 'current' and 'next'.
		//	 We use 2 because it's possible that the current one will expire 1 second after it's generated.
		//	 If one passes, the hash was made for this user between time-15m and time now.
		//	 So validity is 15:01 - 29:59.

		// The log entry we will write
		var report = '';
		
		// unixtime in seconds
		var unix_now = Math.round(new Date().getTime() / 1000);

		var interval = 900; // 15 min

		// round to last interval (e.g. 5 min. when sec is set to 300)
		var current = unix_now - (unix_now % interval);
		
		var next = unix_now - ((unix_now+(interval+1)) % interval);
		// var next = unix_now + interval + (interval/2);
		// var next = unix_now - (next % interval);

		var current_key = (crypto.createHash("sha1")
			.update(user_id + conf.appSecret + current)
			.digest("hex"));
		  
		var next_key = (crypto.createHash("sha1")
			.update(user_id + conf.appSecret + next)
			.digest("hex"));

		// var current_key = bcrypt.hashSync(user.name + path + conf.appSecret + current, salt);
		// var next_key = bcrypt.hashSync(user.name + path + conf.appSecret + next, salt);
		// console.log('-CHECK token- UID: ' + user_id + ' CURRENT: ' + current);
		// console.log('KEY: ' +next_key);

		// console.log('GIVEN: '+current+'/'+urlKey+', CURRENT:  '+current+'/'+ current_key +',  NEXT:  '+next+'/'+ next_key );

		/*
		Async versions
		compare(user.username + next + )

		// Load hash from your password DB.
		bcrypt.compare("bacon", hash, function(err, res) {
			return res;
		});

		bcrypt.compare("veggies", hash, function(err, res) {
			return res;
		});
		*/

		// 2 ways to pass an auth string:
		//	- in header if your outgoing urls might be unsafe (e.g. tokens dispensed to frontend)
		// - in query string as 'token'
		var requesttoken = '';
		var tokenlocation = '';
		
		
		
		if(typeof (req.get('x-custom-payprd-auth')) == 'undefined'){

			requesttoken = req.get('x-custom-payprd-auth');
			tokenlocation = 'header';
			report += 'auth:header';

		}

		// query overrides header
		if(typeof (req.query.token) != 'undefined'){

			requesttoken = req.query.token;
			tokenlocation = 'query string';
			report += 'auth:query string';

		}

		// res.send(403, 'Not sure about this. siZUkjPrC2tvYZUkjxPo1');

		

		if(typeof(requesttoken) !== '') {
			// pass if compare current OR next
			if((requesttoken === current_key) || (requesttoken === next_key)){
				report += 'Key is OK ('+tokenlocation+').  header: ' +req.get('x-custom-payprd-auth');
				return true;
			} else {
				report += 'Key is invalid ('+tokenlocation+').';
				report += 'Querystring: '+ req.query.token + ', Header: '+req.get('x-custom-payprd-auth');
				// report += '(possible: ' +current_key+' / '+next_key+');
				// console.log(report);
				exec('echo "."', function (){});
				// res.send(401, 'Not sure about this. xpoZUkjPoj2xpoZUkjPoj');
			}
			return report;
		}
	},
	makeTimedToken : function(path, user_id){
		// unixtime in seconds
		var unix_now = Math.round(new Date().getTime() / 1000);

		var interval = 300;

		// round to last interval (e.g. 5 min. when sec is set to 300)
		var current = unix_now - (unix_now % interval);

		// console.log('-MAKE token- UID: ' + user_id + ' CURRENT: ' + current);
		// console.log('-trip- UID: ' + user_id + ' SECRET: ' + conf.appSecret + ' CURRENT: ' + current);

		/*
		var lines_key = console.log("CRYPTO" + crypto.createHash("sha1")
			.update(req.user.id + "user/"+ req.user.id +"/trip/"+ imagechecksum +"/field/lines" + conf.appSecret + unix_now)
			.digest("hex"));
		*/

		// var current_key = bcrypt.hashSync(conf.appSecret + user.name + path + unix_now);

		var token = crypto.createHash("sha1")
			.update(user_id + conf.appSecret + current)
			.digest("hex");

		// console.log('Made token: ' +token);
		return token;
	}
}

/*

// sample 1 orig

[CLIENT] Before making the REST API call, combine a bunch of unique data together (this is typically all the parameters and values you intend on sending, it is the �data� argument in the code snippets on AWS�s site)
[CLIENT] Hash (HMAC-SHA1 or SHA256 preferably) the blob of data data (from Step #1) with your private key assigned to you by the system.
[CLIENT] Send the server the following data:
Some user-identifiable information like an �API Key�, client ID, user ID or something else it can use to identify who you are. This is the public API key, never the private API key. This is a public value that anyone (even evil masterminds can know and you don�t mind). It is just a way for the system to know WHO is sending the request, not if it should trust the sender or not (it will figure that out based on the HMAC).
Send the HMAC (hash) you generated.
Send all the data (parameters and values) you were planning on sending anyway. Probably unencrypted if they are harmless values, like �mode=start&number=4&order=desc� or other operating nonsense. If the values are private, you�ll need to encrypt them.
(OPTIONAL) The only way to protect against �replay attacks� on your API is to include a timestamp of time kind along with the request so the server can decide if this is an �old� request, and deny it. The timestamp must be included into the HMAC generation (effectively stamping a created-on time on the hash) in addition to being checked �within acceptable bounds� on the server.
[SERVER] Receive all the data from the client.
[SERVER] (see OPTIONAL) Compare the current server�s timestamp to the timestamp the client sent. Make sure the difference between the two timestamps it within an acceptable time limit (5-15mins maybe) to hinder replay attacks.
NOTE: Be sure to compare the same timezones and watch out for issues that popup with daylight savings time change-overs.
UPDATE: As correctly pointed out by a few folks, just use UTC time and forget about the DST issues.
[SERVER] Using the user-identifying data sent along with the request (e.g. API Key) look the user up in the DB and load their private key.
[SERVER] Re-combine the same data together that the client did in the same way the client did it. Then hash (generate HMAC) that data blob using the private key you looked up from the DB.
(see OPTIONAL) If you are protecting against replay attacks, include the timestamp from the client in the HMAC re-calculation on the server. Since you already determined this timestamp was within acceptable bounds to be accepted, you have to re-apply it to the hash calculation to make sure it was the same timestamp sent from the client originally, and not a made-up timestamp from a man-in-the-middle attack.
[SERVER] Run that mess of data through the HMAC hash, exactly like you did on the client.
[SERVER] Compare the hash you just got on the server, with the hash the client sent you; if they match, then the client is considered legit, so process the command. Otherwise reject the command!

// end sample 1 orig

[CLIENT] Before API call, combine all parameters and values to send: the �data� argument.(Make replay attacks weaker: Use a timestamp in the hash)
[CLIENT] Hash the blob of data with your private key assigned to you by the system.
[CLIENT] Send the server:
WHO is sending the request: Some user-identifiable information (API key or user ID) to identify who you are.
Send the datablob (hash) you generated.
Send all the data (parameters and values) you were planning on sending anyway. Unencrypted if they are harmless operating syntax; if private, you�ll need to encrypt them.
[SERVER] Receive all the data from the client.
[SERVER] (see OPTIONAL) Compare the current server�s timestamp to the timestamp the client sent. Make sure the difference between the two timestamps it within an acceptable time limit (5-15mins maybe) to hinder replay attacks.
NOTE: Be sure to compare the same timezones and watch out for issues that popup with daylight savings time change-overs.
UPDATE: As correctly pointed out by a few folks, just use UTC time and forget about the DST issues.
[SERVER] Using the user-identifying data sent along with the request (e.g. API Key) look the user up in the DB and load their private key.
[SERVER] Re-combine the same data together that the client did in the same way the client did it. Then hash (generate HMAC) that data blob using the private key you looked up from the DB.
(see OPTIONAL) If you are protecting against replay attacks, include the timestamp from the client in the HMAC re-calculation on the server. Since you already determined this timestamp was within acceptable bounds to be accepted, you have to re-apply it to the hash calculation to make sure it was the same timestamp sent from the client originally, and not a made-up timestamp from a man-in-the-middle attack.
[SERVER] Run that mess of data through the HMAC hash, exactly like you did on the client.
[SERVER] Compare the hash you just got on the server, with the hash the client sent you; if they match, then the client is considered legit, so process the command. Otherwise reject the command!

*/
