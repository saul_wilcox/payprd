var mongoose = require('mongoose')
	, tools = require('./tools.js')
	, SimpleTimestamps = require( 'mongoose-simpletimestamps' ).SimpleTimestamps
	, t = require('./trip.js')
	, m = require('./pos.js')
	, http = require('http');

var itemSchema = new mongoose.Schema({
	brand: {
		type: String,
		"default": ''
	},
	sellers: {
		type: Array
	},
	text_aliases:{
		type: Array
	},
	name: {
		type: String 
	},
	size: {
		type: String 
	},
	unit: {
		type: String,
		"default": ''
	},
	tags: {
		type: Array 
	},
	detail: {
		type: Array 
	},
	origin_trip_id: {
		type: String,
		"default": '' 
	},
	origin_user_id: {
		type: String,
		"default": '' 
	},
	origin_checksum: {
		type: String,
		"default": '' 
	},
	add_date: {
		type: Date,
		"default": Date.now
	},
	locale: {
		type: String 
	},
	status:{
		type: Number,
		"default": 0
	},
	createdAt : {
		type: Date,
		"default": Date.now
	},
	updatedAt : {
		type: Date,
		"default": Date.now
	}
});

itemSchema.plugin(SimpleTimestamps);
mongoose.model('Item', itemSchema);

exports = item = {
	upsertItem : function (originId, alias, brand, name, size, unit, tags, categories, originUserId, originTripId, originChecksum, locale, detail, callback){

		// look up locale based on city?
		if(typeof(locale) != 'undefined' || locale == ''){
			locale = 'de_DE';
			// locale = 'en_US';
		}
		
		var Item = mongoose.model('Item', itemSchema);

		var d = new Date();
		
		// TODO: 
		// loop tags and make lowercase before adding to query object
		// tags.toLowerCase();
		// loop detail?
		// detail
		
		// always make tags from the item name
		var default_tags = name.match(/[^\s.]+/g);
		
		alias = alias.toLowerCase();
		var textalias = new Array();
		textalias.push(alias);
		
		// Defaults for optional parameters
		sellers = typeof sellers !== 'undefined' ? sellers : '';
		textalias 	= typeof textalias !== 'undefined' ? textalias : '';
		name 	= typeof name !== 'undefined' ? name : '';
		brand 	= typeof brand !== 'undefined' ? brand : '';
		size 	= typeof size !== 'undefined' ? size : '';
		unit 	= typeof unit !== 'undefined' ? unit : '';
		tags 	= (typeof tags !== 'undefined' && tags !== null && tags !=='') ? default_tags.concat(toolkit.extract_tags(tags)) : default_tags;
		categories 	= typeof categories !== 'undefined' ? categories : new Array();
		locale 	= typeof locale !== 'undefined' ? locale : '';
		origin_user_id 	= typeof originUserId !== 'undefined' ? originUserId : '';
		origin_trip_id 	= typeof originTripId !== 'undefined' ? originTripId : '';
		origin_checksum = typeof originChecksum !== 'undefined' ? originChecksum : '';
		locale 	= typeof locale !== 'undefined' ? locale : '';
		detail 	= typeof detail !== 'undefined' ? detail : new Array();
		// SimpleTimeStmp not working on upsert, so add createdAt
		createdAt = d;

		/* Note the "update inside update" because you can't easily use $addToSet with $set and upsert:true - needed for tags/sellers */
		
		// name.toLowerCase()
		// , {'tags': tags}
		// unit.toLowerCase()
		// $addToSet: {'text_aliases' : [alias]}
		// , {'text_aliases' : {$in : alias}}
		Item.findOneAndUpdate(
			{ $and: [ {'brand': brand},{'name': name}, {'size': size}, {'unit': unit} ] },
			{ $set: { 'text_aliases' : textalias, 'brand': brand.toLowerCase(), 'name': name, 'size': size.toLowerCase(), 'unit': unit.toLowerCase(), 'tags': tags,  'categories': categories, 'origin_user_id': origin_user_id, 'origin_trip_id': origin_trip_id, 'origin_checksum': origin_checksum, 'detail': detail, 'locale': locale, 'createdAt' : createdAt } }, 
			{ upsert: true }, 
			function(err, newItem){
				if (err){
					console.log(err);
					return(err);
				}
				
				console.log("Item born at "+createdAt+". Wow. Such creation.");
				console.log("Inserted Item: "+ newItem.name  + ' ' + newItem.size + newItem.unit + ' ' + '(' + newItem.brand + ',' + newItem.origin_trip_id + ')');
				callback(newItem);
			}
		);
		/*
		
		Item.update(
			{ $and: [{'brand': brand.toLowerCase()},{'name': name.toLowerCase()}, {'size': size.toLowerCase()}, {'unit': unit.toLowerCase()}, {'tags': tags},{'detail': detail.toLowerCase()}, {'locale': locale}] },
			{ $set: {'brand': brand.toLowerCase(), 'name': name.toLowerCase(), 'size': size.toLowerCase(), 'tags': tags, 'categories': categories, 'origin_user_id': origin_user_id, 'origin_trip_id': origin_trip_id, 'origin_checksum': origin_checksum,'detail': detail.toLowerCase(),'locale': locale } }, 
			{ upsert: true }, 
			function(err, newItem, response){
				if (err){
					console.log(err);
					return(err);
				}
				console.log("Item born at "+newItem.updatedAt+". Wow. Such creation.");
				console.log("Inserted Item: "+ newItem.name  + ' ' + newItem.size + newItem.unit + ' ' + '(' + newItem.brand + ',' + newItem.origin_trip_id + ')');
				callback(newItem);
			}
		);
		*/
	},
	addTags : function(iid, tags, callback) {
		console.log("item.addTag: Trying IID: "+ iid + "...",", TAG: "+ tag);
		Item.findOneAndUpdate(
			{ '_id': iid },
			{ $addToSet : { 'tags' : { $each : new_tag_array } } }, 
			{ upsert: true }, 
			function(err, newItem){
				if (err){
					console.log(err);
					return(err);
				}
				console.log("Inserted Item: "+ newItem.name  + ' ' + newItem.size + newItem.unit + ' ' + '(' + newItem.brand + ',' + newItem.origin_trip_id + ')');
				callback(newItem);
			}
		);
	},
	getField : function(iid, fid, callback) {
		console.log("item.getField: Trying IID: "+ iid + "...",", FIELD: "+ fid);
		var Item = mongoose.model('Item', itemSchema);
		Item.find({ '_id' : iid },
		fid, {},
			function(err, items) {
				if (err) {
					return callback(err, null);
				}
				if(items){
					// console.log("Found: " + items.length);
					callback(err, items);
				}
		});
	},
	getLocaleItems : function (locales, callback){
		var Item = mongoose.model('Item', itemSchema);

		Item.find({ $or:[{tags : {$in : tags}},{'brand':{$in : tags}}]},  null, {}, function(err, result) {
			if (err) return callback(err, null);
			console.log("getLocaleItems - Found " + result.length + " items");

			callback(err, result);
		});
	},
	getLocaleItemNames : function (locales, callback){
		var Item = mongoose.model('Item', itemSchema);
		
		// var tmp_locales = new Array(null,undefined,'','de_DE');
		// tmp_locales = ['','de_DE'];

		Item.find({ 'locale' : {$in : ["","de_DE"]} },  null, {sort: {add_date: -1}}, function(err, result) {
			if (err) return callback(err, null);
			console.log("getLocaleItemNames - Found " + result.length + " items");
			callback(err, result);
		});
	},
	updateField : function(iid, fid, data, callback) {
		console.log("Trying to update UID:" + uid +", TID: "+ tid + " FIELD: "+ fid," DATA: "+data);

		var Item = mongoose.model('Item', itemSchema);

		// can't use $set {fid : data} because fid is taken as literal
		var set = {};

		if(fid == 'price' && isNaN(data)){
			data = parseFloat(data.replace(/,/g, "").toFixed(2));
		}

		set[fid] = data;

		console.log('Updating: '+fid);

		Item.update({ '_id' : iid },{ $set : set }, {},	function(err, items) {
			if (err) {
				return callback(err, null);
			} else {
				console.log("Updated " + iid + " / " + fid);
				console.dir(data);
				callback(data);
			}
		});
	},
}
