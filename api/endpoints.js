var express = require('express')
, multer  = require('multer')

var router = express.Router()

var app_params = require('../public/public_params.json')

// TOKEN
router.get('/token', function (req, res) {
	if(req.user){
		// A user is alredy logged in, give them a key
		res.type('application/json');
		return res.send(200, user.getApiToken(req.user.id));
	} else {
		// Prompt for details for a userId, apiKey and token
		res.render('token', { title: 'Token', lang_content:langcontent, user : req.user });
	}
})
router.post('/token', function (req, res) {
	console.log("POST token request...");
	user.tokenRequest(req, res, function(data){
		console.log('Granted token.');
		console.dir(data);
		res.status(200).send(''+data);
	});
})

// USER
router.get('/user/:uid', function (req, res) {})
router.put('/user/:uid/preferences', function (req, res) {
	user.savePreferences(req.params.uid, req.body, function(err, value){
		if (err) {
			res.send(500, 'user.savePreferences - Error: '+err);
		} else {
			console.log('Preferences for user '+ req.params.uid +' saved:');
			res.status(200).send("OK");
		}
	});
})
router.post('/user/:uid', function (req, res) {})

// TRIP
router.get('/user/:uid/trip/:tid', function (req, res) {
	console.log("getTrip?");
	res.type('application/json');
	if(token.tokenAuth(req, req.path, req.params.uid, res)){
		console.log("Trying getTrip. UID: " + req.params.uid +", TID: "+ req.params.tid + "...");
		trip.getTrip(req.params.uid, req.params.tid, function(err, t){
			if (err) {
				res.send(500, 'trip.getTrip - Error: '+err);
			} else {
				if(t){
					res.type('application/json');
					res.send(t);
				} else {
					res.status(404).send('trip.getTrip - Not found');
				}
			}
		});
	}
})
router.get('/user/:uid/trip/:tid/field/:fid', function (req, res) {
	console.log("getTrip?");
	if(token.tokenAuth(req, req.path, req.params.uid, res)){
		var fields_array = toolkit.extract_tags(req.params.fid+'');
		
		if(Object.prototype.toString.call( fields_array ) === '[object Array]' && fields_array.length >= 1){
			trip.getFields(req.params.uid, req.params.tid, fields_array, function(err, value){
				if (err) {
					res.status(500).send('trip.getFields - Error: '+err);
				} else {
					res.status(200).send(value);
				}
			});
		} else {
			trip.getField(req.params.uid, req.params.tid, req.params.fid, function(err, value){
				if (err) {
					res.status(500).send('trip.getField - Error: '+err);
				} else {
					res.status(200).send(value);
				}
			});
		}
	}
})
router.post('/user/:uid/trip/:tid/field/:fid', function (req, res) {
	if(token.tokenAuth(req, req.path, req.params.uid, res)){
		trip.updateField(req.params.uid, req.params.tid, req.params.fid, req.query.data, function(err, value){
			if (err) {
				res.status(500).send('trip.updateField - Error: '+err);
			} else {
				console.log('trip.updateField updated '+req.params.tid+': '+req.params.fid+ ', '+req.query.data);
				res.status(200).send(value);
			}
		});
	}
})
router.post('/user/:uid/trip/', function (req, res) {})
router.delete('/user/:uid/trip/:tid', function (req, res) {
	if(token.tokenAuth(req, req.path, req.params.uid, res)){
		console.log("Trying Delete. UID: " + req.params.uid +", TID: "+ req.params.tid + "...");
		trip.removeTrip(req.params.uid, req.params.tid, function(err, value){
			if (err) {
				res.status(500).send('trip.getField - Error: '+err);
			} else {
				res.status(200).send(value);
			}
		});
	}
})

module.exports = router
